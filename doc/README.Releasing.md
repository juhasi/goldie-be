## Creating master versions

1. First create new version tag

	`git tag -a vX.Y.Z -m "Version X.Y.Z"`

2. Rebase current branch (the tagged commit) on top of latest master version and squash all commits to a single commit (leave the first commit to "pick" and change other commits to "squash").
For example version v1.0.2 on top of v1.0.1 would be: **git rebase -i --onto goldie-v1.0.1 v1.0.1**

	`git rebase -i --onto goldie-v(X.Y.Z - 1) v(X.Y.Z - 1)`

3. Amend commit message

	`git commit --amend -m "Version X.Y.Z"`

4. Tag the new master version

	`git tag -a goldie-vX.Y.Z -m "Goldie-BE vX.Y.Z"`

5. Push tags to origin and review

	`git push origin --tags`
	`git push review --tags`