'use strict';

var App = angular.module('app', [
        'ngCookies',
        'ngResource',
        'ngSanitize',
        'ngMessages',
        'pascalprecht.translate',
        'ui.bootstrap',
        'ui.router',
        //'ui.utils',
        'placeholders',

        // Pre-cached partials and application metadata
        'app.partials', 'app.metadata',

        // Common
        'app.common.config', 'app.common.controllers', 'app.common.directives', 'app.common.filters',
        'app.common.validation',

        // Login
        'app.login.services', 'app.login.events', 'app.login.controllers',

        // Account
        'app.account.controllers', 'app.account.services',

        // Users
        'app.user.controllers', 'app.user.services',

        // Todo
        'app.todo.controllers'
    ])
    .config(['$stateProvider',
        function ($stateProvider) {
            $stateProvider.state('home', {
                url: '/home',
                templateUrl: 'frontend/partials/main.html'
            })
            .state('validation', {
                url: '/validation',
                templateUrl: 'frontend/partials/validation-sample.html'
            });
        }]);
