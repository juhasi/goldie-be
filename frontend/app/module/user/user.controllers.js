'use strict';

angular.module('app.user.controllers', [])
    .config(['$stateProvider',
        function ($stateProvider) {
            $stateProvider
                .state('users', {
                    url: '/users',
                    templateUrl: 'frontend/partials/user/user-list.html',
                    controller: 'UserController',
                    resolve: {
                        resolvedPage: ['Users', function (Users) {
                            // Pre-load data before route is changed
                            return Users.query({size: 10, sort: 'username'}).$promise;
                        }]
                    }
                });
        }])

    .controller('UserController', ['$scope', 'resolvedPage', 'Users', '$modal',
        function ($scope, resolvedPage, Users, $modal) {
            $scope.page = resolvedPage;

            var reloadPage = function () {
                Users.query({
                    page: ($scope.pager.currentPage - 1),
                    size: ($scope.pager.pageSize),
                    sort: 'username'
                }).$promise
                    .then(function (data) {
                        $scope.page = data;
                    });
            };

            // Pager
            $scope.pager = {
                currentPage: $scope.page.pageable.page + 1,
                pageSize: $scope.page.pageable.size,
                total: $scope.page.total
            };

            $scope.$watch('pager.currentPage', function () {
                reloadPage();
            });

            $scope.create = function () {
                var modalInstance = $modal.open({
                    templateUrl: 'frontend/partials/user/user-form.html',
                    resolve: {
                        user: function () {
                            return { active: true, role: "ROLE_USER" };
                        }
                    },
                    controller: 'UserFormController'
                });

                modalInstance.result.then(function () {
                    reloadPage();
                });
            };

            $scope.edit = function (id) {
                var modalInstance = $modal.open({
                    templateUrl: 'frontend/partials/user/user-form.html',
                    resolve: {
                        user: function () {
                            return Users.get({id: id}).$promise;
                        }
                    },
                    controller: 'UserFormController'
                });

                modalInstance.result.then(function () {
                    reloadPage();
                });
            };

            $scope.delete = function (id) {
                Users.delete({id: id}, reloadPage);
            };
        }])

    .controller('UserFormController', ['$scope', '$modalInstance', 'Users', 'user',
        function ($scope, $modalInstance, Users, user) {
            $scope.user = user;
            $scope.roles = ["ROLE_ADMIN", "ROLE_USER"];

            var onSaveSuccessful = function () {
                $modalInstance.close();
            };

            $scope.save = function () {
                if ($scope.user.id) {
                    Users.update($scope.user, onSaveSuccessful);
                } else {
                    Users.save($scope.user, onSaveSuccessful);
                }
            };

            $scope.cancel = function () {
                $modalInstance.dismiss('cancel');
            };
        }]);
