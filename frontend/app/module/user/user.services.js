'use strict';

angular.module('app.user.services', [])
    .factory('Users', ['$resource', function ($resource) {
        return $resource('api/v1/users/:id', {"id": "@id"}, {
            'query': { method: 'GET', params: { type: "page" }},
            'get': { method: 'GET'},
            'update': {method: 'PUT'}
        });
    }]);
