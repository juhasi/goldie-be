'use strict';

angular.module('app.common.config', ['ui.router', 'pascalprecht.translate'])

    .config(['$stateProvider', '$urlRouterProvider', '$locationProvider',
        function ($stateProvider, $urlRouterProvider, $locationProvider) {
            $urlRouterProvider.otherwise('/');

            // Without server side support html5 must be disabled.
            $locationProvider.html5Mode(false);
        }])

    .config(['$translateProvider', 'appRevision', function ($translateProvider, appRevision) {
        console.log("Application revision is", appRevision);

        // Initialize angular-translate
        $translateProvider.useStaticFilesLoader({
            prefix: '/v/' + appRevision + '/i18n/',
            suffix: '.json'
        });

        $translateProvider.preferredLanguage('en');
        $translateProvider.useCookieStorage();
        $translateProvider.useMissingTranslationHandlerLog();
    }])

    .config(['$httpProvider', function($httpProvider) {
        $httpProvider.interceptors.push(['$q', function($q) {
            function shouldCacheBust(config) {
                if (config.method !== 'GET') {
                    return false;
                }

                if (config.url.indexOf(".html") !== -1) {
                    return false;
                }

                if (config.url.indexOf("/i18n/") !== -1) {
                    return false;
                }

                return true;
            }

            return {
                'request': function (config) {
                    if (config && config.url && shouldCacheBust(config)) {
                        var ts = +(new Date());
                        // try replacing _= if it is there
                        var ret = config.url.replace(/(\?|&)_=.*?(&|$)/, "$1_=" + ts + "$2");
                        // if nothing was replaced, add timestamp to the end
                        config.url = ret + ((ret == config.url) ? (config.url.match(/\?/) ? "&" : "?") + "_=" + ts : "");
                    }

                    return config || $q.when(config);
                }
            };
        }]);
    }]);
