'use strict';

angular.module('app.common.validation', [])
    .directive('validPhoneNumber', ['$http', '$q', function ($http, $q) {
        var BASIC_PATTERN = /^[+]?[0-9 -]{6,}$/;

        function _simpleValidityCheck(phoneNumber) {
            return angular.isString(phoneNumber) && BASIC_PATTERN.test(phoneNumber);
        }

        return {
            require: 'ngModel',
            link: function ($scope, element, attrs, ngModel) {
                // 2 character countryCode (FI, EE) to use as default prefix during backend validation
                var defaultRegion = attrs.defaultRegion || 'FI';

                // NOTE: empty string is considered valid value. Must use ng-required to enforce value.
                ngModel.$asyncValidators.validPhoneNumber = function(modelValue, viewValue) {
                    var phoneNumber = modelValue || viewValue;

                    if (ngModel.$isEmpty(phoneNumber)) {
                        return $q.when(true);
                    } else if (!_simpleValidityCheck(phoneNumber)) {
                        // Skip backend validation if basic format is wrong
                        return $q.reject(false);
                    } else {
                        return $http({
                            method: 'GET',
                            url: '/api/v1/validation/phoneNumber',
                            params: {
                                phoneNumber: phoneNumber,
                                defaultRegion: defaultRegion
                            }
                        });
                    }
                };
            }
        };
    }])

    .directive('validBusinessId', function () {
        var BUSINESS_ID_PATTERN = /^[0-9]{7}-[0-9]$/;
        var BUSINESS_ID_WEIGHTS = [7, 9, 10, 5, 8, 4, 2];

        function _calculateChecksum(input) {
            var sum = 0;
            for (var i = 0; i < 7; i++) {
                sum += BUSINESS_ID_WEIGHTS[i] * (input.charAt(i) - '0');
            }

            var remainder = sum % 11;

            if (remainder === 0) {
                return '0';
            } else if (remainder === 1) {
                return 'x';
            } else {
                return '' + (11 - remainder);
            }
        }

        function _validateBusinessId(businessId) {
            return BUSINESS_ID_PATTERN.test(businessId) &&
                businessId.charAt(8) === _calculateChecksum(businessId);
        }

        return {
            restrict: 'A',
            require: 'ngModel',
            link: function ($scope, element, attrs, ngModel) {
                ngModel.$validators.finnishBusinessId = function(modelValue, viewValue) {
                    var businessId = modelValue || viewValue;

                    if (ngModel.$isEmpty(businessId)) {
                        return true;
                    } else {
                        return angular.isString(businessId) && _validateBusinessId(businessId);
                    }
                };
            }
        };
    })

    .directive('validFinnishPersonalIdentity', function () {
        var PERSONAL_IDENTITY_NUMBER_PATTERN = /^(0[1-9]|[12][0-9]|3[01])(0[1-9]|1[012])([0-9]{2})[+-A][0-9]{3}[0-9A-FHJKLMNPR-Y]$/;
        var CHECK_CHARACTERS = "0123456789ABCDEFHJKLMNPRSTUVWXY";

        function _calculateCheckCharacter(input) {
            var birthDate = input.substring(0, 6);
            var identityNumber = input.substring(7, 10);
            var checkSum = parseInt(birthDate + identityNumber) % 31;
            return CHECK_CHARACTERS[checkSum];
        }

        function _validateFinnishPersonalIdentity(personalIdentity) {
            personalIdentity = personalIdentity.toUpperCase().trim();
            return personalIdentity.length === 11
                && PERSONAL_IDENTITY_NUMBER_PATTERN.test(personalIdentity)
                && personalIdentity.charAt(10) === _calculateCheckCharacter(personalIdentity);
        }

        return {
            restrict: 'A',
            require: 'ngModel',
            link: function ($scope, element, attrs, ngModel) {
                ngModel.$validators.finnishPersonalIdentity = function(modelValue, viewValue) {
                    var personalIdentity = modelValue || viewValue;
                    if (ngModel.$isEmpty(personalIdentity)) {
                        return true;
                    } else {
                        return angular.isString(personalIdentity) && _validateFinnishPersonalIdentity(personalIdentity);
                    }
                };
            }
        };
    });