'use strict';

angular.module('app.common.directives', ['dialogs'])
    .constant('version', '1.0')
    .directive('appVersion', [
        'version', function (version) {
            return function (scope, elm, attrs) {
                elm.text(version);
            };
        }])
    .directive('ngConfirmClick', ['$dialogs', '$translate', function ($dialogs, $translate) {
        // Confirm user action before executing click handler

        var link = function (scope, element, attr) {
            var dialogTitle = $translate.instant('global.dialog.confirmation.title');
            var dialogMessage = attr.ngConfirmClick || $translate.instant('global.dialog.confirmation.text');
            var onClickAction = attr.ngConfirmClickAction;

            var onClick = function (event) {
                var dialog = $dialogs.confirm(dialogTitle, dialogMessage);
                dialog.result.then(function (btn) {
                    scope.$eval(onClickAction);
                });
            };

            element.bind('click', onClick);
        };

        return {
            link: link
        };
    }])
    .directive('selectedLanguage', ['$translate', function ($translate) {
        // Higlight selected language menu item by toggling CSS class
        return {
            restrict: 'A',
            link: function (scope, element, attrs, controller) {
                var language = attrs.selectedLanguage;

                scope.$watch(function () {
                    return $translate.use();

                }, function (selectedLanguage) {
                    if (language === selectedLanguage) {
                        element.addClass('active');
                    } else {
                        element.removeClass('active');
                    }
                });
            }
        };
    }]);
