'use strict';

var App = angular.module('app.common.controllers', [])
    .controller('LanguageController', ['$scope', '$translate',
        function ($scope, $translate) {
            $scope.changeLanguage = function (languageKey) {
                $translate.use(languageKey);
            };
        }]);
