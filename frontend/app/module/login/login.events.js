'use strict';

angular.module('app.login.events', ['app.login.services', 'app.account.services'])
    .run(['$rootScope', '$state', 'LoginService',
        function ($rootScope, $state, LoginService) {

            function stateRequiresAuthentication(state) {
                return state && state.authenticate !== false;
            }

            function stateIsLogin(state) {
                return state && state.name === 'login';
            }

            function updateRootScope(account) {
                if (account) {
                    $rootScope.authenticated = true;
                    $rootScope.account = account;

                } else {
                    $rootScope.authenticated = false;
                    $rootScope.account = null;

                    LoginService.clearAuthentication();
                }
            }

            function onAuthenticationRequired() {
                console.log("Authentication required");

                updateRootScope(null);

                var currentState = $state.$current;

                if (stateRequiresAuthentication(currentState)) {
                    console.log("Redirecting to login...");

                    $state.go("login");
                }
            }

            function onAuthenticationCancelled() {
                console.log("Authentication cancelled");

                updateRootScope(null);
            }

            function onAuthenticationConfirmed(event, account) {
                console.log("Authentication confirmed");

                updateRootScope(account);

                if (stateIsLogin($state.$current)) {
                    console.log("Redirecting after login...");

                    $state.go("home");
                }
            }

            $rootScope.$on("$stateChangeStart", function (event, toState, toParams, fromState, fromParams) {
                var authenticationRequired = stateRequiresAuthentication(toState);

                if (authenticationRequired && !LoginService.isAuthenticated()) {
                    console.log("Not authenticated");

                    // Prevent unauthenticated access
                    event.preventDefault();

                    LoginService.authenticate().then(function() {
                        console.log("Proceeding with state transition to ", toState.name);

                        // Continue state transition
                        $state.go(toState, toParams);

                    }, onAuthenticationRequired);
                }
            });

            // Call when the 401 response is returned by the client
            $rootScope.$on('event:auth-loginRequired', onAuthenticationRequired);

            // Called when user has logged out or cancelled login
            $rootScope.$on('event:auth-loginCancelled', onAuthenticationCancelled);

            // Called after login is successful or when user was already authenticated on page load
            $rootScope.$on('event:auth-loginConfirmed', onAuthenticationConfirmed);
        }]);