'use strict';

angular.module('app.login.services', ['http-auth-interceptor'])
    .factory('LoginService', ['$http', '$q', 'httpAuthService',
        function ($http, $q, httpAuthService) {
            var currentUser = null;
            var hasLoggedOut = false;

            function onLoginSuccess(user) {
                currentUser = user;
                hasLoggedOut = false;

                // Retry deferred requests
                httpAuthService.loginConfirmed(currentUser);

                return currentUser;
            }

            function onLogoutSuccess() {
                currentUser = null;
                hasLoggedOut = true;

                // Clear list of deferred requests
                httpAuthService.loginCancelled();
            }

            var service = {
                isAuthenticated: function () {
                    return !!currentUser && angular.isNumber(currentUser.id);
                },
                clearAuthentication: function() {
                    currentUser = null;
                    hasLoggedOut = true;
                },
                authenticate: function () {
                    if (service.isAuthenticated()) {
                        console.log("Already authenticated");

                        return $q.when(onLoginSuccess(currentUser));

                    } else if (hasLoggedOut) {
                        return $q.reject("Logged out");
                    }

                    // Check authentication, 200 = logged in and 401 = logged out
                    return $http.get('/api/v1/account', {
                        ignoreAuthModule: 'ignoreAuthModule'
                    }).success(onLoginSuccess);
                },
                // Ask the backend to see if a user is already authenticated - this may be from a previous session.
                requestCurrentUser: function () {
                    if (service.isAuthenticated()) {
                        return $q.when(currentUser);
                    } else {
                        return service.authenticate();
                    }
                },
                login: function (param) {
                    // Spring Security Form Login request
                    var data = "username=" + encodeURIComponent(param.username) +
                        "&password=" + encodeURIComponent(param.password) +
                        "&_spring_security_remember_me=" + param.rememberMe;

                    var config = {
                        // Do not buffer authentication request if response status is 401
                        ignoreAuthModule: 'ignoreAuthModule',
                        headers: {
                            "Content-Type": "application/x-www-form-urlencoded"
                        }
                    };

                    return $http.post('/login', data, config).success(onLoginSuccess);
                },
                logout: function () {
                    return $http.post('/logout', {}).success(onLogoutSuccess);
                }
            };

            return service;
        }]);
