'use strict';

angular.module('app.login.controllers', ['ui.router', 'app.login.services'])
    .config(['$stateProvider', function ($stateProvider) {

        $stateProvider
            .state('login', {
                url: '/',
                templateUrl: 'frontend/partials/login/login.html',
                controller: 'LoginController',
                authenticate: false

            }).state('logout', {
                url: '/logout',
                templateUrl: 'frontend/partials/login/login.html',
                controller: 'LogoutController',
                authenticate: false
            });
    }])

    .controller('LoginController', ['$scope', 'LoginService',
        function ($scope, LoginService) {
            $scope.username = "";
            $scope.password = "";
            $scope.rememberMe = true;
            $scope.authenticationError = false;

            // Make sure user is not already logged in
            LoginService.authenticate();

            $scope.login = function () {
                LoginService.login({
                    username: $scope.username,
                    password: $scope.password,
                    rememberMe: $scope.rememberMe

                }).then(function () {
                    $scope.authenticationError = false;
                    $scope.password = "";

                }, function () {
                    $scope.authenticationError = true;
                    $scope.password = "";
                });
            };
        }])

    .controller('LogoutController', ['LoginService', '$state',
        function (LoginService, $state) {
            LoginService.logout().then(function() {
                $state.go('login');
            });
        }]);
