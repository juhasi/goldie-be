'use strict';

angular.module('app.todo.controllers', [])
    .config(['$stateProvider', function ($stateProvider) {
        $stateProvider
            .state('todos', {
                url: '/todo',
                templateUrl: 'frontend/partials/todo/todo.html',
                authenticate: false
            });
    }])
    .controller('TodoCtrl', [
        '$scope', function ($scope) {
            $scope.todos = [
                {
                    text: "learn angula!r",
                    done: true
                },
                {
                    text: "build an angular app",
                    done: false
                }
            ];
            $scope.addTodo = function () {
                $scope.todos.push({
                    text: $scope.todoText,
                    done: false
                });
                $scope.todoText = "";
            };
            $scope.remaining = function () {
                var count;
                count = 0;
                angular.forEach($scope.todos, function (todo) {
                    count += (todo.done ? 0 : 1);
                });
                return count;
            };
            $scope.archive = function () {
                var oldTodos;
                oldTodos = $scope.todos;
                $scope.todos = [];
                angular.forEach(oldTodos, function (todo) {
                    if (!todo.done) {
                        $scope.todos.push(todo);
                    }
                });
            };
        }
    ]);