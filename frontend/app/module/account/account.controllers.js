'use strict';

angular.module('app.account.controllers', [])
    .config(['$stateProvider',
        function ($stateProvider) {
            $stateProvider
                .state('account', {
                    url: '/account',
                    templateUrl: 'frontend/partials/account/account.html',
                    controller: 'AccountController',
                    resolve: {
                        resolvedAccount: ['Account', function (Account) {
                            return Account.get().$promise;
                        }]
                    }
                })
                .state('password', {
                    url: '/account/password',
                    templateUrl: 'frontend/partials/account/password.html',
                    controller: 'PasswordController'
                })
                .state('recoverPassword', {
                    url: '/password/recover',
                    templateUrl: 'frontend/partials/account/password_recover.html',
                    controller: 'RecoverPasswordController',
                    authenticate: false
                })
                .state('resetPassword', {
                    url: '/password/reset/{token}',
                    templateUrl: 'frontend/partials/account/password_reset.html',
                    controller: 'PasswordResetController',
                    authenticate: false
                });
        }])
    .controller('AccountController', ['$scope', 'resolvedAccount', 'Account',
        function ($scope, resolvedAccount, Account) {
            $scope.success = null;
            $scope.error = null;
            $scope.settingsAccount = resolvedAccount;

            $scope.save = function () {
                Account.save($scope.settingsAccount,
                    function (value, responseHeaders) {
                        $scope.error = null;
                        $scope.success = 'OK';
                        $scope.settingsAccount = Account.get();
                    },
                    function (httpResponse) {
                        $scope.success = null;
                        $scope.error = "ERROR";
                    });
            };
        }])
    .controller('PasswordController', ['$scope', 'Password',
        function ($scope, Password) {
            $scope.success = null;
            $scope.error = null;
            $scope.doNotMatch = null;
            $scope.changePassword = function () {
                if ($scope.password != $scope.confirmPassword) {
                    $scope.doNotMatch = "ERROR";
                } else {
                    $scope.doNotMatch = null;

                    var data = {
                        password: $scope.password,
                        passwordConfirm: $scope.confirmPassword,
                        passwordCurrent: $scope.previousPassword
                    };

                    Password.save(data,
                        function (value, responseHeaders) {
                            $scope.error = null;
                            $scope.success = 'OK';
                        },
                        function (httpResponse) {
                            $scope.success = null;
                            $scope.error = "ERROR";
                        });
                }
            };
        }])
    .controller('RecoverPasswordController', ['$scope', '$state', 'PasswordResetService',
        function ($scope, $state, PasswordResetService) {
            $scope.email = "";

            $scope.submit = function () {
                $scope.error = false;

                var successCallback = function () {
                    $state.transitionTo('login');
                };

                var errorCallback = function () {
                    $scope.error = true;
                };

                PasswordResetService.requestPasswordResetEmail($scope.email)
                    .then(successCallback, errorCallback);
            };
        }])
    .controller('PasswordResetController', ['$scope', '$state', '$stateParams', 'PasswordResetService',
        function ($scope, $state, $stateParams, PasswordResetService) {
            $scope.password = "";

            var passwordResetToken = $stateParams.token;

            $scope.submit = function () {
                $scope.error = false;

                var successCallback = function () {
                    $state.transitionTo('login');
                };

                var errorCallback = function () {
                    $scope.error = true;
                };

                PasswordResetService.resetPasswordUsingToken(passwordResetToken, $scope.password)
                    .then(successCallback, errorCallback);
            };
        }]);