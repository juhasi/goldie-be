'use strict';

angular.module('app.account.services', ['ngResource'])
    .factory('Account', ['$resource',
        function ($resource) {
            return $resource('/api/v1/account', {}, {});
        }])
    .factory('Password', ['$resource',
        function ($resource) {
            return $resource('/api/v1/account/password', {}, {});
        }])
    .factory('PasswordResetService', ['$http',
        function ($http) {
            return {
                requestPasswordResetEmail: function (email) {
                    var data = {
                        "email": email
                    };
                    return $http.post("/api/v1/password/forgot", data);
                },
                resetPasswordUsingToken: function (resetToken, newPassword) {
                    var data = {
                        "token": resetToken,
                        "password": newPassword
                    };
                    return $http.post("/api/v1/password/reset", data);
                }
            };
        }]);