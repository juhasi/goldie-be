Goldie Backend
=============

## Developer profile
Developer specific profile file  
Either comment out the following section form pom.xml  

	<build>
		<filters>  
			<!-- Enable this for developer specific properties. -->
			<filter>profiles/${build.profile.id}/config.${user.name}.properties</filter>
		</filters>
	</build>

or create new file,`{{projectDirectory}}\profiles\dev\config.{{username}}.properties`, containing your personal parameter overrides  

## Default user accounts
admin/admin   
user/user

## JVM parameters
To avoid JVM OutOfMemory and PermGen errors then at least the following JVM options must be applied for Tomcat and Maven:

-Xms384m -Xmx512m -XX:MaxPermSize=256m -XX:+UseConcMarkSweepGC

Some additional tweaks and more memory are recommended for production use:

-Xms512m -Xmx1024m -XX:PermSize=128m -XX:MaxPermSize=256m
-XX:+UseConcMarkSweepGC -XX:+CMSIncrementalMode -XX:+CMSClassUnloadingEnabled
-Djava.awt.headless=true -Djava.net.preferIPv4Stack=true -Xverify:none

## Prerequisite

[Maven](http://maven.apache.org/)  
Git ([msysGit for Windows](http://msysgit.github.io/))  
[Node.js](http://nodejs.org/download/)  
[Bower](http://bower.io/) (npm install -g bower)  
[Gulp](http://gulpjs.com/) (npm install -g gulp)

## Notes for Eclipse users

Tested with Eclipse Luna Java EE version

- Import existing Maven project -> Eclipse should open dialog "Setup Maven plugin connectors" and let you install buildhelper connector automatically. Select "resolve later" for antrun.
- Install antrun connector: https://code.google.com/p/nl-mwensveen-m2e-extras/
- Add to pom.xml plugins section:


```
#!xml
	<plugin>
		<groupId>org.eclipse.m2e</groupId>
		<artifactId>lifecycle-mapping</artifactId>
		<version>1.0.0</version>
		<configuration>
			<lifecycleMappingMetadata>
				<pluginExecutions>
					<pluginExecution>
						<pluginExecutionFilter>
							<groupId>org.codehaus.mojo</groupId>
							<artifactId>build-helper-maven-plugin</artifactId>
							<versionRange>[1.0,)</versionRange>
							<goals>
								<goal>add-source</goal>
								<goal>add-test-source</goal>
								<goal>add-resource</goal>
								<goal>add-test-resource</goal>
								<goal>maven-version</goal>
								<goal>parse-version</goal>
							</goals>
						</pluginExecutionFilter>
						<action>
							<execute>
								<runOnConfiguration>true</runOnConfiguration>
								<runOnIncremental>true</runOnIncremental>
							</execute>
						</action>
					</pluginExecution>
				</pluginExecutions>
			</lifecycleMappingMetadata>
		</configuration>
	</plugin>
```


- Open "Java Resources" in the source tree, remove src/integration-test/java from build path.
- Install Tomcat and add Tomcat/lib/jsp-api.jar and servlet-api.jar to java build path / libraries.