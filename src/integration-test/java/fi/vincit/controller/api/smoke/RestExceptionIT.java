package fi.vincit.controller.api.smoke;

import fi.vincit.controller.api.AbstractRestApiIT;
import org.hamcrest.Matchers;
import org.junit.Test;
import org.springframework.http.HttpStatus;

import static com.jayway.restassured.RestAssured.given;

public class RestExceptionIT extends AbstractRestApiIT {

    @Test
    public void testThrowIllegalStateException() {
        given().auth().none().queryParam("type", "state")
                .expect().statusCode(HttpStatus.INTERNAL_SERVER_ERROR.value())
                .when().get(Constants.API_URL_THROW);
    }

    @Test
    public void testThrowIllegalArgumentException() {
        given().auth().none().queryParam("type", "argument")
                .expect().statusCode(HttpStatus.BAD_REQUEST.value())
                .when().get(Constants.API_URL_THROW);
    }

    @Test
    public void testThrowNotFoundException() {
        given().auth().none().queryParam("type", "notfound")
                .expect().statusCode(HttpStatus.NOT_FOUND.value())
                .body("message", Matchers.equalTo("notfound"))
                .when().get(Constants.API_URL_THROW);
    }

    @Test
    public void testThrowNotImplementedException() {
        given().auth().none().queryParam("type", "notimplemented")
                .expect().statusCode(HttpStatus.NOT_IMPLEMENTED.value())
                .body("message", Matchers.equalTo("notimplemented"))
                .when().get(Constants.API_URL_THROW);
    }

    @Test
    public void testThrowAccessDeniedException() {
        // Security exceptions should not expose any additional information!
        given().auth().none().queryParam("type", "accessdenied")
                .expect().statusCode(HttpStatus.FORBIDDEN.value())
                .body("status", Matchers.equalTo("FORBIDDEN"))
                .body("message", Matchers.equalTo("Forbidden"))
                .when().get(Constants.API_URL_THROW);
    }

    @Test
    public void testThrowRuntimeException() {
        // Should not expose any additional information about unknown exception!
        given().auth().none().queryParam("type", "runtime")
                .expect().statusCode(HttpStatus.INTERNAL_SERVER_ERROR.value())
                .body("status", Matchers.equalTo("INTERNAL_SERVER_ERROR"))
                .body("message", Matchers.equalTo("Internal Server Error"))
                .when().get(Constants.API_URL_THROW);
    }
}
