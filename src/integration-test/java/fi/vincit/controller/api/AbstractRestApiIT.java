package fi.vincit.controller.api;

import com.jayway.restassured.RestAssured;
import com.jayway.restassured.filter.log.RequestLoggingFilter;
import com.jayway.restassured.filter.log.ResponseLoggingFilter;
import com.jayway.restassured.parsing.Parser;
import com.jayway.restassured.specification.RequestSpecification;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;
import org.junit.runners.BlockJUnit4ClassRunner;

import java.io.IOException;
import java.util.Properties;

import static com.jayway.restassured.RestAssured.given;

@RunWith(BlockJUnit4ClassRunner.class)
public abstract class AbstractRestApiIT {

    protected static final String API_USERNAME = "admin";
    protected static final String API_PASSWORD = "admin";

    @BeforeClass
    public static void init() throws IOException {
        Properties props = new Properties();
        props.load(AbstractRestApiIT.class.getClassLoader().getResourceAsStream("configuration/application.properties"));

        RestAssured.reset();
        RestAssured.baseURI=props.getProperty("server.url");
        RestAssured.port=Integer.parseInt(props.getProperty("server.port"));
        RestAssured.defaultParser = Parser.JSON;
        RestAssured.filters(new RequestLoggingFilter(), new ResponseLoggingFilter());
    }

    protected static final RequestSpecification givenUserAuthentication() {
        return given().auth().preemptive().basic(API_USERNAME, API_PASSWORD);
    }
}
