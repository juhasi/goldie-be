package fi.vincit.security.authentication;

import fi.vincit.feature.account.exception.RecentlyUsedPasswordException;
import fi.vincit.feature.account.entity.SystemUser;
import fi.vincit.security.UserInfo;
import org.springframework.security.authentication.TestingAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;

import java.util.LinkedList;
import java.util.List;
import org.springframework.security.crypto.password.PasswordEncoder;

public class TestAuthenticationTokenUtil {
    public static Authentication createAuthentication(final String username,
                                                      final String plaintextPassword,
                                                      final Long userId,
                                                      final SystemUser.Role role,
                                                      final boolean authenticated,
                                                      final PasswordEncoder passwordEncoder) {
        final SystemUser user = new SystemUser();
        user.setUsername(username);
        user.setId(userId);
        user.setRole(role);
        try {
            user.setPasswordAsPlaintext(plaintextPassword, passwordEncoder);
        } catch (RecentlyUsedPasswordException ex) {
            throw new IllegalStateException("newly created user cannot have previous passwords", ex);
        }

        final List<GrantedAuthority> grantedAuthorities = (role != null)
                ? AuthorityUtils.createAuthorityList(role.name())
                : new LinkedList<GrantedAuthority>();

        final UserInfo.UserInfoBuilder builder =
                new UserInfo.UserInfoBuilder(user);

        final UserInfo principal = builder.createUserInfo();

        final TestingAuthenticationToken authenticationToken =
                new TestingAuthenticationToken(principal, passwordEncoder.encode(plaintextPassword), grantedAuthorities);

        authenticationToken.setAuthenticated(authenticated);

        return authenticationToken;
    }

    private TestAuthenticationTokenUtil() {}
}
