package fi.vincit.security.audit;

import fi.vincit.feature.account.entity.SystemUser;
import fi.vincit.security.UserInfo;
import fi.vincit.security.authorization.api.EntityAuthorizationTarget;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import static org.assertj.core.api.Assertions.assertThat;

public class ConsoleAuthorizationAuditListenerTest {

    @Mock
    private EntityAuthorizationTarget target;

    private ConsoleAuthorizationAuditListener auditListener = new ConsoleAuthorizationAuditListener();

    @Before
    public void init() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testBuildLogMessage() {
        UserInfo testUser = new UserInfo.UserInfoBuilder(
                "testUser", 256L, SystemUser.Role.ROLE_USER).createUserInfo();
        Mockito.when(target.getAuthorizationTargetClass()).thenReturn(Double.class);
        Mockito.when(target.getAuthorizationTargetId()).thenReturn(312L);
        Mockito.when(target.getAuthorizationTargetName()).thenReturn("testTarget");

        String logMessage = auditListener.createLogMessage(true, "write", target, testUser);

        Assert.assertEquals("Wrong message",
                "Granted 'write' permission for user [id=256, username='testUser'] for target object [type='testTarget', id=312]", logMessage);
    }

    @Test
    public void testBuildGrantedPermissionPrefix() {
        StringBuilder sb = new StringBuilder();
        auditListener.buildPermissionPrefix(true, "write", sb);

        assertThat(sb.toString()).as("Wrong message").isEqualTo("Granted 'write' permission");
    }

    @Test
    public void testBuildDeniedPermissionPrefix() {
        StringBuilder sb = new StringBuilder();
        auditListener.buildPermissionPrefix(false, "read", sb);

        assertThat(sb.toString()).as("Wrong message").isEqualTo("Denied 'read' permission");
    }

    @Test
    public void testBuidNormalUserIdentifier() {
        UserInfo testUser = new UserInfo.UserInfoBuilder(
                "testUser", 256L, SystemUser.Role.ROLE_USER).createUserInfo();

        StringBuilder sb = new StringBuilder();
        auditListener.buildUserIdentifier(testUser, sb);

        assertThat(sb.toString()).as("Wrong message").isEqualTo("for user [id=256, username='testUser']");
    }

    @Test
    public void testBuildAnonymousUserIdentifier() {
        StringBuilder sb = new StringBuilder();
        auditListener.buildUserIdentifier(null, sb);

        assertThat(sb.toString()).as("Wrong message").isEqualTo("for user <anonymous>");
    }

    @Test
    public void testTargetIdentifier() {
        Mockito.when(target.getAuthorizationTargetClass()).thenReturn(Double.class);
        Mockito.when(target.getAuthorizationTargetId()).thenReturn(312L);
        Mockito.when(target.getAuthorizationTargetName()).thenReturn("testTarget");

        StringBuilder sb = new StringBuilder();
        auditListener.buildTargetIdentifier(target, sb);

        assertThat(sb.toString()).as("Wrong message").isEqualTo("for target object [type='testTarget', id=312]");
    }

    @Test
    public void testForUnknownTarget() {
        StringBuilder sb = new StringBuilder();
        auditListener.buildTargetIdentifier(null, sb);

        assertThat(sb.toString()).as("Wrong message").isEqualTo("for unknown target object");
    }
}
