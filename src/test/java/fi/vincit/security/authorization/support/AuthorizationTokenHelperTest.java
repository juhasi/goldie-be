package fi.vincit.security.authorization.support;

import fi.vincit.feature.account.entity.SystemUser;
import fi.vincit.security.authorization.UserEntityAuthorization;
import org.junit.Test;

import java.util.Collections;

import static org.assertj.core.api.Assertions.assertThat;

public class AuthorizationTokenHelperTest {
    @Test
    public void testDeniedWithEmptyAcquiredTokensAndNoGrants() {
        AuthorizationTokenHelper helper = new AuthorizationTokenHelper("");
        assertThat(helper.hasPermission("read", Collections.<String>emptySet())).isFalse();
    }

    @Test
    public void testDeniedWithNonEmptyAcquiredTokensAndNoGrants() {
        AuthorizationTokenHelper helper = new AuthorizationTokenHelper("");
        assertThat(helper.hasPermission("read", Collections.singleton("ROLE_USER"))).isFalse();
    }

    @Test
    public void testDeniedWithAcquiredTokenNotGranted() {
        AuthorizationTokenHelper helper = new AuthorizationTokenHelper("");
        helper.grant("read", SystemUser.Role.ROLE_ADMIN);

        assertThat(helper.hasPermission("read", Collections.singleton("ROLE_USER"))).isFalse();
    }

    @Test
    public void testDeniedWithAcquiredTokenNotGrantedForGivenPermission() {
        AuthorizationTokenHelper helper = new AuthorizationTokenHelper("");
        helper.grant("write", SystemUser.Role.ROLE_USER);

        assertThat(helper.hasPermission("read", Collections.singleton("ROLE_USER"))).isFalse();
    }

    @Test
    public void testAccessGranted() {
        AuthorizationTokenHelper helper = new AuthorizationTokenHelper("");
        helper.grant("read", SystemUser.Role.ROLE_USER);

        assertThat(helper.hasPermission("read", Collections.singleton("ROLE_USER"))).isTrue();
    }

    @Test
    public void testAccessGrantedForMultipleRoles() {
        AuthorizationTokenHelper helper = new AuthorizationTokenHelper("");
        helper.grant("read", SystemUser.Role.ROLE_USER);
        helper.grant("read", SystemUser.Role.ROLE_ADMIN);

        assertThat(helper.hasPermission("read", Collections.singleton("ROLE_USER"))).isTrue();
    }

    @Test
    public void testCanonicalTokenNameForUserRole() {
        assertThat(AuthorizationTokenHelper.getCanonicalAuthorizationToken(SystemUser.Role.ROLE_ADMIN))
            .isEqualTo("ROLE_ADMIN").as("Should not add class prefix");
    }

    @Test
    public void testCanonicalTokenNameForEntityRole() {
        assertThat(AuthorizationTokenHelper.getCanonicalAuthorizationToken(UserEntityAuthorization.Role.SELF))
                .isEqualTo("Role.SELF").as("Should add class prefix");
    }
}
