package fi.vincit.feature.storage.impl.db;

import fi.vincit.EmbeddedDatabaseTest;
import fi.vincit.feature.storage.api.FileMetadata;
import fi.vincit.feature.storage.api.FileStorageService;
import fi.vincit.feature.storage.api.FileType;
import fi.vincit.feature.storage.api.StorageType;
import fi.vincit.feature.storage.entity.PersistentFileMetadata;
import org.junit.Test;

import javax.annotation.Resource;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.UUID;

import static org.assertj.core.api.Assertions.*;

public class DbFileStorageServiceTest extends EmbeddedDatabaseTest {

    @Resource
    private FileStorageService fileStorageService;

    @Test
    public void testNormal() throws IOException {
        FileMetadata metadata = FileMetadata.createForFilename("test.txt", "text/plain");
        byte[] data = "Hello world".getBytes("UTF-8");

        UUID id = fileStorageService.storeFile(
                FileType.UNKNOWN,
                StorageType.LOCAL_DATABASE,
                metadata,
                new ByteArrayInputStream(data)).getId();

        PersistentFileMetadata file = fileStorageService.getMetadata(id);

        assertThat(file.getStorageType()).isEqualTo(StorageType.LOCAL_DATABASE);
        assertThat(file.getOriginalFilename()).isEqualTo("test.txt");
        assertThat(file.getContentType()).isEqualTo("text/plain");
        assertThat(file.getContentSize()).isEqualTo(11L);
        assertThat(file.getCrc32()).isEqualTo(2346098258L);
    }
}
