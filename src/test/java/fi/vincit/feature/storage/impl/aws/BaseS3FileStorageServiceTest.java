package fi.vincit.feature.storage.impl.aws;

import org.jets3t.service.model.S3Bucket;
import org.jets3t.service.model.S3Object;
import org.junit.Test;

import java.net.URISyntaxException;
import java.net.URL;

import static org.assertj.core.api.Assertions.assertThat;

public class BaseS3FileStorageServiceTest {

    private static URL createUrl() {
        final S3Bucket s3Bucket = new S3Bucket("demo-bucket", S3Bucket.LOCATION_EUROPE);
        final S3Object s3Object = new S3Object(s3Bucket, "bucket-key");

        return BaseS3FileStorage.createResourceURL(s3Bucket, s3Object, BaseS3FileStorage.ENDPOINT_EU_WEST1);
    }

    @Test
    public void testCreateResourceUrl() throws URISyntaxException {
        final URL resourceURL = createUrl();

        assertThat(resourceURL.getHost()).isEqualTo("demo-bucket.s3-eu-west-1.amazonaws.com");
        assertThat(resourceURL.getPort()).isEqualTo(-1);
        assertThat(resourceURL.getPath()).isEqualTo("/bucket-key");
        assertThat(resourceURL.getQuery()).isNullOrEmpty();

        assertThat(resourceURL.toExternalForm()).isEqualTo("http://demo-bucket.s3-eu-west-1.amazonaws.com/bucket-key");
        assertThat(resourceURL.toURI().toASCIIString()).isEqualTo("http://demo-bucket.s3-eu-west-1.amazonaws.com/bucket-key");
    }

    @Test
    public void testParseResourceUrl() {
        S3Object s3Object = BaseS3FileStorage.parseResourceURL(createUrl(), BaseS3FileStorage.ENDPOINT_EU_WEST1);

        assertThat(s3Object.getBucketName()).isEqualTo("demo-bucket");
        assertThat(s3Object.getKey()).isEqualTo("bucket-key");
        assertThat(s3Object.getName()).isEqualTo("bucket-key");
    }
}