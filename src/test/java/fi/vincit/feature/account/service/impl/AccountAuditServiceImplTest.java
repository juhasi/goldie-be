package fi.vincit.feature.account.service.impl;

import fi.vincit.feature.account.dao.AccountActivityMessageRepository;
import fi.vincit.feature.account.dao.UserRepository;
import fi.vincit.feature.account.service.AccountAuditService;
import fi.vincit.feature.account.entity.AccountActivityMessage;
import org.hamcrest.CustomTypeSafeMatcher;
import org.hamcrest.Matcher;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.security.authentication.event.AbstractAuthenticationFailureEvent;
import org.springframework.security.authentication.event.AuthenticationFailureDisabledEvent;
import org.springframework.security.authentication.event.AuthenticationSuccessEvent;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.WebAuthenticationDetails;

import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class AccountAuditServiceImplTest {

    @InjectMocks
    private AccountAuditService auditService = new AccountAuditServiceImpl();

    @Mock
    private AccountActivityMessageRepository accountActivityMessageRepository;

    @Mock
    private UserRepository userRepository;

    @Mock
    private Authentication authMock;

    private String username;

    @Before
    public void setup() {
        username = "user";
        authMock = mock(Authentication.class);
        when(authMock.getName()).thenReturn(username);

        WebAuthenticationDetails details = new WebAuthenticationDetails(new MockHttpServletRequest());
        when(authMock.getDetails()).thenReturn(details);
    }

    @Test
    public void testAuditLogin_success() {
        AuthenticationSuccessEvent event = new AuthenticationSuccessEvent(authMock);
        auditService.auditLoginSuccessEvent(event);

        verify(accountActivityMessageRepository, times(1)).save(argThat(matches(true, username, null)));
        verifyNoMoreInteractions(accountActivityMessageRepository);
    }

    @Test
    public void testAuditLogin_failure() {
        String error = "exception message";
        AuthenticationException ex = new AuthenticationException(error) {
        };
        AbstractAuthenticationFailureEvent event = new AuthenticationFailureDisabledEvent(authMock, ex);
        auditService.auditLoginFailureEvent(event);

        verify(accountActivityMessageRepository, times(1)).save(argThat(matches(false, username, error)));
        verifyNoMoreInteractions(accountActivityMessageRepository);
    }

    private static Matcher<AccountActivityMessage> matches(
            final boolean isSuccessfulLogin,
            final String username,
            final String errorMessage) {
        return new CustomTypeSafeMatcher<AccountActivityMessage>("checks error, username and login success") {
            @Override
            protected boolean matchesSafely(AccountActivityMessage item) {
                boolean errorOk = (item.getExceptionMessage() == null && errorMessage == null) || item.getExceptionMessage().equals(errorMessage);
                boolean usernameOk = (username == null && item.getUsername() == null) || item.getUsername().equals(username);
                boolean typeOk = isSuccessfulLogin
                        ? (item.getActivityType() == AccountActivityMessage.ActivityType.LOGIN_SUCCESS)
                        : (item.getActivityType() == AccountActivityMessage.ActivityType.LOGIN_FAILRE);

                return errorOk && usernameOk && typeOk;
            }
        };
    }
}