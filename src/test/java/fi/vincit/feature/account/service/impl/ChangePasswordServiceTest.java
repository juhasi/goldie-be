package fi.vincit.feature.account.service.impl;

import fi.vincit.feature.account.entity.SystemUser;
import fi.vincit.feature.account.exception.RecentlyUsedPasswordException;
import fi.vincit.feature.account.service.ChangePasswordService;
import fi.vincit.feature.common.util.RuntimeEnvironmentUtil;
import org.junit.Test;
import org.springframework.security.crypto.password.NoOpPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class ChangePasswordServiceTest {
    public static final String SAME_PASSWORD = "newPassword";

    private ChangePasswordService changePasswordService;

    private void setup(int passwordsHeld) {
        PasswordEncoder noOpEncoder = NoOpPasswordEncoder.getInstance(); //Faster than bcrypt.
        RuntimeEnvironmentUtil mockEnvUtil = mock(RuntimeEnvironmentUtil.class);

        when(mockEnvUtil.isPreviousPasswordCheckEnabled()).thenReturn(passwordsHeld > 0);
        when(mockEnvUtil.getMaximumPreviousPasswordsHeld()).thenReturn(passwordsHeld);

        this.changePasswordService = new ChangePasswordServiceImpl(noOpEncoder, mockEnvUtil);
    }

    @Test
    public void testWhenDisabled() {
        setup(0);

        final SystemUser user = new SystemUser();

        changePasswordService.setUserPassword(user, SAME_PASSWORD);
        changePasswordService.setUserPassword(user, SAME_PASSWORD);
    }

    @Test(expected = RecentlyUsedPasswordException.class)
    public void testSamePasswordTwice() {
        setup(1);

        final SystemUser user = new SystemUser();

        changePasswordService.setUserPassword(user, SAME_PASSWORD);
        changePasswordService.setUserPassword(user, SAME_PASSWORD);
    }

    @Test
    public void testForgetOldPasswords()  {
        setup(2);

        final SystemUser user = new SystemUser();

        changePasswordService.setUserPassword(user, SAME_PASSWORD);
        changePasswordService.setUserPassword(user, SAME_PASSWORD + "1");
        changePasswordService.setUserPassword(user, SAME_PASSWORD + "2");
        changePasswordService.setUserPassword(user, SAME_PASSWORD);
    }
}