package fi.vincit.feature.account.support;

import com.jayway.jsonassert.JsonAssert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.security.core.token.Token;
import org.springframework.security.core.token.TokenService;

import java.net.URI;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class PasswordResetTokenLinkBuilderTest {

    @Mock
    private TokenService tokenService;

    @Mock
    private Token token;

    private PasswordResetTokenLink.Builder tokenLinkBuilder;

    @Before
    public void before() {
        when(token.getKey()).thenReturn("ABBACD");
        when(token.getExtendedInformation()).thenReturn("");
        when(tokenService.allocateToken(anyString())).thenReturn(token);

        this.tokenLinkBuilder = PasswordResetTokenLink.builder(tokenService);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testBaseUriRequired() {
        this.tokenLinkBuilder
                .withSystemUserId(1L)
                .buildLinkURI();
    }

    @Test(expected = IllegalStateException.class)
    public void testUserRequired() {
        this.tokenLinkBuilder
                .withUriBase(URI.create("http://localhost"))
                .buildLinkURI();
    }

    @Test
    public void testBuildBasicUri() {
        URI uri = this.tokenLinkBuilder
                .withUriBase(URI.create("http://localhost"))
                .withSystemUserId(1L)
                .buildLinkURI();

        assertThat(uri).as("No return value").isNotNull();
        assertThat(uri.toString()).isEqualTo("http://localhost");
    }

    @Test
    public void testBuildBasicUriWithTokenInQuery() {
        URI uri = this.tokenLinkBuilder
                .withUriBase(URI.create("http://localhost"))
                .withUriQuery("token={key}")
                .withSystemUserId(1L)
                .buildLinkURI();

        assertThat(uri.toString()).isEqualTo("http://localhost?token=ABBACD");
    }

    @Test
    public void testBuildComplicatedUriWithTokenInFragment() {
        URI uri = this.tokenLinkBuilder
                .withUriBase(URI.create("http://localhost"))
                .withUriQuery("type=reset")
                .withUriPath("/client")
                .withUriFragment("/login/{key}")
                .withSystemUserId(1L)
                .buildLinkURI();

        assertThat(uri.toString()).isEqualTo("http://localhost/client?type=reset#/login/ABBACD");
    }

    @Test
    public void testTokenExtendedInformation() {
        this.tokenLinkBuilder
                .withUriBase(URI.create("http://localhost"))
                .withSystemUserId(1L)
                .buildLinkURI();

        ArgumentCaptor<String> captor = ArgumentCaptor.forClass(String.class);
        verify(tokenService, times(1)).allocateToken(captor.capture());
        JsonAssert.with(captor.getValue()).assertEquals("u", "1").assertEquals("$.$", "reset-password");
        verify(token, times(1)).getKey();
        verifyNoMoreInteractions(tokenService, token);
    }
}
