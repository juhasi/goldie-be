package fi.vincit.feature.account;

import fi.vincit.EmbeddedDatabaseTest;
import fi.vincit.feature.account.entity.SystemUser;
import org.junit.Test;

import javax.persistence.PersistenceException;

public class DatabaseSmokeTest extends EmbeddedDatabaseTest {

    @Test
    public void testPersistUser() {
        createNewUser("test", SystemUser.Role.ROLE_USER);

        persist();
    }

    @Test(expected = PersistenceException.class)
    public void testPersistDuplicateUsername() {
        createNewUser("test", SystemUser.Role.ROLE_USER);
        createNewUser("test", SystemUser.Role.ROLE_USER);

        persist();
    }

}
