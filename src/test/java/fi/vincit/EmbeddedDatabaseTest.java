package fi.vincit;

import fi.vincit.config.Constants;
import fi.vincit.feature.account.entity.SystemUser;
import fi.vincit.feature.account.service.ActiveUserService;
import fi.vincit.test.DatabaseCleaner;
import fi.vincit.test.EntityPersister;
import fi.vincit.test.EntitySupplier;
import fi.vincit.test.TransactionalTaskExecutor;

import org.junit.Before;
import org.junit.runner.RunWith;
import org.springframework.context.ApplicationContext;
import org.springframework.data.domain.Persistable;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;

import javax.annotation.Resource;

import java.util.Random;
import java.util.concurrent.Callable;
import java.util.concurrent.ThreadLocalRandom;

// This base test class being non-transactional is a distinct design decision.
@TransactionConfiguration(defaultRollback = false)
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = EmbeddedDatabaseTestContext.class)
@ActiveProfiles(Constants.EMBEDDED_DATABASE)
public abstract class EmbeddedDatabaseTest {

    private final EntitySupplier entities = new EntitySupplier();

    @Resource
    private ApplicationContext applicationContext;

    @Resource
    private ActiveUserService activeUserService;

    @Resource
    private PasswordEncoder passwordEncoder;

    @Resource
    private EntityPersister persister;

    @Resource
    private TransactionalTaskExecutor txExecutor;

    @Resource
    private DatabaseCleaner dbCleaner;

    @Before
    public void initTest() {
        entities.initialize();
        dbCleaner.clearManagedEntityTablesFromH2Database();
    }

    protected SystemUser createNewUser(final String username, final SystemUser.Role role) {
        return entities.newUser(username, role, passwordEncoder);
    }

    protected void authenticate(final SystemUser user) {
        activeUserService.loginWithoutCheck(user);
    }

    protected void persist() {
        persister.persist(entities.get());
        entities.clear();
    }

    protected void persist(final Iterable<? extends Persistable<?>> entities) {
        persister.persist(entities);
    }

    protected void persistRequireOpenTransaction() {
        persister.persistRequireOpenTransaction(entities.get());
        entities.clear();
    }

    protected void persistRequireOpenTransaction(final Iterable<? extends Persistable<?>> entities) {
        persister.persistRequireOpenTransaction(entities);
    }

    protected void runInTransaction(final Runnable runnable) {
        txExecutor.execute(runnable);
    }

    protected <T> T callInTransaction(final Callable<T> callable) {
        return txExecutor.execute(callable);
    }

    // Getters -->

    protected Random random() {
        return ThreadLocalRandom.current();
    }

    protected EntitySupplier model() {
        return entities;
    }

    protected ApplicationContext applicationContext() {
        return applicationContext;
    }

}
