package fi.vincit;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

@Configuration
@Import(GoldieApplicationContext.class)
@ComponentScan("fi.vincit.test")
public class EmbeddedDatabaseTestContext {
}
