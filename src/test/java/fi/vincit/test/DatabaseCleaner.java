package fi.vincit.test;

import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaDelete;
import javax.persistence.metamodel.EntityType;

@Component
public class DatabaseCleaner {

    @PersistenceContext
    private EntityManager em;

    @Transactional
    public void clearManagedEntityTablesFromH2Database() {

        // Disable foreign key checks.
        em.createNativeQuery("SET REFERENTIAL_INTEGRITY FALSE").executeUpdate();

        // We do not want to truncate tables not mapped to JPA entity graph.

        for (final EntityType<?> entityType : em.getMetamodel().getEntities()) {
            em.createQuery(criteriaDelete(entityType.getJavaType(), em.getCriteriaBuilder())).executeUpdate();
        }

        em.createNativeQuery("SET REFERENTIAL_INTEGRITY TRUE").executeUpdate();
    }

    private static <T> CriteriaDelete<T> criteriaDelete(final Class<T> entityJavaType, final CriteriaBuilder cb) {
        final CriteriaDelete<T> deleteCrit = cb.createCriteriaDelete(entityJavaType);
        deleteCrit.from(entityJavaType);
        return deleteCrit;
    }

}
