package fi.vincit.test;

import com.google.common.collect.Lists;

import fi.vincit.feature.account.entity.SystemUser;

import org.springframework.data.domain.Persistable;
import org.springframework.security.crypto.password.PasswordEncoder;

import javax.annotation.Nonnull;

import java.util.List;
import java.util.Objects;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * This class can be used to create @Entity annotated objects or graphs for test
 * code.
 */
public class EntitySupplier {

    private static final int SEED = 1;

    private final AtomicInteger serial = new AtomicInteger(SEED);
    private final List<Persistable<?>> list = Lists.newLinkedList();

    public SystemUser newUser(String username, SystemUser.Role role, PasswordEncoder passwordEncoder) {
        SystemUser user = new SystemUser();

        user.setUsername(username);
        user.setRole(role);
        user.setActive(true);
        user.setPasswordAsPlaintext(String.format("%12d", serial()), passwordEncoder);

        return add(user);
    }

    public final void initialize() {
        clear();
        serial.set(SEED);
    }

    public final <T extends Persistable<?>> T add(@Nonnull final T object) {
        list.add(Objects.requireNonNull(object));
        serial();
        return object;
    }

    public final void clear() {
        list.clear();
    }

    protected final int serial() {
        return serial.getAndIncrement();
    }

    public final List<Persistable<?>> get() {
        return list;
    }
}
