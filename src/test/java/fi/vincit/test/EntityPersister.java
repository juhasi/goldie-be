/*
 * Copyright 2012-2014 Jarkko Kaura
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package fi.vincit.test;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Persistable;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Nonnull;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import java.util.Objects;

/**
 * A Spring bean to be used for persisting one or multiple entities in a single
 * Spring-managed transaction.
 *
 * @author Jarkko Kaura
 */
@Component
public class EntityPersister {

    private static final Logger LOG = LoggerFactory.getLogger(EntityPersister.class);

    @PersistenceContext
    private EntityManager entityManager;

    @Transactional
    public void persist(@Nonnull final Persistable<?> entity) {
        saveAndFlush(entity);
    }

    @Transactional
    public void persist(@Nonnull final Iterable<? extends Persistable<?>> entities) {
        saveAndFlush(entities);
    }

    @Transactional(propagation = Propagation.MANDATORY, noRollbackFor = RuntimeException.class)
    public void persistRequireOpenTransaction(@Nonnull final Persistable<?> entity) {
        saveAndFlush(entity);
    }

    @Transactional(propagation = Propagation.MANDATORY, noRollbackFor = RuntimeException.class)
    public void persistRequireOpenTransaction(@Nonnull final Iterable<? extends Persistable<?>> entities) {
        saveAndFlush(entities);
    }

    private void saveAndFlush(final Persistable<?> entity) {
        entityManager.persist(Objects.requireNonNull(entity));
        entityManager.flush();
        LOG.debug("Inserted {} entity with ID {} into database.", entity.getClass().getSimpleName(), entity.getId());
    }

    private void saveAndFlush(final Iterable<? extends Persistable<?>> entities) {
        int count = 0;
        for (final Persistable<?> entity : Objects.requireNonNull(entities)) {
            entityManager.persist(entity);
            count++;
        }
        if (count > 0) {
            entityManager.flush();
        }
        LOG.debug("Inserted {} entities into database.", count);
    }

}
