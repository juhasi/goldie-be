package fi.vincit.security;

public enum EntityPermission {
    CREATE,
    READ,
    LIST,
    UPDATE,
    DELETE
}
