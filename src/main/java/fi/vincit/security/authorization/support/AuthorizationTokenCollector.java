package fi.vincit.security.authorization.support;

import com.google.common.base.Preconditions;
import com.google.common.collect.Sets;
import org.springframework.security.access.hierarchicalroles.RoleHierarchy;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.authority.AuthorityUtils;

import java.util.Collections;
import java.util.Set;

public final class AuthorizationTokenCollector {
    private final Set<String> acquiredTokens = Sets.newHashSet();
    private final AuthorizationTokenHelper authorizationTokenHelper;

    public AuthorizationTokenCollector(final Authentication authentication,
                                       final RoleHierarchy roleHierarchy,
                                       final AuthorizationTokenHelper authorizationTokenHelper) {
        this.authorizationTokenHelper = authorizationTokenHelper;

        // Initialize default role authorization tokens using active user role and role hierarchy
        this.acquiredTokens.addAll(AuthorityUtils.authorityListToSet(
                roleHierarchy.getReachableGrantedAuthorities(authentication.getAuthorities())));
    }

    public <E extends Enum<E>> void addAuthorizationRole(final E authorizationRole) {
        this.acquiredTokens.add(AuthorizationTokenHelper.getCanonicalAuthorizationToken(
                Preconditions.checkNotNull(authorizationRole, "authorizationRole is null")));
    }

    public boolean hasPermission(final Object permission) {
        return authorizationTokenHelper.hasPermission(permission, acquiredTokens);
    }

    public Set<String> getAcquiredTokens() {
        return Collections.unmodifiableSet(this.acquiredTokens);
    }
}
