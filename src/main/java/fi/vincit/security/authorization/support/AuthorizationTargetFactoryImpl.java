package fi.vincit.security.authorization.support;

import com.google.common.base.Preconditions;
import fi.vincit.security.authorization.api.AuthorizationTargetFactory;
import fi.vincit.security.authorization.api.EntityAuthorizationTarget;
import fi.vincit.feature.common.entity.BaseEntityDTO;
import org.apache.commons.lang.reflect.MethodUtils;
import org.hibernate.proxy.HibernateProxyHelper;
import org.springframework.data.domain.Persistable;
import org.springframework.stereotype.Component;

import java.io.Serializable;
import java.lang.reflect.Method;

@Component
public class AuthorizationTargetFactoryImpl implements AuthorizationTargetFactory {
    @Override
    public EntityAuthorizationTarget create(final Object targetDomainObject) {
        if (targetDomainObject == null) {
            return null;

        } else if (targetDomainObject instanceof Class<?>) {
            return forClass((Class<?>) targetDomainObject);

        } else if (targetDomainObject instanceof String) {
            return this.decodeFromString((String) targetDomainObject);

        } else if (targetDomainObject instanceof EntityAuthorizationTarget) {
            return (EntityAuthorizationTarget) targetDomainObject;

        } else if (targetDomainObject instanceof Persistable) {
            return this.forPersistentEntity((Persistable<?>) targetDomainObject);

        } else if (targetDomainObject instanceof BaseEntityDTO) {
            return this.forDTO((BaseEntityDTO<?>) targetDomainObject);

        } else {
            return this.forObject(targetDomainObject);
        }
    }

    @Override
    public EntityAuthorizationTarget create(String type, Serializable primaryKey) {
        return new AuthorizationTargetImpl(type, primaryKey, null, null);
    }

    EntityAuthorizationTarget forPersistentEntity(Persistable<?> persistentEntity) {
        Preconditions.checkNotNull(persistentEntity);

        final Class<?> typeClass = HibernateProxyHelper.getClassWithoutInitializingProxy(persistentEntity);
        final String type = getTypeNameFor(typeClass);

        return new AuthorizationTargetImpl(type, persistentEntity.getId(), typeClass, persistentEntity);
    }

    EntityAuthorizationTarget forDTO(BaseEntityDTO<?> dto) {
        Preconditions.checkNotNull(dto);

        final String type = getTypeNameFor(dto.getClass());

        return new AuthorizationTargetImpl(type, dto.getId(), dto.getClass(), dto);
    }

    AuthorizationTargetImpl<Object> forClass(final Class<?> targetClass) {
        return new AuthorizationTargetImpl(targetClass.getSimpleName(), null, targetClass, null);
    }

    EntityAuthorizationTarget forObject(Object object) {
        Preconditions.checkNotNull(object);

        final String type = getTypeNameFor(object.getClass());
        final Serializable primaryKey = getPrimaryKeyForInstance(object);

        return new AuthorizationTargetImpl(type, primaryKey, object.getClass(), object);
    }

    EntityAuthorizationTarget decodeFromString(final String stringEntityReference) {
        final String[] parts = stringEntityReference.split(":");

        final String targetType = (parts.length > 0) ? parts[0].trim() : null;
        final Long targetId = (parts.length > 1) ? Long.valueOf(parts[1]) : null;

        if (targetType == null || targetType.isEmpty()) {
            return null;
        }

        return new AuthorizationTargetImpl(targetType, targetId, null, null);
    }

    static String getTypeNameFor(Class<?> typeClass) {
        return typeClass.getCanonicalName();
    }

    static Serializable getPrimaryKeyForInstance(Object instance) {
        final Class<?> typeClass = HibernateProxyHelper.getClassWithoutInitializingProxy(instance);

        final Method method = MethodUtils.getAccessibleMethod(typeClass, "getId", new Class[]{});

        if (method == null) {
            return null;
        }

        final Object primaryKey;
        try {
            primaryKey = method.invoke(instance);

            if (primaryKey instanceof Serializable) {
                return (Serializable) primaryKey;
            }
            throw new IllegalArgumentException("Primary key should be serializable");
        } catch (Exception e) {
            throw new RuntimeException("Could not execute entity getId() method", e);
        }
    }
}
