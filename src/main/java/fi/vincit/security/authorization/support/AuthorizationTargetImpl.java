package fi.vincit.security.authorization.support;

import com.google.common.base.MoreObjects;
import com.google.common.base.Preconditions;
import fi.vincit.security.authorization.api.EntityAuthorizationTarget;

import java.io.Serializable;

final class AuthorizationTargetImpl<T> implements EntityAuthorizationTarget {
    private final String type;
    private final Serializable id;
    private final Class<T> clazz;
    private final T target;

    AuthorizationTargetImpl(String type, Serializable primaryKey, Class<T> clazz, T targetObject) {
        Preconditions.checkNotNull(type);

        this.target = targetObject;
        this.id = primaryKey;
        this.clazz = clazz;
        this.type = type;
    }

    @Override
    public String getAuthorizationTargetName() {
        return type;
    }

    @Override
    public Serializable getAuthorizationTargetId() {
        return id;
    }

    @Override
    public <P> P getAuthorizationTarget(Class<P> type) {
        Preconditions.checkNotNull(type);

        if (this.target == null) {
            return null;
        }

        if (type.isInstance(target)) {
            return (P) this.target;
        }

        return null;
    }

    @Override
    public Class<T> getAuthorizationTargetClass() {
        return this.clazz;
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this).add("type", type).add("id", id).add("class", clazz).toString();
    }
}
