package fi.vincit.security.authorization;

import fi.vincit.feature.account.dto.AccountDTO;
import fi.vincit.feature.account.dto.SystemUserDTO;
import fi.vincit.feature.account.entity.SystemUser;
import fi.vincit.security.UserInfo;
import fi.vincit.security.authorization.api.EntityAuthorizationTarget;
import fi.vincit.security.authorization.support.AuthorizationTokenCollector;
import org.springframework.stereotype.Component;
import org.springframework.util.ObjectUtils;

/**
 * Authorization strategy to make decision on SystemUser domain entity.
 */
@Component
public class UserEntityAuthorization extends AbstractEntityAuthorization {
    public static final String CHANGE_PASSWORD = "changepassword";

    public static enum Role {
        SELF
    }

    UserEntityAuthorization() {
        super("SystemUser");

        allow(READ, UserEntityAuthorization.Role.SELF);
        allow(UPDATE, UserEntityAuthorization.Role.SELF);
        allow(CHANGE_PASSWORD, UserEntityAuthorization.Role.SELF);

        allow(READ, SystemUser.Role.ROLE_ADMIN);
        allow(UPDATE, SystemUser.Role.ROLE_ADMIN);
        allow(CREATE, SystemUser.Role.ROLE_ADMIN);
        allow(DELETE, SystemUser.Role.ROLE_ADMIN);

        allow(LIST, SystemUser.Role.ROLE_ADMIN);
        allow(CHANGE_PASSWORD, SystemUser.Role.ROLE_ADMIN);
    }

    @Override
    protected void authorizeTarget(final AuthorizationTokenCollector collector,
                                   final EntityAuthorizationTarget target,
                                   final UserInfo userInfo) {
        // Check if target entity is active user?
        if (ObjectUtils.nullSafeEquals(target.getAuthorizationTargetId(), userInfo.getUserId())) {
            // Authorizing operation for active user
            collector.addAuthorizationRole(UserEntityAuthorization.Role.SELF);
        }
    }

    @Override
    public Class[] getSupportedTypes() {
        return new Class[]{SystemUser.class, SystemUserDTO.class, AccountDTO.class};
    }
}