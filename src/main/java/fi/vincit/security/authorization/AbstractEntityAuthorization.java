package fi.vincit.security.authorization;

import fi.vincit.security.UserInfo;
import fi.vincit.security.authorization.api.EntityAuthorizationTarget;
import fi.vincit.security.authorization.spi.AuthorizationAuditListener;
import fi.vincit.security.authorization.spi.EntityAuthorizationStrategy;
import fi.vincit.security.authorization.support.AuthorizationTokenCollector;
import fi.vincit.security.authorization.support.AuthorizationTokenHelper;
import org.springframework.security.access.hierarchicalroles.RoleHierarchy;
import org.springframework.security.core.Authentication;

import javax.annotation.Resource;

public abstract class AbstractEntityAuthorization
        implements EntityAuthorizationStrategy {

    @Resource
    private AuthorizationAuditListener auditListener;

    @Resource
    private RoleHierarchy roleHierarchy;

    /**
     * Configuration helper stores mappings from
     * permission names to granted entity role token
     */
    private final AuthorizationTokenHelper authorizationTokenHelper;

    private final String supportedEntityName;

    protected AbstractEntityAuthorization(String name) {
        this.authorizationTokenHelper = new AuthorizationTokenHelper(name);
        this.supportedEntityName = name;
    }

    protected <E extends Enum<E>>
    void allow(final Object permission, final E token) {
        this.authorizationTokenHelper.grant(permission, token);
    }

    @Override
    public String getEntityName() {
        return supportedEntityName;
    }

    @Override
    public abstract Class[] getSupportedTypes();

    @Override
    public boolean hasPermission(final EntityAuthorizationTarget target,
                                 final Object permission,
                                 final Authentication authentication) {

        final AuthorizationTokenCollector tokenCollector = new AuthorizationTokenCollector(
                authentication, roleHierarchy, authorizationTokenHelper);

        // Extract user details
        final UserInfo userInfo = authentication.isAuthenticated() ? UserInfo.extractFrom(authentication) : null;

        // Check permission
        final boolean accessGranted = hasPermission(target, permission, userInfo, tokenCollector);

        // Audit event
        auditListener.onAccessDecision(accessGranted, permission, target,
                tokenCollector.getAcquiredTokens(), userInfo);

        return accessGranted;
    }

    private boolean hasPermission(final EntityAuthorizationTarget target,
                                  final Object permission,
                                  final UserInfo userInfo,
                                  final AuthorizationTokenCollector tokenCollector) {
        if (tokenCollector.hasPermission(permission)) {
            return true;

        } else if (userInfo != null) {
            this.authorizeTarget(tokenCollector, target, userInfo);

            return tokenCollector.hasPermission(permission);

        } else {
            return false;
        }
    }

    /**
     * Enumerate any additional roles which apply to relationship
     * with authenticated user and supported entity instance.
     */
    protected abstract void authorizeTarget(final AuthorizationTokenCollector collector,
                                            final EntityAuthorizationTarget target,
                                            final UserInfo userInfo);
}