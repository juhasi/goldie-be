package fi.vincit.security.authorization.spi;

import fi.vincit.security.UserInfo;
import fi.vincit.security.authorization.api.EntityAuthorizationTarget;

import java.util.Set;

public interface AuthorizationAuditListener {
    public void onAccessDecision(
            final boolean granted, final Object permission,
            final EntityAuthorizationTarget target,
            final Set<String> acquiredTokens,
            final UserInfo userInfo);
}
