package fi.vincit.security.csrf;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.web.csrf.CsrfToken;
import org.springframework.security.web.csrf.CsrfTokenRepository;
import org.springframework.security.web.csrf.DefaultCsrfToken;
import org.springframework.web.util.WebUtils;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.UUID;

/**
CSRF token is stored in Cookie and received using special request header. For background see:
http://docs.spring.io/spring-security/site/docs/3.2.0.CI-SNAPSHOT/reference/html/csrf.html

Also see section Cross Site Request Forgery (XSRF) Protection at:
https://docs.angularjs.org/api/ng/service/$http
 */
public class CookieCsrfTokenRepository implements CsrfTokenRepository {
    private static final Logger LOG = LoggerFactory.getLogger(CookieCsrfTokenRepository.class);

    private static final String DEFAULT_PARAMETER_NAME = "_csrf";
    private static final String ANGULAR_CSRF_DEFAULT_COOKIE_NAME = "XSRF-TOKEN";
    private static final String ANGULAR_CSRF_DEFAULT_HEADER_NAME = "X-XSRF-TOKEN";

    private String cookieName = ANGULAR_CSRF_DEFAULT_COOKIE_NAME;

    @Override
    public CsrfToken generateToken(HttpServletRequest request) {
        return createCsrfToken(UUID.randomUUID().toString());
    }

    private static DefaultCsrfToken createCsrfToken(String value) {
        final String headerName = ANGULAR_CSRF_DEFAULT_HEADER_NAME;
        final String parameterName = DEFAULT_PARAMETER_NAME;

        return new DefaultCsrfToken(headerName, parameterName, value);
    }

    @Override
    public CsrfToken loadToken(HttpServletRequest request) {
        final CsrfToken csrfToken = getCSRFCookie(request);

        if (csrfToken != null) {
            LOG.debug("Loaded CSRF token {}", csrfToken.getToken());
        } else {
            LOG.debug("No CSRF token stored");
        }

        return csrfToken;
    }

    @Override
    public void saveToken(CsrfToken token,
                          HttpServletRequest request,
                          HttpServletResponse response) {
        if (token != null) {
            LOG.debug("Saving CSRF token={}", token.getToken());

            response.addCookie(createHttpCookie(request, token));
        }
    }

    private Cookie createHttpCookie(HttpServletRequest request, CsrfToken token) {
        Cookie cookie = new Cookie(cookieName, token.getToken());
        cookie.setMaxAge(-1);
        cookie.setHttpOnly(false);
        cookie.setSecure(request.isSecure());
        cookie.setPath(getCookiePath(request));

        return cookie;
    }

    private static String getCookiePath(final HttpServletRequest request) {
        final String contextPath = request.getContextPath();

        return contextPath.length() > 0 ? contextPath : "/";
    }

    public DefaultCsrfToken getCSRFCookie(HttpServletRequest request) {
        final Cookie cookie = WebUtils.getCookie(request, cookieName);

        if (cookie != null && cookie.getValue() != null) {
            return createCsrfToken(cookie.getValue());
        }

        return null;
    }
}
