package fi.vincit.security.crypto;

import com.google.common.io.BaseEncoding;
import org.springframework.security.crypto.codec.Utf8;
import org.springframework.security.crypto.encrypt.BytesEncryptor;
import org.springframework.security.crypto.encrypt.TextEncryptor;

public class SafeBase64TextEncryptor implements TextEncryptor {
    private final BytesEncryptor bytesEncryptor;

    public SafeBase64TextEncryptor(BytesEncryptor bytesEncryptor) {
        this.bytesEncryptor = bytesEncryptor;
    }

    @Override
    public String encrypt(String text) {
        return BaseEncoding.base64Url().encode(bytesEncryptor.encrypt(Utf8.encode(text)));
    }

    @Override
    public String decrypt(String encryptedText) {
        return Utf8.decode(bytesEncryptor.decrypt(BaseEncoding.base64Url().decode(encryptedText)));
    }
}
