package fi.vincit.security.authentication;

import fi.vincit.feature.account.dao.UserRepository;
import fi.vincit.feature.account.entity.SystemUser;
import fi.vincit.security.UserInfo;
import org.springframework.dao.DataAccessException;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;

public class CustomUserDetailsService implements UserDetailsService {
    @Resource
    private UserRepository userRepository;

    /**
     * @param username the username identifying the user whose data is required.
     *
     * @return a fully populated user record (never <code>null</code>)
     *
     * @throws org.springframework.security.core.userdetails.UsernameNotFoundException if the user could not be found or the user has no GrantedAuthority
     */
    @Override
    @Transactional(readOnly = true, noRollbackFor = RuntimeException.class)
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        if (username == null) {
            throw new UsernameNotFoundException("Invalid empty login username");
        }

        try {
            final SystemUser user = userRepository.findByUsername(username);

            if (user != null) {
                return new UserInfo.UserInfoBuilder(user).createUserInfo();
            }

        } catch (DataAccessException dae) {
            throw new UsernameNotFoundException("Could not lookup user", dae);
        }

        throw new UsernameNotFoundException("No such username");
    }
}
