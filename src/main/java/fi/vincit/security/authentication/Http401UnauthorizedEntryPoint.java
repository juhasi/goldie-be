package fi.vincit.security.authentication;

import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.AuthenticationEntryPoint;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.io.IOException;

public class Http401UnauthorizedEntryPoint implements AuthenticationEntryPoint {
    /**
     * Always returns a 401 error code to the client.
     */
    @Override
    public void commence(HttpServletRequest request, HttpServletResponse response, AuthenticationException arg2)
            throws IOException {

        //response.sendError(HttpServletResponse.SC_UNAUTHORIZED, "Access Denied");
        response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);

        if (!response.isCommitted()) {
            response.setContentType("application/json");
            response.getWriter().print("{\"status\": \"UNAUTHORIZED\"}");
            response.getWriter().flush();
            response.getWriter().close();
        }
    }

}
