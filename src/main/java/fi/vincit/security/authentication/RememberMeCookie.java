package fi.vincit.security.authentication;

import com.google.common.base.Preconditions;
import fi.vincit.feature.account.entity.PersistentRememberMeToken;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.springframework.security.crypto.codec.Base64;
import org.springframework.security.crypto.keygen.StringKeyGenerator;
import org.springframework.security.web.authentication.rememberme.InvalidCookieException;
import org.springframework.util.StringUtils;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Arrays;

public final class RememberMeCookie {
    public static final String HTTP_COOKIE_NAME = "SPRING_SECURITY_REMEMBER_ME_COOKIE";
    public static final String HTTP_COOKIE_DELIMITER = ":";
    public static final int RANDOM_BIT_COUNT = 8;

    public static RememberMeCookie create(final StringKeyGenerator keyGenerator) {
        final String series = keyGenerator.generateKey();
        final String value = keyGenerator.generateKey();

        return new RememberMeCookie(series, value);
    }

    /**
     * Sets a "cancel cookie" (with maxAge = 0) on the response to disable persistent logins.
     */
    public static void cancelCookie(final HttpServletRequest request, final HttpServletResponse response) {
        final Cookie cookie = new Cookie(RememberMeCookie.HTTP_COOKIE_NAME, null);
        cookie.setMaxAge(0);
        cookie.setPath(getCookiePath(request));

        response.addCookie(cookie);
    }

    public static RememberMeCookie forPersistentCookie(final PersistentRememberMeToken pToken) {
        return new RememberMeCookie(pToken.getSeries(), pToken.getTokenValue());
    }

    public static RememberMeCookie forHttpRequest(final HttpServletRequest request) throws InvalidCookieException {
        return RememberMeCookie.forHttpCookie(findCookie(request));
    }

    public static RememberMeCookie forHttpCookie(final Cookie httpCookie) throws InvalidCookieException {
        if (httpCookie == null) {
            return null;
        }

        if (httpCookie.getValue() == null || httpCookie.getValue().isEmpty()) {
            throw new InvalidCookieException("Cookie content is empty");
        }

        final StringBuilder cookieValue = new StringBuilder(httpCookie.getValue());
        for (int j = 0; j < cookieValue.length() % 4; j++) {
            cookieValue.append('=');
        }

        final byte[] cookieBytes = cookieValue.toString().getBytes();
        if (!Base64.isBase64(cookieBytes)) {
            throw new InvalidCookieException("Cookie token was not Base64 encoded; value was '" + cookieValue + "'");
        }

        final String cookieAsPlainText = new String(Base64.decode(cookieBytes));
        final String[] tokens = StringUtils.delimitedListToStringArray(cookieAsPlainText, HTTP_COOKIE_DELIMITER);

        if (tokens.length != 2) {
            throw new InvalidCookieException("Cookie token did not contain " + 2 +
                    " tokens, but contained '" + Arrays.asList(tokens) + "'");
        }

        return new RememberMeCookie(tokens[0], tokens[1]);
    }

    private static Cookie findCookie(HttpServletRequest request) {
        if (request.getCookies() != null) {
            for (final Cookie cookie : request.getCookies()) {
                if (RememberMeCookie.HTTP_COOKIE_NAME.equals(cookie.getName())) {
                    return cookie;
                }
            }
        }

        return null;
    }

    private static String getCookiePath(final HttpServletRequest request) {
        final String contextPath = request.getContextPath();
        return contextPath.length() > 0 ? contextPath : "/";
    }

    private final String series;

    private final String value;

    public RememberMeCookie(final String series, final String value) {
        this.series = Preconditions.checkNotNull(series, "Empty series");
        this.value = Preconditions.checkNotNull(value, "Empty value");
    }

    public RememberMeCookie refresh(final StringKeyGenerator keyGenerator) {
        return new RememberMeCookie(this.series, keyGenerator.generateKey());
    }

    public boolean matches(final PersistentRememberMeToken pToken) {
        return pToken != null && this.value.equals(pToken.getTokenValue());
    }

    public String getSeries() {
        return series;
    }

    public String getValue() {
        return value;
    }

    public Cookie asHttpCookie(final HttpServletRequest request, final int maxAge) {
        final Cookie cookie = new Cookie(HTTP_COOKIE_NAME, encodeHttpCookieValue());
        cookie.setMaxAge(maxAge);
        cookie.setPath(getCookiePath(request));
        cookie.setSecure(request.isSecure());
        cookie.setHttpOnly(true);

        return cookie;
    }

    private String encodeHttpCookieValue() {
        final String content = this.series + HTTP_COOKIE_DELIMITER + this.value;
        final String base64 = new String(Base64.encode(content.getBytes()));

        return StringUtils.trimTrailingCharacter(base64, '=');
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("series", series).toString();
    }
}
