package fi.vincit.security.authentication;

import fi.vincit.config.properties.SecurityConfigProperties;
import fi.vincit.feature.account.service.PersistentRememberMeTokenService;
import org.apache.commons.lang.BooleanUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.DataAccessException;
import org.springframework.security.authentication.AccountStatusUserDetailsChecker;
import org.springframework.security.authentication.AuthenticationDetailsSource;
import org.springframework.security.authentication.RememberMeAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsChecker;
import org.springframework.security.web.authentication.RememberMeServices;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.security.web.authentication.logout.LogoutHandler;
import org.springframework.security.web.authentication.rememberme.InvalidCookieException;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Custom implementation of Spring Security's RememberMeServices.
 * source: https://github.com/jdubois/generator-jhipster
 * <p/>
 * Persistent tokens are used by Spring Security to automatically log in users.
 * <p/>
 * This is a specific implementation of Spring Security's remember-me authentication, but it is much
 * more powerful than the standard implementations:
 * <ul>
 * <li>It allows a user to see the list of his currently opened sessions, and invalidate them</li>
 * <li>It stores more information, such as the IP address and the user agent, for audit purposes<li>
 * </ul>
 * <p/>
 * This is inspired by:
 * <ul>
 * <li><a href="http://jaspan.com/improved_persistent_login_cookie_best_practice">
 * Improved Persistent Login Cookie Best Practice</a></li>
 * <li><a href="https://github.com/blog/1661-modeling-your-app-s-user-session">
 * Github's "Modeling your App's User Session"</a></li></li>
 * </ul>
 * <p/>
 * The main algorithm comes from Spring Security's PersistentTokenBasedRememberMeServices, but this class
 * couldn't be cleanly extended.
 * <p/>
 */
@Component
public class CustomPersistentRememberMeServices implements RememberMeServices, LogoutHandler {
    private static final Logger LOG = LoggerFactory.getLogger(CustomPersistentRememberMeServices.class);

    public static final String DEFAULT_PARAMETER = "_spring_security_remember_me";

    private final UserDetailsChecker userDetailsChecker = new AccountStatusUserDetailsChecker();
    private final AuthenticationDetailsSource<HttpServletRequest, ?>
            authenticationDetailsSource = new WebAuthenticationDetailsSource();

    @Resource
    private PersistentRememberMeTokenService rememberMeTokenService;

    @Resource
    private SecurityConfigProperties securityConfigurationProperties;

    @Override
    public void loginSuccess(HttpServletRequest request, HttpServletResponse response,
                             Authentication successfulAuthentication) {

        if (BooleanUtils.toBoolean(request.getParameter(DEFAULT_PARAMETER))) {
            try {
                final RememberMeCookie rememberMeCookie = rememberMeTokenService.create(
                        successfulAuthentication, request);

                if (rememberMeCookie != null) {
                    LOG.debug("Remember-me cookie created: {}", rememberMeCookie);

                    setCookie(request, response, rememberMeCookie);
                    return;
                }

            } catch (DataAccessException e) {
                LOG.error("Failed to save persistent token ", e);
            }
        }

        // Otherwise remove remember-me cookie if exists
        RememberMeCookie.cancelCookie(request, response);
    }

    @Override
    public void loginFail(HttpServletRequest request, HttpServletResponse response) {
        LOG.debug("Interactive login attempt was unsuccessful.");

        RememberMeCookie.cancelCookie(request, response);
    }

    @Override
    public Authentication autoLogin(HttpServletRequest request, HttpServletResponse response) {
        if (!request.getRequestURI().equals("/api/v1/account")) {
            return null;
        }

        final RememberMeCookie presentedCookie = getCookie(request, response);

        if (presentedCookie == null) {
            return null;
        }

        try {
            final UserDetails userDetails = rememberMeTokenService.validate(presentedCookie, request);
            userDetailsChecker.check(userDetails);

            final RememberMeCookie refreshedToken = rememberMeTokenService.update(presentedCookie, request);
            setCookie(request, response, refreshedToken);

            LOG.debug("Remember-me cookie accepted: series={}", presentedCookie.getSeries());

            // Init session manually, because SessionCreationPolicy.NEVER does not create it by default
            request.getSession(true);

            final RememberMeAuthenticationToken auth = new RememberMeAuthenticationToken(
                    securityConfigurationProperties.getRememberMeSecretKey(),
                    userDetails, userDetails.getAuthorities());
            auth.setDetails(authenticationDetailsSource.buildDetails(request));
            return auth;

        } catch (DataAccessException dae) {
            LOG.error("Failed to update token: ", dae);

        } catch (AuthenticationException e) {
            LOG.debug("Remember-me authentication failed", e);
        }

        try {
            rememberMeTokenService.delete(presentedCookie, true);

        } catch (DataAccessException dae) {
            LOG.error("Failed to remove persistentCookie", dae);

        } finally {
            RememberMeCookie.cancelCookie(request, response);
        }

        return null;
    }

    @Override
    public void logout(HttpServletRequest request, HttpServletResponse response, Authentication authentication) {
        final RememberMeCookie presentedCookie = getCookie(request, response);

        if (presentedCookie == null) {
            return;
        }

        try {
            rememberMeTokenService.delete(presentedCookie, false);

        } catch (DataAccessException e) {
            LOG.error("Failed to remove persistentCookie", e);

        } finally {
            RememberMeCookie.cancelCookie(request, response);
        }
    }

    private void setCookie(HttpServletRequest request, HttpServletResponse response, RememberMeCookie rememberMeCookie) {
        final int maxAge = securityConfigurationProperties.getRememberMeTokenValiditySeconds();
        response.addCookie(rememberMeCookie.asHttpCookie(request, maxAge));
    }

    private static RememberMeCookie getCookie(HttpServletRequest request, HttpServletResponse response) {
        try {
            return RememberMeCookie.forHttpRequest(request);

        } catch (InvalidCookieException invalidCookie) {
            LOG.debug("Invalid remember-me cookie: {}", invalidCookie.getMessage());

            RememberMeCookie.cancelCookie(request, response);
        }

        return null;
    }
}
