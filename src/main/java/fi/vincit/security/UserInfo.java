package fi.vincit.security;

import com.google.common.base.Preconditions;

import fi.vincit.feature.account.entity.SystemUser;

import org.apache.commons.lang.RandomStringUtils;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.web.authentication.preauth.PreAuthenticatedAuthenticationToken;
import org.springframework.util.Assert;

import java.util.Collections;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;

/**
 * User Info is serializable version of User domain object stored
 * in SecurityContextHolder during request processing.
 */
public class UserInfo extends org.springframework.security.core.userdetails.User {

    private static final long serialVersionUID = -2737203219146965683L;

    private final Long userId;
    private final String username;
    private final SystemUser.Role role;

    private final TimeZone timeZone;
    private Locale locale;

    private final String firstName;
    private final String lastName;

    public static UserInfo extractFrom(final Authentication authentication) {
        Preconditions.checkNotNull(authentication, "No authentication available");
        Preconditions.checkState(authentication.isAuthenticated(), "User is not authenticated");
        Preconditions.checkNotNull(authentication.getPrincipal(), "No principal for authentication");

        if (authentication.getPrincipal() instanceof UserInfo) {
            return UserInfo.class.cast(authentication.getPrincipal());
        }
        throw new IllegalStateException("Authenticate user principal type is unknown: "
                + authentication.getPrincipal().getClass().getSimpleName());
    }

    public static class UserInfoBuilder {
        private String username;
        private String password;
        private Long userId;
        private Locale locale;
        private TimeZone timeZone;
        private String firstName;
        private String lastName;
        private boolean enabled;
        private boolean accountNonExpired;
        private boolean credentialsNonExpired;
        private boolean accountNonLocked;
        private SystemUser.Role role;

        public UserInfoBuilder(final String username,
                               final Long userId,
                               final SystemUser.Role role) {
            this.username = username;
            this.userId = userId;
            this.role = role;
            this.password = RandomStringUtils.random(32);
        }

        public UserInfoBuilder(final SystemUser user) {
            this.username = user.getUsername();
            this.password = user.getHashedPassword();

            this.userId = user.getId();
            this.locale = user.getLocale();
            this.timeZone = user.getTimeZone();

            this.firstName = user.getFirstName();
            this.lastName = user.getLastName();

            this.enabled = user.isActive();
            this.accountNonExpired = user.isActive();
            this.credentialsNonExpired = user.isActive();
            this.accountNonLocked = user.isActive();
            this.role = user.getRole();
        }

        private List<GrantedAuthority> getAuthorities() {
            if (role == null) {
                return Collections.emptyList();
            }
            return AuthorityUtils.createAuthorityList(role.name());
        }

        /**
         * Create authenticated authentication token with granted authorities set
         *
         * @return
         */
        public Authentication createAuthentication() {
            return new PreAuthenticatedAuthenticationToken(new UserInfo(this), null, getAuthorities());
        }

        public UserInfo createUserInfo() {
            Assert.hasText(this.username);

            if (this.firstName == null && this.lastName == null) {
                this.firstName = this.username;
                this.lastName = this.username;
            }

            return new UserInfo(this);
        }
    }

    private UserInfo(final UserInfoBuilder builder) {
        super(builder.username, builder.password, builder.enabled, builder.accountNonExpired, builder.credentialsNonExpired,
                builder.accountNonLocked, builder.getAuthorities());
        this.username = builder.username;
        this.role = builder.role;
        this.userId = builder.userId;
        this.timeZone = builder.timeZone;
        this.locale = builder.locale;
        this.firstName = builder.firstName;
        this.lastName = builder.lastName;
    }

    public boolean isAdmin() {
        return this.role == SystemUser.Role.ROLE_ADMIN;
    }

    public Long getUserId() {
        return userId;
    }

    @Override
    public String getUsername() {
        return username;
    }

    public SystemUser.Role getRole() {
        return role;
    }

    public TimeZone getTimeZone() {
        return timeZone;
    }

    public Locale getLocale() {
        return locale;
    }

    public void setLocale(Locale locale) {
        this.locale = locale;
    }

    public String getLastName() {
        return lastName;
    }

    public String getFirstName() {
        return firstName;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).
                append("userId", userId).
                append("role", role).
                append("firstName", firstName).
                append("lastName", lastName).
                append("timeZone", timeZone).
                append("locale", locale).
                toString();
    }
}
