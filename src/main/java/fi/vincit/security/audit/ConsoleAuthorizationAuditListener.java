package fi.vincit.security.audit;

import fi.vincit.security.UserInfo;
import fi.vincit.security.authorization.spi.AuthorizationAuditListener;
import fi.vincit.security.authorization.api.EntityAuthorizationTarget;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.util.Set;

@Component
public class ConsoleAuthorizationAuditListener implements AuthorizationAuditListener {

    private static final Logger LOG = LoggerFactory.getLogger(ConsoleAuthorizationAuditListener.class);

    @Override
    public void onAccessDecision(final boolean granted, final Object permission,
                                 final EntityAuthorizationTarget target,
                                 final Set<String> acquiredTokens, final UserInfo userInfo) {
        final String logMessage = createLogMessage(granted, permission, target, userInfo);

        if (granted) {
            LOG.info(logMessage);
        } else {
            LOG.warn(logMessage);
        }
    }

    String createLogMessage(final boolean granted,
                            final Object permission,
                            final EntityAuthorizationTarget target,
                            final UserInfo userInfo) {
        final StringBuilder sb = new StringBuilder();
        buildPermissionPrefix(granted, permission, sb);
        sb.append(' ');
        buildUserIdentifier(userInfo, sb);
        sb.append(' ');
        buildTargetIdentifier(target, sb);

        return sb.toString();
    }

    void buildPermissionPrefix(final boolean granted, final Object permission, final StringBuilder sb) {
        sb.append(granted ? "Granted" : "Denied");
        sb.append(" '");
        sb.append(permission);
        sb.append("' permission");
    }

    void buildUserIdentifier(final UserInfo userInfo, final StringBuilder sb) {
        sb.append("for user ");
        if (userInfo != null) {
            sb.append("[id=");
            sb.append(userInfo.getUserId());
            sb.append(", username='");
            sb.append(userInfo.getUsername());
            sb.append("']");
        } else {
            sb.append("<anonymous>");
        }
    }

    void buildTargetIdentifier(final EntityAuthorizationTarget target, final StringBuilder sb) {
        if (target == null) {
            sb.append("for unknown target object");
        } else {
            sb.append("for target object ");
            sb.append("[type='");
            sb.append(target.getAuthorizationTargetName());
            sb.append("', id=");
            sb.append(target.getAuthorizationTargetId());
            sb.append("]");
        }
    }
}
