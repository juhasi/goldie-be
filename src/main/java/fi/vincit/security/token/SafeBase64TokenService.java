package fi.vincit.security.token;

import com.google.common.base.Charsets;
import com.google.common.base.Strings;
import com.google.common.hash.Hashing;
import fi.vincit.config.properties.SecurityConfigProperties;
import fi.vincit.security.crypto.Base64StringKeyGenerator;
import org.springframework.security.core.token.DefaultToken;
import org.springframework.security.core.token.Token;
import org.springframework.security.core.token.TokenService;
import org.springframework.security.crypto.encrypt.TextEncryptor;
import org.springframework.security.crypto.keygen.StringKeyGenerator;
import org.springframework.stereotype.Component;
import org.springframework.util.Assert;
import org.springframework.util.StringUtils;

import javax.annotation.Resource;
import java.util.Date;

/**
 * Avoid unsafe query URI characters such as + and / using URL-safe Base64.
 * Generated tokens are also little shorter because of SHA1 vs. SHA512 hashing.
 *
 * Original implementation is from Spring Security Core by Ben Alex.
 * @see org.springframework.security.core.token.KeyBasedPersistenceTokenService
 */
@Component
public class SafeBase64TokenService implements TokenService {
    public static final int RANDOM_BIT_COUNT = 8;

    @Resource
    private SecurityConfigProperties securityConfigurationProperties;

    @Resource(name = "tokenEncryptor")
    private TextEncryptor tokenEncryptor;

    private StringKeyGenerator pseudoRandomGenerator =
            Base64StringKeyGenerator.withBitLength(RANDOM_BIT_COUNT);

    private String encodeToken(final String content, final String signature) {
        return tokenEncryptor.encrypt(content + ":" + signature);
    }

    public String[] decodeToken(final String tokenData) {
        return StringUtils.delimitedListToStringArray(tokenEncryptor.decrypt(tokenData), ":");
    }

    @Override
    public Token allocateToken(String extendedInformation) {
        Assert.notNull(extendedInformation, "Must provided non-null extendedInformation (but it can be empty)");

        final long creationTime = new Date().getTime();

        final String content = Long.toString(creationTime)
                + ":" + pseudoRandomGenerator.generateKey()
                + ":" + extendedInformation;

        // Compute signature for content
        final String signature = computeSignature(creationTime, content);

        // Combine content and signature
        final String key = encodeToken(content, signature);

        return new DefaultToken(key, creationTime, extendedInformation);
    }

    @Override
    public Token verifyToken(final String key) {
        if (Strings.isNullOrEmpty(key)) {
            return null;
        }

        final String[] fields = decodeToken(key);

        Assert.isTrue(fields.length >= 4, "Expected 4 or more fields but found " + fields.length);

        final long creationTime = decodeCreationTime(fields[0]);
        final String pseudoRandomNumber = fields[1];
        final String extendedInformation = decodeExtendedInformation(fields);
        final String signature = fields[fields.length - 1];

        // Verification
        final String content = Long.toString(creationTime)
                + ":" + pseudoRandomNumber
                + ":" + extendedInformation;

        final String expectedTokenSignature = computeSignature(creationTime, content);

        Assert.isTrue(expectedTokenSignature.equals(signature), "Key verification failure");

        return new DefaultToken(key, creationTime, extendedInformation);
    }

    private static long decodeCreationTime(final String input) {
        try {
            return Long.decode(input);
        } catch (NumberFormatException nfe) {
            throw new IllegalArgumentException("Expected number but found " + input);
        }
    }

    private static String decodeExtendedInformation(final String[] tokens) {
        // Permit extendedInfoBuilder to itself contain ":" characters
        final StringBuilder extendedInfoBuilder = new StringBuilder();
        for (int i = 2; i < tokens.length - 1; i++) {
            if (i > 2) {
                extendedInfoBuilder.append(":");
            }
            extendedInfoBuilder.append(tokens[i]);
        }
        return extendedInfoBuilder.toString();
    }

    private String computeServerSecretApplicableAt(final long time) {
        final String serverSecretKey = securityConfigurationProperties.getTokenGenerationSecret();
        final int serverInteger = securityConfigurationProperties.getTokenGenerationInteger();

        return serverSecretKey + ":" + time % serverInteger;
    }

    private String computeSignature(final long creationTime, final String content) {
        return Hashing.sha1().newHasher()
                .putString(content + ":" + computeServerSecretApplicableAt(creationTime), Charsets.UTF_8)
                .hash().toString();
    }
}
