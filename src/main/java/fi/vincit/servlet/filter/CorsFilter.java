package fi.vincit.servlet.filter;

import org.springframework.security.web.util.matcher.AntPathRequestMatcher;
import org.springframework.security.web.util.matcher.OrRequestMatcher;
import org.springframework.security.web.util.matcher.RequestMatcher;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.io.IOException;

// Reference: https://developer.mozilla.org/en-US/docs/HTTP/Access_control_CORS
public class CorsFilter extends OncePerRequestFilter {

    // Allow cross-origin access only for REST
    private static final RequestMatcher requestMatcher = new OrRequestMatcher(
            new AntPathRequestMatcher("/api/**")
    );

    // Deny any origin specified by request Origin header by default for safety.
    private static boolean checkValidOrigin(@SuppressWarnings("unused") final String originHeaderValue) {
        // WARNING! Origin header should be checked to prevent XSS attacks.
        return false;
    }

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain)
            throws ServletException, IOException {

        // Cross origin request is detected using Origin-header
        final String requestOrigin = request.getHeader("Origin");

        if (requestOrigin != null && checkValidOrigin(requestOrigin)) {
            // The origin parameter specifies a URI that may access the resource.  The browser must enforce this.
            // For requests without credentials, the server may specify "*" as a wildcard,
            // thereby allowing any origin to access the resource.
            response.addHeader("Access-Control-Allow-Origin", requestOrigin);

            // Indicates whether or not the response to the request can be exposed when the credentials flag is true.
            // When used as part of a response to a preflight request, this indicates whether or not the actual
            // request can be made using credentials.  Note that simple GET requests are not preflighted, and so
            // if a request is made for a resource with credentials, if this header is not returned with the resource,
            // the response is ignored by the browser and not returned to web content.
            response.addHeader("Access-Control-Allow-Credentials", "true");

            // Unlike simple requests, "preflighted" requests first send an HTTP request by the OPTIONS method to the
            // resource on the other domain, in order to determine whether the actual request is safe to send.
            // Cross-site requests are preflighted like this since they may have implications to user data.
            // In particular, a request is preflighted if:
            //
            //  a) It uses methods other than GET, HEAD or POST.  Also, if POST is used to send request data
            //     with a Content-Type other than application/x-www-form-urlencoded, multipart/form-data or text/plain
            //
            //  b) It sets custom headers in the request (e.g. the request uses a header such as X-PINGOTHER)

            if ("OPTIONS".equals(request.getMethod())) {
                // Specifies the method or methods allowed when accessing the resource.
                response.addHeader("Access-Control-Allow-Methods", "GET, POST, PUT, PATCH, DELETE");

                // Indicate which HTTP headers can be used when making the actual request.
                response.addHeader("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");

                // How long the results of a preflight request can be cached?
                response.addHeader("Access-Control-Max-Age", "1728000"); // 20 days

                // Send empty response status code to pre-flight request
                response.setStatus(HttpServletResponse.SC_NO_CONTENT);

            } else {
                // Allow safe CORS requests to pass (GET, HEAD)
                filterChain.doFilter(request, response);
            }

        } else {
            // Normal request without Origin header
            filterChain.doFilter(request, response);
        }
    }

    @Override
    protected boolean shouldNotFilter(HttpServletRequest request) {
        return !requestMatcher.matches(request);
    }

}
