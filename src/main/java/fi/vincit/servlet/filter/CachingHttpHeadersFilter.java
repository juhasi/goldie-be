package fi.vincit.servlet.filter;

import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * This filter is used in production, to put HTTP cache headers with a long (1 month) expiration time.
 */
public class CachingHttpHeadersFilter extends OncePerRequestFilter {
    public static final String PATH_STATIC_CONTENT = "/static/";
    public static final String PATH_FRONTEND_CONTENT = "/frontend/";

    // Cache period is 1 month (in ms)
    private final static long CACHE_ONE_MONTH = 30 * 24 * 3600L;

    // We consider the last modified date is the start up time of the server
    private final static long LAST_MODIFIED = System.currentTimeMillis();

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException {

        if (request.getRequestURI().startsWith(PATH_STATIC_CONTENT) ||
            request.getRequestURI().startsWith(PATH_FRONTEND_CONTENT)) {

            setPublicCacheHeaders(response, CACHE_ONE_MONTH);
        }

        filterChain.doFilter(request, response);
    }

    private static void setPublicCacheHeaders(final HttpServletResponse httpResponse, long cacheSeconds) {
        // Setting the Last-Modified header, for browser caching
        httpResponse.setDateHeader("Last-Modified", LAST_MODIFIED);

        httpResponse.setDateHeader("Date", System.currentTimeMillis());

        // Setting Expires header, for proxy caching
        httpResponse.setDateHeader("Expires", System.currentTimeMillis() + 1000L * cacheSeconds);

        httpResponse.setHeader("Cache-Control", "public, max-age=" + cacheSeconds);

        // Instruct public proxies to cache both compressed / uncompressed version
        httpResponse.setHeader("Vary", "Accept-Encoding");
    }
}