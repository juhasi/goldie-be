package fi.vincit.servlet;

import fi.vincit.GoldieApplicationContext;
import fi.vincit.config.WebMVCConfig;
import fi.vincit.servlet.filter.CachingHttpHeadersFilter;
import fi.vincit.servlet.filter.CorsFilter;
import org.springframework.web.context.ContextLoaderListener;
import org.springframework.web.context.support.AnnotationConfigWebApplicationContext;
import org.springframework.web.filter.CharacterEncodingFilter;
import org.springframework.web.filter.DelegatingFilterProxy;
import org.springframework.web.filter.HiddenHttpMethodFilter;
import org.springframework.web.filter.HttpPutFormContentFilter;
import org.springframework.web.multipart.support.MultipartFilter;
import org.tuckey.web.filters.urlrewrite.UrlRewriteFilter;
import org.tuckey.web.filters.urlrewrite.gzip.GzipFilter;

import javax.servlet.Filter;
import javax.servlet.ServletContext;
import java.nio.charset.StandardCharsets;

public class WebApplicationConfig extends BaseWebApplicationConfig {

    public static final String DEFAULT_CHARACTER_ENCODING = StandardCharsets.UTF_8.name();

    @Override
    public void onStartup(ServletContext servletContext) {
        final AnnotationConfigWebApplicationContext rootApplicationContext = createRootApplicationContext();
        servletContext.addListener(new ContextLoaderListener(rootApplicationContext));

        registerDispatcherServlet(servletContext, rootApplicationContext);

        initUrlRewriteFilter(servletContext);
    }

    @Override
    protected Class<?>[] getApplicationConfigClasses() {
        return new Class<?>[] {GoldieApplicationContext.class, WebMVCConfig.class};
    }

    @Override
    protected Filter[] getDispatcherServletFilters() {
        final CharacterEncodingFilter characterEncodingFilter = new CharacterEncodingFilter();
        characterEncodingFilter.setEncoding(DEFAULT_CHARACTER_ENCODING);

        return new Filter[]{
//                new MDCInsertingServletFilter(),
                new CorsFilter(),
                new GzipFilter(),
                new CachingHttpHeadersFilter(),
                characterEncodingFilter,
                new MultipartFilter(),
                // This filter needs to run after multipart processing in case of a multipart POST request
                new HiddenHttpMethodFilter(),
                new HttpPutFormContentFilter(),
                new DelegatingFilterProxy("springSecurityFilterChain")
        };
    }

    private void initUrlRewriteFilter(ServletContext servletContext) {
        registerFilter(servletContext, "rewriteFilter", new UrlRewriteFilter(), true, "/*");
    }
}
