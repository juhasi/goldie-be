package fi.vincit.servlet;

import org.springframework.core.Conventions;
import org.springframework.util.Assert;
import org.springframework.util.ObjectUtils;
import org.springframework.web.WebApplicationInitializer;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.AnnotationConfigWebApplicationContext;
import org.springframework.web.servlet.DispatcherServlet;

import javax.servlet.*;
import java.util.EnumSet;
import java.util.HashMap;

abstract class BaseWebApplicationConfig implements WebApplicationInitializer {

    public static final String DISPATCHER_SERVLET_NAME = "dispatcher";

    protected void registerDispatcherServlet(ServletContext servletContext, WebApplicationContext servletAppContext) {
        DispatcherServlet dispatcherServlet = new DispatcherServlet(servletAppContext);
        ServletRegistration.Dynamic registration = servletContext.addServlet(
            DISPATCHER_SERVLET_NAME, dispatcherServlet);

        Assert.notNull(registration,
            "Failed to register servlet with name '" + DISPATCHER_SERVLET_NAME + "'." +
                "Check if there is another servlet registered under the same name."
        );

        registration.setLoadOnStartup(1);
        registration.addMapping("/");
        registration.setAsyncSupported(true);

        Filter[] filters = getDispatcherServletFilters();
        if (!ObjectUtils.isEmpty(filters)) {
            for (Filter filter : filters) {
                registerDispatcherServletFilter(servletContext, filter);
            }
        }
    }

    protected AnnotationConfigWebApplicationContext createRootApplicationContext() {
        AnnotationConfigWebApplicationContext servletAppContext = new AnnotationConfigWebApplicationContext();
        Class<?>[] configClasses = getApplicationConfigClasses();
        if (!ObjectUtils.isEmpty(configClasses)) {
            servletAppContext.register(configClasses);
        }
        return servletAppContext;
    }

    protected abstract Class<?>[] getApplicationConfigClasses();

    /**
     * Specify filters to add and map to the {@code DispatcherServlet}.
     *
     * @return an array of filters or {@code null}
     * @see #registerDispatcherServletFilter(javax.servlet.ServletContext, javax.servlet.Filter)
     */
    protected abstract Filter[] getDispatcherServletFilters();

    private static EnumSet<DispatcherType> getDispatcherTypes() {
        return EnumSet.of(DispatcherType.REQUEST, DispatcherType.FORWARD, DispatcherType.INCLUDE, DispatcherType.ASYNC);
    }

    protected FilterRegistration.Dynamic registerDispatcherServletFilter(ServletContext servletContext, Filter filter) {
        String filterName = Conventions.getVariableName(filter);
        FilterRegistration.Dynamic registration = servletContext.addFilter(filterName, filter);
        registration.setAsyncSupported(true);
        registration.addMappingForServletNames(getDispatcherTypes(), false, "dispatcher");

        return registration;
    }

    protected void registerFilter(ServletContext servletContext, String name, Filter filter,
                                  boolean async, String... patterns) {
        final FilterRegistration.Dynamic filterRegistration = servletContext.addFilter(name, filter);
        filterRegistration.setInitParameters(new HashMap<String, String>());
        filterRegistration.setAsyncSupported(async);

        final EnumSet<DispatcherType> dispatcherTypes = EnumSet.of(DispatcherType.REQUEST, DispatcherType.FORWARD);

        if (async) {
            dispatcherTypes.add(DispatcherType.ASYNC);
        }

        for (final String pattern : patterns) {
            filterRegistration.addMappingForUrlPatterns(dispatcherTypes, true, pattern);
        }
    }
}
