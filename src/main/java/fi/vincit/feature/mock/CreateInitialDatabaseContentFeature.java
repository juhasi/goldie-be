package fi.vincit.feature.mock;

import fi.vincit.feature.account.dao.UserRepository;
import fi.vincit.feature.account.entity.SystemUser;
import fi.vincit.feature.account.service.ChangePasswordService;
import fi.vincit.feature.common.util.RuntimeEnvironmentUtil;
import fi.vincit.security.UserInfo;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;

@Component
public class CreateInitialDatabaseContentFeature {
    private static final Logger LOG = LoggerFactory.getLogger(CreateInitialDatabaseContentFeature.class);

    @Resource
    private RuntimeEnvironmentUtil runtimeEnvironmentUtil;

    @Resource
    private UserRepository userRepository;

    @Resource
    private ChangePasswordService changePasswordService;

    @Transactional
    public void initSampleContent() {
        if (!runtimeEnvironmentUtil.isDevelopmentEnvironment() && !runtimeEnvironmentUtil.isIntegrationTestEnvironment()) {
            LOG.info("Sample data generation skipped when not in development environment");
            return;
        }

        if (userRepository.count() > 0) {
            LOG.info("Database seems to be populated already, returning...");
            return;
        }

        LOG.info("Creating sample content...");

        try {
            SecurityContextHolder.getContext().setAuthentication(new UserInfo.UserInfoBuilder(
                    "admin", null, SystemUser.Role.ROLE_ADMIN).createAuthentication());

            createDatabase();

        } catch (Exception e) {
            throw new RuntimeException("Error generating sample context: {}", e);

        } finally {
            SecurityContextHolder.clearContext();
        }
    }

    private void createDatabase() {
        // Create default admin user
        createUser("admin", SystemUser.Role.ROLE_ADMIN, "admin");

        // Create default user
        createUser("user", SystemUser.Role.ROLE_USER, "user");

        // Create additional users to demo paging and user search
        for (int i = 1; i < 35; i++) {
            // NOTE: Do not set password, because bcrypt is slow.
            createUser("user" + i, SystemUser.Role.ROLE_USER, null);
        }
    }

    private SystemUser createUser(final String username, final SystemUser.Role role, final String password) {
        final SystemUser user = new SystemUser();
        user.setUsername(username);
        user.setRole(role);
        user.setActive(true);

        if (password != null) {
            changePasswordService.setUserPassword(user, password);
        }

        return userRepository.save(user);
    }
}
