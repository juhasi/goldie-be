package fi.vincit.feature.account;

import com.google.common.base.MoreObjects;
import com.google.common.base.Preconditions;
import com.google.common.collect.ImmutableMap;
import fi.vincit.feature.account.dao.UserRepository;
import fi.vincit.feature.account.dto.PasswordResetDTO;
import fi.vincit.feature.account.entity.SystemUser;
import fi.vincit.feature.account.service.AccountAuditService;
import fi.vincit.feature.account.service.ActiveUserService;
import fi.vincit.feature.account.service.ChangePasswordService;
import fi.vincit.feature.account.support.PasswordResetTokenLink;
import fi.vincit.feature.common.util.RuntimeEnvironmentUtil;
import fi.vincit.feature.mail.api.MailService;
import fi.vincit.feature.mail.dto.MailMessageDTO;
import org.apache.velocity.app.VelocityEngine;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.security.core.token.TokenService;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import javax.annotation.Resource;
import java.net.URI;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

@Component
public class PasswordResetFeature {
    private static final Logger LOG = LoggerFactory.getLogger(PasswordResetFeature.class);

    public static final String TEMPLATE_PASSWORD_RESET = "email_password_reset.vm";

    @Resource
    private TokenService tokenService;
    @Resource
    private ChangePasswordService changePasswordService;
    @Resource
    private ActiveUserService activeUserService;
    @Resource
    private MailService mailService;
    @Resource
    private AccountAuditService accountAuditService;
    @Resource
    private VelocityEngine velocityEngine;
    @Resource
    private MessageSource messageSource;
    @Resource
    private UserRepository userRepository;
    @Resource
    private RuntimeEnvironmentUtil runtimeEnvironmentUtil;

    /**
     * Sends password renewal link to user(s) specified by email.
     * User can use the link to login to the application and to change her password.
     *
     * @param email Specifies the user(s) who will receive password renewal link.
     */
    @Transactional
    public void sendPasswordResetEmail(final String email) {
        LOG.debug("Send password renewal link to '{}'", email);

        // There can be multiple users with the same email
        for (final SystemUser user : userRepository.findByEmail(email)) {
            // Log account activity
            accountAuditService.auditPasswordResetRequest(user, activeUserService.getAuthentication());

            final Locale userLocale = MoreObjects.firstNonNull(user.getLocale(), LocaleContextHolder.getLocale());
            final String subject = messageSource.getMessage("account.password.reset.mail.subject", null, userLocale);

            final URI passwordResetLink = PasswordResetTokenLink.builder(tokenService)
                    .withUriBase(runtimeEnvironmentUtil.getBackendBaseUri())
                    .withUriPath("/")
                    .withUriQuery(null)
                    .withUriFragment("/password/reset/{token}")
                    .withSystemUserId(user.getId())
                    .buildLinkURI();

            final ImmutableMap<String, Object> params = ImmutableMap.<String, Object>of(
                    "link", passwordResetLink.toString());

            mailService.sendImmediate(new MailMessageDTO.Builder()
                    .withTo(user.getEmail())
                    .withSubject(subject)
                    .withVelocityBody(velocityEngine, TEMPLATE_PASSWORD_RESET, params));
        }
    }

    /**
     * Process password renewal token
     *
     * @return forward link stored in token or default redirect
     */
    @Transactional
    public void processPasswordReset(final PasswordResetDTO resetDTO) {
        Preconditions.checkNotNull(resetDTO, "No dto");
        Preconditions.checkArgument(StringUtils.hasText(resetDTO.getToken()), "Token is empty");
        Preconditions.checkArgument(StringUtils.hasText(resetDTO.getPassword()), "Empty password");

        final PasswordResetTokenLink data = PasswordResetTokenLink.parser(tokenService)
                .withTokenValidityPeriod(2, TimeUnit.HOURS)
                .parse(resetDTO.getToken());

        // Attempt login
        final SystemUser systemUser = userRepository.findOne(data.getSystemUserId());

        if (systemUser != null) {
            if (systemUser.isActive() && !systemUser.isDeleted()) {
                // Log account activity
                accountAuditService.auditPasswordReset(systemUser, activeUserService.getAuthentication());

                changePasswordService.setUserPassword(systemUser, resetDTO.getPassword());

                activeUserService.loginWithoutCheck(systemUser);

            } else {
                LOG.error("Refusing to process password reset for de-active userId={} username={}",
                        systemUser.getId(), systemUser.getUsername());
            }

        } else {
            throw new IllegalStateException("No valid user with password reset token userId");
        }
    }
}
