package fi.vincit.feature.account.exception;

/**
 * Thrown when user's password is updated to a password, that has been used too
 * recently.
 */
public class RecentlyUsedPasswordException extends IllegalStateException {
}
