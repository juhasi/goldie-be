package fi.vincit.feature.account.service;

import fi.vincit.security.authentication.RememberMeCookie;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UserDetails;

import javax.servlet.http.HttpServletRequest;

public interface PersistentRememberMeTokenService {
    RememberMeCookie create(Authentication successfulAuthentication, HttpServletRequest request);

    UserDetails validate(RememberMeCookie cookie, HttpServletRequest request);

    RememberMeCookie update(RememberMeCookie presentedCookie, HttpServletRequest request);

    void delete(RememberMeCookie cookie, boolean removeAllByUser);
}
