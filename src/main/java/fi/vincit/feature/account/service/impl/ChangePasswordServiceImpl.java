package fi.vincit.feature.account.service.impl;

import com.google.common.base.Preconditions;
import fi.vincit.feature.account.entity.PreviousPassword;
import fi.vincit.feature.account.entity.SystemUser;
import fi.vincit.feature.account.exception.RecentlyUsedPasswordException;
import fi.vincit.feature.account.service.ChangePasswordService;
import fi.vincit.feature.common.util.RuntimeEnvironmentUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import javax.inject.Inject;

@Service
public class ChangePasswordServiceImpl implements ChangePasswordService {
    private static final Logger LOG = LoggerFactory.getLogger(ChangePasswordServiceImpl.class);

    private PasswordEncoder passwordEncoder;
    private RuntimeEnvironmentUtil env;

    @Inject
    public ChangePasswordServiceImpl(PasswordEncoder passwordEncoder,
                                     RuntimeEnvironmentUtil env) {
        this.passwordEncoder = passwordEncoder;
        this.env = env;
    }

    @Override
    @Transactional(readOnly = true, propagation = Propagation.MANDATORY, noRollbackFor = RuntimeException.class)
    public boolean confirmPassword(final SystemUser user, final String plainPassword) {
        Preconditions.checkNotNull(plainPassword, "No password to confirm given");
        return user.getHashedPassword() != null
                ? passwordEncoder.matches(plainPassword, user.getHashedPassword())
                : false;
    }

    @Override
    @Transactional(propagation = Propagation.MANDATORY, noRollbackFor = RuntimeException.class)
    public void setUserPassword(final SystemUser user, final String plainPassword) {
        Preconditions.checkArgument(StringUtils.hasText(plainPassword), "Empty password");

        if (env.isPreviousPasswordCheckEnabled()) {
            checkForPasswordReuse(user, plainPassword);
        }

        user.setPasswordAsPlaintext(plainPassword, passwordEncoder);
    }

    private void checkForPasswordReuse(final SystemUser user, final String plainPassword) {
        if (StringUtils.hasText(user.getHashedPassword())) {
            LOG.debug("Adding previous password for username={}", user.getUsername());
            user.addPreviousPassword(new PreviousPassword(user.getHashedPassword()));
        }

        LOG.debug("Purging old passwords for username={} (max={})",
                user.getUsername(), env.getMaximumPreviousPasswordsHeld());
        user.purgeOldPasswords(env.getMaximumPreviousPasswordsHeld());

        if (user.passwordIsRecentlyUsed(plainPassword, passwordEncoder)) {
            LOG.error("Password re-use detected for username={}", user.getUsername());
            throw new RecentlyUsedPasswordException();
        }
    }
}
