package fi.vincit.feature.account.service;

import fi.vincit.security.UserInfo;
import fi.vincit.feature.account.entity.SystemUser;
import org.springframework.security.core.Authentication;

public interface ActiveUserService {
    boolean isAuthenticatedUser();

    Authentication getAuthentication();

    SystemUser getActiveUser();

    UserInfo getActiveUserInfo();

    Long getActiveUserId();

    void assertHasPermission(Object targetObject, Object permission);

    boolean checkHasPermission(Object targetObject, Object permission);

    void loginWithoutCheck(SystemUser systemUser);
}
