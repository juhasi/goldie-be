package fi.vincit.feature.account.service.impl;

import fi.vincit.config.properties.SecurityConfigProperties;
import fi.vincit.feature.account.dao.PersistentRememberMeTokenRepository;
import fi.vincit.feature.account.dao.UserRepository;
import fi.vincit.feature.account.entity.PersistentRememberMeToken;
import fi.vincit.feature.account.service.PersistentRememberMeTokenService;
import fi.vincit.security.UserInfo;
import fi.vincit.security.authentication.RememberMeCookie;
import fi.vincit.security.crypto.Base64StringKeyGenerator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.keygen.StringKeyGenerator;
import org.springframework.security.web.authentication.rememberme.CookieTheftException;
import org.springframework.security.web.authentication.rememberme.RememberMeAuthenticationException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.Date;

@Service
@Transactional(noRollbackFor = AuthenticationException.class)
public class PersistentRememberMeTokenServiceImpl implements PersistentRememberMeTokenService {
    private static final Logger LOG = LoggerFactory.getLogger(PersistentRememberMeTokenServiceImpl.class);

    @Resource
    private SecurityConfigProperties securityConfigurationProperties;

    @Resource
    private PersistentRememberMeTokenRepository tokenRepository;

    @Resource
    private UserRepository userRepository;

    // Used to generate random remember-me token value and series
    private StringKeyGenerator pseudoRandomGenerator =
            Base64StringKeyGenerator.withBitLength(RememberMeCookie.RANDOM_BIT_COUNT);

    @Override
    @Transactional
    public RememberMeCookie create(Authentication successfulAuthentication, HttpServletRequest request) {
        final UserInfo userInfo = UserInfo.extractFrom(successfulAuthentication);

        final PersistentRememberMeToken token = new PersistentRememberMeToken();
        token.setUser(userRepository.getOne(userInfo.getUserId()));
        token.setTokenDate(new Date());
        token.setIpAddress(request.getRemoteAddr());
        token.setUserAgent(request.getHeader("User-Agent"));

        final RememberMeCookie cookie = RememberMeCookie.create(pseudoRandomGenerator);
        token.setSeries(cookie.getSeries());
        token.setTokenValue(cookie.getValue());

        LOG.debug("Creating new persistent login for user: '{}'", userInfo.getUsername());

        return RememberMeCookie.forPersistentCookie(tokenRepository.save(token));
    }

    @Override
    @Transactional(readOnly = true)
    public UserDetails validate(final RememberMeCookie presentedCookie,
                                final HttpServletRequest request) {
        final PersistentRememberMeToken token = requireToken(presentedCookie);

        LOG.debug("Validating persistent login token for user '{}', series '{}'",
                token.getUser().getUsername(), presentedCookie.getSeries());

        // We have a match for this user/series combination
        if (!presentedCookie.matches(token)) {
            throw new CookieTheftException("Invalid remember-me token (Series/token) mismatch. " +
                    "Implies previous cookie theft attack.");
        }

        if (securityConfigurationProperties.getRememberMeTokenValiditySeconds() * 1000L
                < System.currentTimeMillis() - token.getTokenDate().getTime()) {
            throw new RememberMeAuthenticationException("Remember-me login has expired");
        }

        return new UserInfo.UserInfoBuilder(token.getUser()).createUserInfo();
    }

    @Override
    @Transactional
    public RememberMeCookie update(final RememberMeCookie cookie,
                                   final HttpServletRequest request) {
        final PersistentRememberMeToken token = requireToken(cookie);

        LOG.debug("Refreshing persistent login token for user '{}', series '{}'",
                token.getUser().getUsername(), token.getSeries());

        // Token also matches, so login is valid. Update the token value, keeping the *same* series number.
        token.setTokenDate(new Date());
        token.setIpAddress(request.getRemoteAddr());
        token.setUserAgent(request.getHeader("User-Agent"));

        final RememberMeCookie refreshedCookie = cookie.refresh(pseudoRandomGenerator);
        token.setTokenValue(refreshedCookie.getValue());

        return RememberMeCookie.forPersistentCookie(tokenRepository.save(token));
    }

    private PersistentRememberMeToken requireToken(RememberMeCookie presentedCookie) {
        final PersistentRememberMeToken token = tokenRepository.findOne(presentedCookie.getSeries());

        if (token == null) {
            // No series match, so we can't authenticate using this cookie
            throw new RememberMeAuthenticationException("No persistent token found for series id: "
                    + presentedCookie.getSeries());
        }

        return token;
    }

    @Override
    @Transactional
    public void delete(final RememberMeCookie cookie, boolean removeAllByUser) {
        final PersistentRememberMeToken token = tokenRepository.findOne(cookie.getSeries());

        if (token == null) {
            return;
        }

        if (removeAllByUser) {
            LOG.debug("Removing all persistent login tokens for user '{}'", token.getUser().getUsername());

            tokenRepository.deleteByUser(token.getUser());

        } else {
            LOG.debug("Removing persistent login token for user '{}', series '{}'",
                    token.getUser().getUsername(), cookie.getSeries());

            tokenRepository.delete(token);
        }
    }
}
