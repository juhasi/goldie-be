package fi.vincit.feature.account.service;

import fi.vincit.feature.account.entity.SystemUser;
import org.springframework.security.authentication.event.AbstractAuthenticationFailureEvent;
import org.springframework.security.authentication.event.AuthenticationSuccessEvent;
import org.springframework.security.core.Authentication;

import javax.servlet.http.HttpServletRequest;

public interface AccountAuditService {
    void auditLoginFailureEvent(AbstractAuthenticationFailureEvent failureEvent);

    void auditLoginSuccessEvent(AuthenticationSuccessEvent successEvent);

    void auditLogoutEvent(HttpServletRequest request, Authentication authentication);

    void auditPasswordReset(SystemUser user, Authentication authentication);

    void auditPasswordResetRequest(SystemUser user, Authentication authentication);

    void auditPasswordChange(SystemUser user, Authentication authentication);
}
