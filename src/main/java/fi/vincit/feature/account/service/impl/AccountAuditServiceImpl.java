package fi.vincit.feature.account.service.impl;

import fi.vincit.feature.account.dao.AccountActivityMessageRepository;
import fi.vincit.feature.account.dao.UserRepository;
import fi.vincit.feature.account.service.AccountAuditService;
import fi.vincit.feature.account.entity.AccountActivityMessage;
import fi.vincit.feature.account.entity.SystemUser;
import fi.vincit.security.UserInfo;
import org.springframework.security.authentication.event.AbstractAuthenticationFailureEvent;
import org.springframework.security.authentication.event.AuthenticationSuccessEvent;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.WebAuthenticationDetails;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

@Service
@Transactional(propagation = Propagation.MANDATORY, noRollbackFor = RuntimeException.class)
public class AccountAuditServiceImpl implements AccountAuditService {

    @Resource
    private AccountActivityMessageRepository logMessageRepository;

    @Resource
    private UserRepository userRepository;

    @Override
    @Transactional
    public void auditLoginFailureEvent(AbstractAuthenticationFailureEvent failureEvent) {
        final AccountActivityMessage message = createLogMessage(
                null, failureEvent.getAuthentication(),
                AccountActivityMessage.ActivityType.LOGIN_FAILRE);

        if (failureEvent.getException() != null) {
            message.setExceptionMessage(failureEvent.getException().getMessage());
        }

        logMessageRepository.save(message);
    }

    @Override
    @Transactional
    public void auditLoginSuccessEvent(AuthenticationSuccessEvent successEvent) {
        final AccountActivityMessage message = createLogMessage(
                null, successEvent.getAuthentication(),
                AccountActivityMessage.ActivityType.LOGIN_SUCCESS);

        logMessageRepository.save(message);
    }

    @Override
    public void auditLogoutEvent(HttpServletRequest request, Authentication authentication) {
        final AccountActivityMessage message = createLogMessage(
                null, authentication,
                AccountActivityMessage.ActivityType.LOGOUT);

        logMessageRepository.save(message);
    }

    @Override
    public void auditPasswordReset(SystemUser user, Authentication authentication) {
        final AccountActivityMessage message = createLogMessage(user, authentication,
                AccountActivityMessage.ActivityType.PASSWORD_RESET);

        logMessageRepository.save(message);
    }

    @Override
    public void auditPasswordResetRequest(SystemUser user, Authentication authentication) {
        final AccountActivityMessage message = createLogMessage(user, authentication,
                AccountActivityMessage.ActivityType.PASSWORD_RESET_REQUESTED);

        logMessageRepository.save(message);
    }

    @Override
    public void auditPasswordChange(SystemUser user, Authentication authentication) {
        final AccountActivityMessage message = createLogMessage(user, authentication,
                AccountActivityMessage.ActivityType.PASSWORD_CHANGE);

        logMessageRepository.save(message);
    }

    private AccountActivityMessage createLogMessage(final SystemUser user,
                                                    final Authentication authentication,
                                                    final AccountActivityMessage.ActivityType activityType) {
        final AccountActivityMessage message = new AccountActivityMessage();

        message.setActivityType(activityType);

        if (user != null) {
            message.setUsername(user.getUsername());
            message.setUserId(user.getId());
        } else {
            message.setUsername(authentication.getName());
            message.setUserId(getUserId(authentication));
        }

        final WebAuthenticationDetails webAuthenticationDetails = getWebAuthenticationDetails(authentication);
        if (webAuthenticationDetails != null) {
            message.setIpAddress(webAuthenticationDetails.getRemoteAddress());
        }

        return message;
    }

    private static WebAuthenticationDetails getWebAuthenticationDetails(final Authentication auth) {
        final Object details = auth.getDetails();
        if (details != null && details instanceof WebAuthenticationDetails) {
            return WebAuthenticationDetails.class.cast(details);
        }

        return null;
    }

    private Long getUserId(final Authentication auth) {
        if (auth.isAuthenticated() && auth.getPrincipal() instanceof UserInfo) {
            final UserInfo userInfo = UserInfo.extractFrom(auth);
            if (userInfo != null) {
                return userInfo.getUserId();
            }
        } else {
            // Failure event do not have user information in principal
            final SystemUser systemUser = userRepository.findByUsername(auth.getName());
            if (systemUser != null) {
                return systemUser.getId();
            }
        }

        return null;
    }
}
