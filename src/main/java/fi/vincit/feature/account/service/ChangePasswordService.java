package fi.vincit.feature.account.service;

import fi.vincit.feature.account.exception.RecentlyUsedPasswordException;
import fi.vincit.feature.account.entity.SystemUser;

public interface ChangePasswordService {
    /**
     * Attempt to change user password and apply password policy.
     *
     * @param user target system user
     * @param plainPassword password in plain-text
     * @throws RecentlyUsedPasswordException when password reuse is forbidden by the password policy.
     */
    void setUserPassword(SystemUser user, String plainPassword);

    /**
     * Confirm user current password matched given value.
     *
     * @param user target system user
     * @param plainPassword password in plain-text
     * @return true, if given password is correct
     */
    boolean confirmPassword(SystemUser user, String plainPassword);
}
