package fi.vincit.feature.account;

import com.google.common.base.Function;
import fi.vincit.feature.account.dao.UserRepository;
import fi.vincit.feature.account.dto.SystemUserDTO;
import fi.vincit.feature.account.entity.SystemUser;
import fi.vincit.feature.account.service.ChangePasswordService;
import fi.vincit.feature.common.AbstractCrudFeature;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Component;

import javax.inject.Inject;

@Component
public class UserCrudFeature extends AbstractCrudFeature<Long, SystemUser, SystemUserDTO> {
    @Inject
    private UserRepository userRepository;

    @Inject
    private ChangePasswordService changePasswordService;

    @Override
    protected JpaRepository<SystemUser, Long> getRepository() {
        return userRepository;
    }

    @Override
    protected void updateEntity(SystemUser user, SystemUserDTO userDTO) {
        if (userDTO.getUsername() != null) {
            user.setUsername(userDTO.getUsername());
        }

        if (userDTO.getRole() != null) {
            user.setRole(userDTO.getRole());
        } else if (user.getRole() == null) {
            user.setRole(SystemUser.Role.ROLE_USER);
        }

        user.setEmail(userDTO.getEmail());
        user.setActive(userDTO.isActive());
        user.setFirstName(userDTO.getFirstName());
        user.setLastName(userDTO.getLastName());
        user.setLocale(userDTO.getLocale());
        user.setTimeZone(userDTO.getTimeZone());

        if (userDTO.getPassword() != null) {
            changePasswordService.setUserPassword(user, userDTO.getPassword());
        }
    }

    @Override
    protected void delete(SystemUser user) {
        user.setActive(false);
        user.softDelete();
    }

    @Override
    protected Function<SystemUser, SystemUserDTO> entityToDTOFunction(final TransformationLevel level) {
        return SystemUserDTO.SMALL_DTO_FUNCTION;
    }
}
