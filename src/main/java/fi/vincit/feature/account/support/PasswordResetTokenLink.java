package fi.vincit.feature.account.support;

import com.google.common.base.Preconditions;
import fi.vincit.feature.common.support.AbstractSignedTokenLink;
import org.springframework.security.core.token.TokenService;

import java.util.Map;

public final class PasswordResetTokenLink extends AbstractSignedTokenLink {
    private static final String TYPE_RESET = "reset-password";
    private static final String KEY_USER_ID = "u";

    public static final Builder builder(final TokenService tokenService) {
        return new Builder(tokenService);
    }

    public static final Parser parser(final TokenService tokenService) {
        return new Parser(tokenService);
    }

    private final Long systemUserId;

    private PasswordResetTokenLink(final Parser parser) {
        Preconditions.checkArgument(null != parser.systemUserId, "Token userId is null");
        this.systemUserId = parser.systemUserId;
    }

    public Long getSystemUserId() {
        return systemUserId;
    }

    public static final class Builder extends AbstractSignedTokenLink.Builder<Builder> {
        protected Builder(final TokenService tokenService) {
            super(tokenService, TYPE_RESET);
        }

        @Override
        protected Builder getThis() {
            return this;
        }

        @Override
        protected boolean isValid() {
            return hasParameter(KEY_USER_ID);
        }

        public Builder withSystemUserId(Long id) {
            addParameter(KEY_USER_ID, Long.toString(id));

            return getThis();
        }
    }

    public static final class Parser extends AbstractSignedTokenLink.Parser<Parser> {
        private Long systemUserId;

        protected Parser(final TokenService tokenService) {
            super(tokenService, TYPE_RESET);
        }

        @Override
        public PasswordResetTokenLink parse(final String tokenKey) {
            final Map<String, String> params = extractTokenAttributes(tokenKey);

            this.systemUserId = Long.parseLong(params.get(KEY_USER_ID));

            return new PasswordResetTokenLink(this);
        }

        @Override
        protected Parser getThis() {
            return this;
        }
    }
}
