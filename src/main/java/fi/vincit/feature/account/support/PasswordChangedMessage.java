package fi.vincit.feature.account.support;

import com.google.common.base.MoreObjects;
import com.google.common.base.Preconditions;
import com.google.common.collect.ImmutableMap;

import fi.vincit.feature.account.entity.SystemUser;
import fi.vincit.feature.mail.dto.MailMessageDTO;

import org.apache.velocity.app.VelocityEngine;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.joda.time.format.DateTimeFormat;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.util.StringUtils;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;

import java.util.Locale;
import java.util.TimeZone;

public class PasswordChangedMessage {
    public static final String TEMPLATE_PASSWORD_CHANGED = "email_password_changed.vm";

    public static final PasswordChangedMessage createForActiveRequest(final VelocityEngine velocityEngine,
                                                                      final MessageSource messageSource) {
        return new PasswordChangedMessage(velocityEngine, messageSource).withRequest(getCurrentHttpServletRequest());
    }

    private static HttpServletRequest getCurrentHttpServletRequest() {
        final RequestAttributes requestAttributes = RequestContextHolder.getRequestAttributes();
        if (requestAttributes != null) {
            if (requestAttributes instanceof ServletRequestAttributes) {
                return ServletRequestAttributes.class.cast(requestAttributes).getRequest();
            }
        }
        return null;
    }

    private final VelocityEngine velocityEngine;
    private final MessageSource messageSource;

    private Locale locale;
    private TimeZone timeZone;
    private String email;
    private String remote;
    private String browser;

    public PasswordChangedMessage(final VelocityEngine velocityEngine,
                                  final MessageSource messageSource) {
        this.velocityEngine = velocityEngine;
        this.messageSource = messageSource;
    }

    public PasswordChangedMessage forUser(final SystemUser user) {
        this.email = user.getEmail();
        this.locale = user.getLocale();
        this.timeZone = user.getTimeZone();
        return this;
    }

    public PasswordChangedMessage withRequest(HttpServletRequest request) {
        if (request != null) {
            this.remote = request.getRemoteAddr();
            this.browser = request.getHeader("User-Agent");
        }
        return this;
    }

    public MailMessageDTO.Builder build() {
        Preconditions.checkState(StringUtils.hasText(this.email), "No email address");

        final Locale emailLocale = MoreObjects.firstNonNull(locale, LocaleContextHolder.getLocale());
        final String emailSubject = messageSource.getMessage("account.password.changed", null, emailLocale);

        final DateTime now = DateTime.now(DateTimeZone.forTimeZone(
                MoreObjects.firstNonNull(timeZone, TimeZone.getDefault())));

        final String patternForStyle = DateTimeFormat.patternForStyle("LM", emailLocale);

        final ImmutableMap<String, Object> model = ImmutableMap.<String, Object>builder()
                .put("timestamp", DateTimeFormat.forPattern(patternForStyle).print(now))
                .put("remote", MoreObjects.firstNonNull(this.remote, "???"))
                .put("browser", MoreObjects.firstNonNull(this.browser, "???"))
                .build();

        return new MailMessageDTO.Builder()
                .withTo(this.email)
                .withSubject(emailSubject)
                .withVelocityBody(velocityEngine, TEMPLATE_PASSWORD_CHANGED, model);
    }
}
