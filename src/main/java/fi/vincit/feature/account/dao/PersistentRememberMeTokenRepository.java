package fi.vincit.feature.account.dao;

import fi.vincit.feature.account.entity.PersistentRememberMeToken;
import fi.vincit.feature.account.entity.SystemUser;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

public interface PersistentRememberMeTokenRepository extends JpaRepository<PersistentRememberMeToken, String> {
    @Modifying
    @Transactional
    @Query("delete from PersistentRememberMeToken p where p.user = ?1")
    void deleteByUser(SystemUser user);
}
