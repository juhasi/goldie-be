package fi.vincit.feature.account.dao;

import fi.vincit.feature.account.entity.AccountActivityMessage;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AccountActivityMessageRepository extends JpaRepository<AccountActivityMessage, Long> {
    Page<AccountActivityMessage> findByUserId(Long userId, Pageable page);
}
