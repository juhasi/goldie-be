package fi.vincit.feature.account.dao;

import fi.vincit.feature.account.entity.SystemUser;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface UserRepository extends JpaRepository<SystemUser, Long> {
	SystemUser findByUsername(String username);

    List<SystemUser> findByEmail(String email);

    Page<SystemUser> findByUsernameContainingIgnoreCase(String searchTerm, Pageable page);

    @Query("SELECT u FROM SystemUser u WHERE UPPER(u.username) like '%'||UPPER(:searchQuery)||'%' OR UPPER(u.email) like '%'||UPPER(:searchQuery)||'%'")
    Page<SystemUser> findByUsernameOrEmailContaining(@Param("searchQuery") String searchQuery, Pageable page);
}
