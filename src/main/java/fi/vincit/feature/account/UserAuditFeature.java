package fi.vincit.feature.account;

import fi.vincit.feature.account.dao.AccountActivityMessageRepository;
import fi.vincit.feature.account.service.ActiveUserService;
import fi.vincit.feature.account.entity.AccountActivityMessage;
import fi.vincit.feature.account.entity.SystemUser;
import fi.vincit.security.EntityPermission;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;

@Component
public class UserAuditFeature {
    @Resource
    private AccountActivityMessageRepository messageRepository;

    @Resource
    private ActiveUserService activeUserService;

    @Transactional(readOnly = true)
    public Page<AccountActivityMessage> listByUser(Long userId, Pageable page) {
        activeUserService.assertHasPermission(SystemUser.class.getSimpleName() + ":" + userId, EntityPermission.READ);

        return messageRepository.findByUserId(userId, page);
    }
}
