package fi.vincit.feature.account.entity;

import com.google.common.collect.ComparisonChain;
import com.google.common.collect.Ordering;

import javax.annotation.Nonnull;
import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import java.io.Serializable;
import java.util.Date;

@Embeddable
@Access(value = AccessType.FIELD)
public class PreviousPassword implements Serializable {

    @Column(length = 255, nullable = false)
    private String password;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(nullable = false)
    private Date creationTime;

    public PreviousPassword() {
    }

    public PreviousPassword(@Nonnull String password) {
        setPassword(password);
        setCreationTime(new Date());
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String hashedPassword) {
        this.password = hashedPassword;
    }

    public Date getCreationTime() {
        return creationTime;
    }

    public void setCreationTime(Date creationTime) {
        this.creationTime = creationTime;
    }

    public static Ordering<PreviousPassword> getOrderingByCreationTime() {
        return new Ordering<PreviousPassword>() {
            @Override
            public int compare(PreviousPassword left, PreviousPassword right) {
                return ComparisonChain.start().
                        compare(left.getCreationTime(), right.getCreationTime()).
                        result();
            }
        };
    }
}
