package fi.vincit.feature.account.entity;

import com.google.common.base.Predicate;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import fi.vincit.feature.common.entity.LifecycleEntity;
import org.hibernate.annotations.Where;
import org.springframework.security.crypto.password.PasswordEncoder;

import javax.persistence.*;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;

@Entity
@Access(value = AccessType.FIELD)
@Where(clause = "deletion_time IS NULL")
public class SystemUser extends LifecycleEntity<Long> {
    public enum Role {
        ROLE_USER,
        ROLE_REST,
        ROLE_ADMIN;

        public String includes(Role that) {
            return this + " > " + that;
        }
    }

    private Long id;

    @Column(nullable = false)
    @Enumerated(EnumType.STRING)
    private Role role;

    @Column(length = 255, nullable = false, unique = true)
    private String username;

    @Column(length = 255, nullable = true)
    private String password;

    @Column(name = "locale_id", nullable = true)
    private Locale locale;

    @Column(name = "timezone_id", nullable = true)
    private TimeZone timeZone;

    @Column(nullable = true, length = 255)
    private String firstName;

    @Column(nullable = true, length = 255)
    private String lastName;

    @Column(nullable = true, length = 255)
    private String email;

    @Column(name = "is_active", nullable = false)
    private boolean active;

    @ElementCollection(fetch = FetchType.LAZY)
    @CollectionTable(name = "system_user_previous_password", joinColumns = @JoinColumn(name = "user_id"))
    private List<PreviousPassword> previousPasswords = Lists.newLinkedList();

    @Override
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Access(value = AccessType.PROPERTY)
    @Column(name = "user_id", nullable = false)
    public Long getId() {
        return id;
    }

    @Override
    public void setId(Long id) {
        this.id = id;
    }

    public String getHashedPassword() {
        return this.password;
    }

    public void setPasswordAsPlaintext(final String plainTextPassword,
                                       final PasswordEncoder passwordEncoder) {
        this.password = passwordEncoder.encode(plainTextPassword);
    }

    public boolean passwordIsRecentlyUsed(final String plainTextPassword,
                                          final PasswordEncoder passwordEncoder) {
        return Iterables.any(this.previousPasswords, new Predicate<PreviousPassword>() {
            @Override
            public boolean apply(PreviousPassword previousPassword) {
                return passwordEncoder.matches(plainTextPassword, previousPassword.getPassword());
            }
        });
    }

    public void addPreviousPassword(final PreviousPassword password) {
        this.previousPasswords.add(password);
    }

    public void purgeOldPasswords(final int maxRetainedCount) {
        final int overhead = this.previousPasswords.size() - maxRetainedCount;

        if (overhead > 0) {
            this.previousPasswords.removeAll(PreviousPassword.getOrderingByCreationTime()
                    .leastOf(this.previousPasswords, overhead));
        }
    }

    public Date getPasswordLastChangeTime() {
        return this.previousPasswords.isEmpty()
                ? getCreationTime()
                : PreviousPassword.getOrderingByCreationTime().max(this.previousPasswords).getCreationTime();
    }

    public String getUsername() {
        return this.username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean isActive) {
        this.active = isActive;
    }

    public Locale getLocale() {
        return locale;
    }

    public void setLocale(Locale locale) {
        this.locale = locale;
    }

    public TimeZone getTimeZone() {
        return timeZone;
    }

    public void setTimeZone(TimeZone timeZone) {
        this.timeZone = timeZone;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }
}