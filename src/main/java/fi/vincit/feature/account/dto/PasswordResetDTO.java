package fi.vincit.feature.account.dto;

import org.hibernate.validator.constraints.NotBlank;

import javax.validation.constraints.Size;

public class PasswordResetDTO {
    // Password reset token from email
    @NotBlank
    private String token;

    // New password
    @NotBlank
    @Size(min = 6, message="{javax.validation.constraints.Size.min.message}")
    private String password;

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
