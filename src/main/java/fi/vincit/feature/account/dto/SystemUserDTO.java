package fi.vincit.feature.account.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.google.common.base.Function;
import fi.vincit.feature.account.entity.SystemUser;
import fi.vincit.feature.common.entity.BaseEntityDTO;
import fi.vincit.feature.common.util.DtoUtil;
import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotEmpty;
import org.hibernate.validator.constraints.SafeHtml;

import javax.validation.constraints.AssertTrue;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.Locale;
import java.util.TimeZone;

public class SystemUserDTO extends BaseEntityDTO<Long> {
    public interface Create {}
    public interface Edit {}

    // Convert SystemUser-entity to light-weight DTO
    public static final Function<SystemUser, SystemUserDTO> SMALL_DTO_FUNCTION = new Function<SystemUser, SystemUserDTO>() {
        @Override
        public SystemUserDTO apply(final SystemUser user) {
            final SystemUserDTO dto = new SystemUserDTO();
            DtoUtil.copyBaseFields(user, dto);

            dto.setUsername(user.getUsername());
            dto.setActive(user.isActive());
            dto.setRole(user.getRole());
            dto.setEmail(user.getEmail());
            dto.setFirstName(user.getFirstName());
            dto.setLastName(user.getLastName());
            dto.setLocale(user.getLocale());
            dto.setTimeZone(user.getTimeZone());

            return dto;
        }
    };

    private Long id;

    private Integer rev;

    private boolean active = true;

    @NotNull(groups = { Create.class })
    private SystemUser.Role role;

    @Size(min = 2, max = 63, groups = { Edit.class, Create.class })
    @SafeHtml(whitelistType = SafeHtml.WhiteListType.NONE)
    private String username;

    @NotEmpty(groups = Create.class)
    @Size(min = 6, groups = Create.class, message = "{javax.validation.constraints.Size.min.message}")
    private String password;

    @NotEmpty(groups = Create.class)
    private String passwordConfirm;

    @Email(groups = { Edit.class, Create.class })
    private String email;

    @SafeHtml(whitelistType = SafeHtml.WhiteListType.NONE, groups = { Edit.class, Create.class })
    private String firstName;

    @SafeHtml(whitelistType = SafeHtml.WhiteListType.NONE, groups = { Edit.class, Create.class })
    private String lastName;

    private TimeZone timeZone;

    private Locale locale;

    /*
    http://stackoverflow.com/questions/1972933/cross-field-validation-with-hibernate-validator-jsr-303
     */
    @AssertTrue(message = "password confirmation must match", groups = { Create.class, Edit.class })
    @JsonIgnore
    public boolean isPasswordConfirmMatch() {
        if (this.getId() == null) {
            // Password is only required when creating new user
            return this.password != null && this.password.equals(this.passwordConfirm);
        }
        return this.password == null || this.password.equals(this.passwordConfirm);
    }

    @Override
    public Long getId() {
        return id;
    }

    @Override
    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public Integer getRev() {
        return rev;
    }

    @Override
    public void setRev(Integer rev) {
        this.rev = rev;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public SystemUser.Role getRole() {
        return role;
    }

    public void setRole(SystemUser.Role role) {
        this.role = role;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPasswordConfirm() {
        return passwordConfirm;
    }

    public void setPasswordConfirm(String passwordConfirm) {
        this.passwordConfirm = passwordConfirm;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(final String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(final String lastName) {
        this.lastName = lastName;
    }

    public TimeZone getTimeZone() {
        return timeZone;
    }

    public void setTimeZone(final TimeZone timeZone) {
        this.timeZone = timeZone;
    }

    public Locale getLocale() {
        return locale;
    }

    public void setLocale(final Locale locale) {
        this.locale = locale;
    }
}
