package fi.vincit.feature.account.dto;

import fi.vincit.feature.account.service.ActiveUserService;
import fi.vincit.feature.account.service.ChangePasswordService;
import fi.vincit.feature.account.entity.SystemUser;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import javax.annotation.Resource;

@Component
public class ChangePasswordDTOValidator implements Validator {
    @Resource
    private ActiveUserService activeUserService;

    @Resource
    private ChangePasswordService changePasswordService;

    @Override
    public boolean supports(Class<?> type) {
        return ChangePasswordDTO.class.isAssignableFrom(type);
    }
    
    @Override
    @Transactional(readOnly = true)
    public void validate(Object target, Errors errors) {
        final ChangePasswordDTO dto = ChangePasswordDTO.class.cast(target);

        if (!isPreviousPasswordCorrect(dto)) {
            errors.rejectValue("passwordCurrent",
                    "validation.error.user.password.current.does.not.match",
                    "Incorrect previous password");
        }
    }
    
    private boolean isPreviousPasswordCorrect(final ChangePasswordDTO dto) {
        if (StringUtils.hasText(dto.getPasswordCurrent())) {
            final SystemUser user = activeUserService.getActiveUser();
            return changePasswordService.confirmPassword(user, dto.getPasswordCurrent());
        }
        return false;
    }
}
