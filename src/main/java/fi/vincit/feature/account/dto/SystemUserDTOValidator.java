package fi.vincit.feature.account.dto;

import fi.vincit.feature.account.dao.UserRepository;
import fi.vincit.feature.account.entity.SystemUser;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import javax.annotation.Resource;

/**
 * Validator for SystemUserDTO class. Implements extra validation that
 * JSR303 annotations specified at SystemUserDTO class directly do not cover.
 * 
 */
@Component
public class SystemUserDTOValidator implements Validator {

    @Resource
    private UserRepository userRepository;

    /**
     * Checks whether this validator supports given class.
     * 
     * @param type
     *      Type of class that will be validated
     * @return 
     *      true if supports, false otherwise
     */
    @Override
    public boolean supports(Class<?> type) {
        return SystemUserDTO.class.isAssignableFrom(type);
    }

    /**
     * Validates target object. 
     * 
     * @param target
     *          Target object which is validated
     * @param errors 
     *          Adds all validation errors to this object
     */
    @Override
    @Transactional(readOnly = true)
    public void validate(Object target, Errors errors) {
        SystemUserDTO dto = (SystemUserDTO) target;
        if (dto != null && StringUtils.hasText(dto.getUsername())) {
            if (isUsernameAlreadyTaken(dto)) {
                errors.rejectValue("username", "validation.error.user.username.taken", "Username is already in use.");
            }
        }
    }

    /**
     * Checks if username defined in dto is already taken by some other user.
     * 
     * @param dto
     *          Contains username and id to compare against
     * @return 
     *          true if is already taken, false otherwise.
     */
    private boolean isUsernameAlreadyTaken(SystemUserDTO dto) {
        SystemUser user = userRepository.findByUsername(dto.getUsername());
        return user != null && !user.getId().equals(dto.getId());
    }
}
