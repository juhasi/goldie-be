package fi.vincit.feature.mail.impl;

import com.google.common.base.Optional;

import fi.vincit.feature.mail.api.MailService;
import fi.vincit.feature.mail.dto.MailMessageDTO;
import fi.vincit.feature.mail.spi.MailDeliveryService;
import fi.vincit.feature.mail.spi.OutgoingMailProvider;

import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

@Service
public class MailServiceImpl implements MailService {

    private static final Logger LOG = LoggerFactory.getLogger(MailServiceImpl.class);

    @Resource
    private OutgoingMailProvider outgoingMailProvider;

    @Resource
    private MailDeliveryService mailDeliveryService;

    @Value("${mail.enabled}")
    private boolean mailDeliveryEnabled;

    @Value("${mail.address.from}")
    private String fallbackDefaultEmailFromAddress;

    @Override
    public MailMessageDTO send(final MailMessageDTO.Builder builder) {
        return sendInternal(builder, false, null);
    }

    @Override
    public MailMessageDTO sendLater(final MailMessageDTO.Builder builder,
                                 final DateTime sendAfterTime) {
        return sendInternal(builder, false, sendAfterTime);
    }

    @Override
    public MailMessageDTO sendImmediate(final MailMessageDTO.Builder builder) {
        return sendInternal(builder, true, null);
    }

    private MailMessageDTO sendInternal(final MailMessageDTO.Builder builder,
                                     final boolean immediate,
                                     final DateTime sendAfterTime) {
        final MailMessageDTO messageDTO = builder
                .withDefaultFrom(fallbackDefaultEmailFromAddress)
                .build();

        if (mailDeliveryEnabled) {
            if (immediate) {
                mailDeliveryService.send(messageDTO);
            } else {
                outgoingMailProvider.scheduleForDelivery(messageDTO, Optional.fromNullable(sendAfterTime));
            }
        } else {
            LOG.warn("Mail message was not sent or persisted because mail is disabled: {}", messageDTO);
        }

        return messageDTO;
    }

    @Override
    public void processOutgoingMail() {
        final Map<Long, MailMessageDTO> outgoingBatch = outgoingMailProvider.getOutgoingBatch();

        if (outgoingBatch.isEmpty()) {
            return;
        }

        final Set<Long> failed = new HashSet<>();
        final Set<Long> successful = new HashSet<>();

        try {
            mailDeliveryService.sendAll(outgoingBatch, successful, failed);

        } catch (Exception ex) {
            LOG.error("Sending mail batch failed", ex);
        }

        outgoingMailProvider.storeDeliveryStatus(successful, failed);
    }
}
