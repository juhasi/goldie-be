package fi.vincit.feature.mail;

import fi.vincit.feature.mail.impl.delivery.JavaMailDeliveryServiceImpl;
import fi.vincit.feature.mail.impl.storage.DatabaseMailProviderImpl;
import fi.vincit.feature.mail.spi.MailDeliveryService;
import fi.vincit.feature.mail.spi.OutgoingMailProvider;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.mail.javamail.JavaMailSenderImpl;

import javax.annotation.Resource;
import java.util.Properties;

@Configuration
@PropertySource("classpath:configuration/mail.properties")
public class MailContext {

    @Resource
    private Environment env;

    @Bean
    public MailDeliveryService mailDeliveryService() {
        JavaMailDeliveryServiceImpl sender = new JavaMailDeliveryServiceImpl();

        sender.setJavaMailProperties(getMailProperties());
        sender.setHost(env.getRequiredProperty("mail.smtp.host"));
        sender.setPort(env.getRequiredProperty("mail.smtp.port", int.class));
        sender.setUsername(env.getRequiredProperty("mail.smtp.username"));
        sender.setPassword(env.getRequiredProperty("mail.smtp.password"));
        sender.setProtocol(env.getRequiredProperty("mail.smtp.protocol"));

        return sender;
    }

    @Bean
    public OutgoingMailProvider outgoingMailProvider() {
        return new DatabaseMailProviderImpl();
    }

    private Properties getMailProperties() {
        final Properties properties = new Properties();

        final String authEnabled = env.getRequiredProperty("mail.smtp.auth");
        properties.setProperty("mail.smtp.auth", authEnabled);

        if (env.getRequiredProperty("mail.smtp.ssl.enable", boolean.class)) {
            properties.setProperty("mail.smtps.auth", authEnabled);
            properties.setProperty("mail.smtp.ssl.enable", "true");
        }

        if (env.getRequiredProperty("mail.smtp.starttls.enable", boolean.class)) {
            properties.setProperty("mail.smtp.starttls.enable", "true");
        }

        properties.setProperty("mail.smtp.connectiontimeout", "1000");
        properties.setProperty("mail.smtp.timeout", "5000");

        return properties;
    }
}
