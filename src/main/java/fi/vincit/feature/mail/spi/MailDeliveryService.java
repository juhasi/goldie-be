package fi.vincit.feature.mail.spi;

import fi.vincit.feature.mail.dto.MailMessageDTO;

import java.io.Serializable;
import java.util.Map;
import java.util.Set;

public interface MailDeliveryService {
    void send(MailMessageDTO message);

    void sendAll(Map<Long, MailMessageDTO> outgoingBatch,
                 Set<Long> successfulMessages,
                 Set<Long> failedMessages);
}
