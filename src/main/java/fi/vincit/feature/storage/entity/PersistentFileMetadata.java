package fi.vincit.feature.storage.entity;

import fi.vincit.feature.common.entity.LifecycleEntity;
import fi.vincit.feature.storage.api.StorageType;
import org.hibernate.annotations.Type;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Id;
import javax.persistence.Table;
import java.net.URL;
import java.util.UUID;

@Entity
@Access(value = AccessType.FIELD)
@Table(name = "file_metadata")
//@Where(clause="deletion_time IS NULL")
public class PersistentFileMetadata extends LifecycleEntity<UUID> {
    private static final long serialVersionUID = -2798494103277387335L;

    private UUID id;

    @Enumerated(EnumType.STRING)
    @Column(name = "storage_type", nullable = false)
    private StorageType storageType;

    @Column(name = "original_file_name", nullable = true)
    private String originalFilename;

    @Column(name = "content_type", nullable = false)
    private String contentType;

    @Column(name = "content_size", nullable = false)
    private long contentSize;

    @Column(name = "crc_32")
    private long crc32;

    // Private / public for example. S3-bucket URL address
    @Column(name = "resource_url", nullable = true)
    private URL resourceURL;

    @Override
    @Id
    @Type(type = "uuid-char")
    @Access(value = AccessType.PROPERTY)
    @Column(name="file_metadata_uuid", columnDefinition = "CHAR(36)", nullable = false, updatable = false)
    public UUID getId() {
        return id;
    }

    @Override
    public void setId(UUID id) {
        this.id = id;
    }

    public StorageType getStorageType() {
        return storageType;
    }

    public void setStorageType(final StorageType storageType) {
        this.storageType = storageType;
    }

    public String getOriginalFilename() {
        return originalFilename;
    }

    public void setOriginalFilename(String originalFilename) {
        this.originalFilename = originalFilename;
    }

    public String getContentType() {
        return contentType;
    }

    public void setContentType(String contentType) {
        this.contentType = contentType;
    }

    public long getContentSize() {
        return contentSize;
    }

    public void setContentSize(long contentSize) {
        this.contentSize = contentSize;
    }

    public long getCrc32() {
        return crc32;
    }

    public void setCrc32(long crc32) {
        this.crc32 = crc32;
    }

    public URL getResourceURL() {
        return resourceURL;
    }

    public void setResourceURL(URL resourceURL) {
        this.resourceURL = resourceURL;
    }
}
