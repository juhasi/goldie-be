package fi.vincit.feature.storage.entity;

import fi.vincit.feature.common.entity.BaseEntity;
import org.apache.commons.codec.binary.Base64;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import java.util.UUID;

@Entity
@Access(value = AccessType.FIELD)
@Table(name = "file_content")
public class PersistentFileContent extends BaseEntity<UUID> {
    private UUID id;

    @PrimaryKeyJoinColumn
    @OnDelete(action = OnDeleteAction.CASCADE)
    @OneToOne(optional = false)
    private PersistentFileMetadata metadata;

    // @Lob - do not use for Postgres
    @Column(name = "file_content", columnDefinition = "text", nullable = false)
    private String contentBase64;

    @Override
    @Id
    @GeneratedValue(generator = "persistentFileContentForeignGenerator")
    @GenericGenerator(name = "persistentFileContentForeignGenerator", strategy = "foreign",
            parameters = @org.hibernate.annotations.Parameter(name = "property", value = "metadata"))
    @Type(type = "uuid-char")
    @Access(value = AccessType.PROPERTY)
    @JoinColumn(name = "file_metadata_uuid", columnDefinition = "CHAR(36)", nullable = false, updatable = false,
            foreignKey = @ForeignKey(name = "fk_file_content_metadata"))
    @Column(name = "file_metadata_uuid", columnDefinition = "CHAR(36)", nullable = false, updatable = false)
    public UUID getId() {
        return id;
    }

    @Override
    public void setId(UUID uuid) {
        this.id = uuid;
    }

    public byte[] getContent() {
        if (this.contentBase64 != null) {
            return Base64.decodeBase64(this.contentBase64);
        }

        return null;
    }

    public void setContent(final byte[] content) {
        if (content != null) {
            this.contentBase64 = Base64.encodeBase64String(content);
        } else {
            this.contentBase64 = null;
        }
    }

    public PersistentFileMetadata getMetadata() {
        return metadata;
    }

    public void setMetadata(final PersistentFileMetadata metadata) {
        if (metadata != null) {
            this.metadata = metadata;
        } else {
            this.metadata = null;
        }
    }
}
