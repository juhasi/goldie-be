package fi.vincit.feature.storage.dto;

import fi.vincit.feature.storage.api.StorageType;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.constraints.AssertTrue;
import javax.validation.constraints.NotNull;

public class AdminFileUploadDTO {
    @NotNull
    private MultipartFile multipartFile;

    @NotNull
    private StorageType storageType;

    @AssertTrue(message = "No file selected to upload")
    public boolean hasMultipartFile() {
        return this.multipartFile != null && !this.multipartFile.isEmpty();
    }

    public MultipartFile getMultipartFile() {
        return multipartFile;
    }

    public void setMultipartFile(MultipartFile multipartFile) {
        this.multipartFile = multipartFile;
    }

    public StorageType getStorageType() {
        return storageType;
    }

    public void setStorageType(StorageType storageType) {
        this.storageType = storageType;
    }
}
