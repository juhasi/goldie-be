package fi.vincit.feature.storage.dto;

import fi.vincit.feature.storage.api.StorageType;
import fi.vincit.feature.storage.entity.PersistentFileMetadata;

import java.net.URL;
import java.util.UUID;

public class PersistentFileDTO {
    public static PersistentFileDTO createFor(PersistentFileMetadata metadata) {
        final PersistentFileDTO dto = new PersistentFileDTO();

        dto.setUuid(metadata.getId());
        dto.setStorageType(metadata.getStorageType());
        dto.setResourceUrl(metadata.getResourceURL());
        dto.setOriginalFilename(metadata.getOriginalFilename());
        dto.setContentSize(metadata.getContentSize());
        dto.setContentType(metadata.getContentType());
        dto.setRemoved(metadata.isDeleted());

        return dto;
    }

    private UUID uuid;
    private String originalFilename;
    private URL resourceUrl;
    private String contentType;
    private long contentSize;
    private StorageType storageType;
    private long crc32;
    public boolean removed;

    public UUID getUuid() {
        return uuid;
    }

    public void setUuid(UUID uuid) {
        this.uuid = uuid;
    }

    public String getOriginalFilename() {
        return originalFilename;
    }

    public void setOriginalFilename(String originalFilename) {
        this.originalFilename = originalFilename;
    }

    public URL getResourceUrl() {
        return resourceUrl;
    }

    public void setResourceUrl(URL resourceUrl) {
        this.resourceUrl = resourceUrl;
    }

    public String getContentType() {
        return contentType;
    }

    public void setContentType(String contentType) {
        this.contentType = contentType;
    }

    public long getContentSize() {
        return contentSize;
    }

    public void setContentSize(long contentSize) {
        this.contentSize = contentSize;
    }

    public StorageType getStorageType() {
        return storageType;
    }

    public void setStorageType(StorageType storageType) {
        this.storageType = storageType;
    }

    public long getCrc32() {
        return crc32;
    }

    public void setCrc32(long crc32) {
        this.crc32 = crc32;
    }

    public boolean isRemoved() {
        return removed;
    }

    public void setRemoved(boolean removed) {
        this.removed = removed;
    }
}
