package fi.vincit.feature.storage.dao;

import fi.vincit.feature.storage.entity.PersistentFileMetadata;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.UUID;

public interface PersistentFileMetadataRepository extends JpaRepository<PersistentFileMetadata, UUID> {
}
