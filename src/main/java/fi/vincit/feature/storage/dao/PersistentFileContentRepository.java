package fi.vincit.feature.storage.dao;

import fi.vincit.feature.storage.entity.PersistentFileContent;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.UUID;

public interface PersistentFileContentRepository extends JpaRepository<PersistentFileContent, UUID> {
}
