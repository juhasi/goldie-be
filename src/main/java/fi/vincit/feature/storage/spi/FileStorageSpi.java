package fi.vincit.feature.storage.spi;

import fi.vincit.feature.storage.api.FileType;
import fi.vincit.feature.storage.entity.PersistentFileMetadata;

import java.io.IOException;
import java.io.InputStream;

public interface FileStorageSpi {
    void storeFile(FileType contentType,
                   PersistentFileMetadata metadata,
                   InputStream inputStream) throws IOException;

    InputStream getDataInputStream(PersistentFileMetadata metadata) throws IOException;

    void removeFromStorage(PersistentFileMetadata metadata);
}
