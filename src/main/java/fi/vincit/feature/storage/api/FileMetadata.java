package fi.vincit.feature.storage.api;

import com.google.common.base.Preconditions;
import org.springframework.web.multipart.MultipartFile;

import java.util.UUID;

public class FileMetadata {
    private final UUID uuid;
    private final String originalFilename;
    private final String contentType;

    public static FileMetadata createForFilename(String filename, String contentType) {
        return new FileMetadata(UUID.randomUUID(), filename, contentType);
    }

    public static FileMetadata createForMultipartFile(MultipartFile f) {
        return new FileMetadata(UUID.randomUUID(), f.getOriginalFilename(), f.getContentType());
    }

    public FileMetadata(final UUID uuid,
                        final String originalFilename,
                        final String contentType) {
        this.uuid = Preconditions.checkNotNull(uuid, "UUID is required");
        this.contentType = Preconditions.checkNotNull(contentType, "File contentType is required");
        this.originalFilename = originalFilename;
    }

    public UUID getUuid() {
        return uuid;
    }

    public String getOriginalFilename() {
        return originalFilename;
    }

    public String getContentType() {
        return contentType;
    }
}