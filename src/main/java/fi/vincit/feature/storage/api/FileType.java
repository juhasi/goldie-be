package fi.vincit.feature.storage.api;

// File storage can be customized using file content type
public enum FileType {
    UNKNOWN,
    IMAGE_UPLOAD
}
