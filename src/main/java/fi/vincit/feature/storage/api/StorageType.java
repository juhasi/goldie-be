package fi.vincit.feature.storage.api;

// Storage method
public enum StorageType {
    LOCAL_DATABASE,
    LOCAL_FOLDER,
    AWS_S3_BUCKET
}
