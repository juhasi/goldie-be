package fi.vincit.feature.storage.api;

import fi.vincit.feature.storage.entity.PersistentFileMetadata;

import java.io.IOException;
import java.io.InputStream;
import java.util.UUID;

public interface FileStorageService {
    /**
     * Get file metadata
     *
     * @param uuid file unique id
     * @return file metadata entity
     * @throws java.lang.IllegalArgumentException if no metadata with given UUID is found.
     */
    PersistentFileMetadata getMetadata(UUID uuid);

    /**
     * Get file data
     *
     * @param uuid file unique id
     * @return raw file data as byte array
     * @throws IOException
     * @throws java.lang.IllegalArgumentException if no metadata with given UUID is found.
     */
    byte[] getBytes(UUID uuid) throws IOException;

    /**
     * Remove file data from storage.
     *
     * @param uuid         file unique id
     * @throws java.lang.IllegalArgumentException if no metadata with given UUID is found.
     */
    void remove(UUID uuid);

    /**
     * Store file content and metadata using various storage implementations.
     *
     * @param fileType    General purpose of the file is used to control storage options
     * @param storageType Required storage method
     * @param metadata    File metadata attributes
     * @param inputStream File data input stream
     * @return File metadata entity suitable as reference from domain classes such as Thumbnail, Upload, Report etc.
     * @throws IOException
     */
    PersistentFileMetadata storeFile(
            FileType fileType,
            StorageType storageType,
            FileMetadata metadata,
            InputStream inputStream) throws IOException;
}
