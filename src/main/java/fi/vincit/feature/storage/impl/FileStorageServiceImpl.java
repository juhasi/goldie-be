package fi.vincit.feature.storage.impl;

import com.google.common.base.Preconditions;

import com.google.common.io.CountingInputStream;
import fi.vincit.feature.storage.dao.PersistentFileMetadataRepository;
import fi.vincit.feature.storage.api.FileType;
import fi.vincit.feature.storage.api.FileStorageService;
import fi.vincit.feature.storage.api.StorageType;
import fi.vincit.feature.storage.api.FileMetadata;
import fi.vincit.feature.storage.spi.FileStorageSpi;
import fi.vincit.feature.storage.entity.PersistentFileMetadata;

import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;

import java.io.IOException;
import java.io.InputStream;
import java.util.UUID;
import java.util.zip.CRC32;
import java.util.zip.CheckedInputStream;

@Service
@Transactional(propagation = Propagation.MANDATORY, noRollbackFor = RuntimeException.class)
public class FileStorageServiceImpl implements FileStorageService {
    @Resource
    private PersistentFileMetadataRepository metadataRepository;

    @Resource
    @Qualifier("s3")
    private FileStorageSpi s3FileStorage;

    @Resource
    @Qualifier("db")
    private FileStorageSpi dbFileStorage;

    @Resource
    @Qualifier("folder")
    private FileStorageSpi folderFileStorage;

    private FileStorageSpi getStorage(final StorageType storageType) {
        switch (storageType) {
            case LOCAL_FOLDER:
                return folderFileStorage;
            case AWS_S3_BUCKET:
                return s3FileStorage;
            case LOCAL_DATABASE:
                return dbFileStorage;
            default:
                throw new IllegalStateException("Unknown storageType=" + storageType);
        }
    }

    @Override
    @Transactional(readOnly = true)
    public PersistentFileMetadata getMetadata(final UUID uuid) {
        Preconditions.checkNotNull(uuid, "File UUID is required parameter");

        final PersistentFileMetadata persistentFileMetadata = metadataRepository.findOne(uuid);

        if (persistentFileMetadata == null) {
            throw new IllegalArgumentException("Could not find file metadata in for uuid=" + uuid);
        }

        return persistentFileMetadata;
    }

    @Override
    @Transactional(rollbackFor = IOException.class)
    public PersistentFileMetadata storeFile(final FileType fileType,
                                            final StorageType storageType,
                                            final FileMetadata metadata,
                                            final InputStream inputStream) throws IOException {
        final PersistentFileMetadata fileMetadata = storePersistentMetadata(metadata, storageType);

        try (final CountingInputStream countingStream = new CountingInputStream(inputStream);
             final CheckedInputStream checkedStream = new CheckedInputStream(countingStream, new CRC32())) {
            getStorage(fileMetadata.getStorageType()).storeFile(fileType, fileMetadata, checkedStream);

            fileMetadata.setCrc32(checkedStream.getChecksum().getValue());
            fileMetadata.setContentSize(countingStream.getCount());

            return fileMetadata;
        }
    }

    private PersistentFileMetadata storePersistentMetadata(
            final FileMetadata input,
            final StorageType storageType) {

        final PersistentFileMetadata metadata = new PersistentFileMetadata();

        metadata.setStorageType(Preconditions.checkNotNull(storageType, "Storage type is null"));
        metadata.setId(Preconditions.checkNotNull(input.getUuid(), "File UUID is null"));
        metadata.setContentType(Preconditions.checkNotNull(input.getContentType(), "File contentType is null"));
        metadata.setOriginalFilename(input.getOriginalFilename());

        return metadataRepository.save(metadata);
    }

    @Override
    @Transactional(readOnly = true, rollbackFor = IOException.class)
    public byte[] getBytes(final UUID uuid) throws IOException {
        final PersistentFileMetadata metadata = getMetadata(uuid);

        if (metadata.isDeleted()) {
            throw new IllegalStateException("File metadata indicates content has been removed");
        }

        FileStorageSpi fss = getStorage(metadata.getStorageType());

        try (final InputStream dataInputStream = fss.getDataInputStream(metadata)) {
            return IOUtils.toByteArray(dataInputStream);
        }
    }

    @Override
    @Transactional
    public void remove(final UUID uuid) {
        final PersistentFileMetadata metadata = getMetadata(uuid);

        if (!metadata.isDeleted()) {
            getStorage(metadata.getStorageType()).removeFromStorage(metadata);
        }

        metadataRepository.delete(metadata);
    }
}
