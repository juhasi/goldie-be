package fi.vincit.feature.storage.impl.file;

import com.google.common.io.Files;
import fi.vincit.feature.storage.api.FileType;
import fi.vincit.feature.storage.entity.PersistentFileMetadata;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import java.io.File;

@Component
@Qualifier("folder")
public class LocalFolderFileStorage extends BaseFolderFileStorage {
    private static final Logger LOG = LoggerFactory.getLogger(LocalFolderFileStorage.class);

    @Resource
    private Environment environment;

    private File storageFolder;

    @PostConstruct
    public void init() {
        final String storageFolderPath = environment.getProperty("file.storage.folder");

        if (storageFolderPath == null) {
            LOG.warn("File local storage folder not set, using temporary folder");

            this.storageFolder = Files.createTempDir();
            this.storageFolder.deleteOnExit();

        } else {
            LOG.info("File local storage folder: {}", storageFolderPath);

            this.storageFolder = new File(storageFolderPath);
        }
    }

    @Override
    protected File getStorageFolder() {
        return storageFolder;
    }

    @Override
    protected String createStorageFilename(final FileType fileType,
                                           final PersistentFileMetadata metadata) {
        return metadata.getId().toString();
    }
}
