package fi.vincit.feature.storage.impl.file;

import com.google.common.base.Preconditions;
import com.google.common.io.Files;
import fi.vincit.feature.storage.api.FileType;
import fi.vincit.feature.storage.entity.PersistentFileMetadata;
import fi.vincit.feature.storage.spi.FileStorageSpi;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.transaction.support.TransactionSynchronizationAdapter;
import org.springframework.transaction.support.TransactionSynchronizationManager;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URISyntaxException;
import java.net.URL;

// All not so interesting boiler-plate code is inherited from base class.
abstract class BaseFolderFileStorage implements FileStorageSpi {

    private static final Logger LOG = LoggerFactory.getLogger(BaseFolderFileStorage.class);

    @Override
    public void storeFile(final FileType fileType,
                          final PersistentFileMetadata metadata,
                          final InputStream inputStream) throws IOException {
        final String fileName = createStorageFilename(fileType, metadata);
        final File storageFile = new File(getStorageFolder(), fileName);

        metadata.setResourceURL(storageFile.toURI().toURL());

        TransactionSynchronizationManager.registerSynchronization(new TransactionSynchronizationAdapter() {
            @Override
            public void afterCompletion(final int status) {
                // Remove file if transaction is rolled back
                if (status == STATUS_ROLLED_BACK) {
                    if (storageFile.exists()) {
                        if (!storageFile.delete()) {
                            LOG.warn("Could not delete file {} within rollback.", fileName);
                        }
                    }
                }
            }
        });

        try {
            Files.asByteSink(storageFile).writeFrom(inputStream);
        } finally {
            inputStream.close();
        }
    }

    @Override
    public InputStream getDataInputStream(PersistentFileMetadata metadata) throws IOException {
        return new FileInputStream(getStorageFile(metadata));
    }

    @Override
    public void removeFromStorage(PersistentFileMetadata metadata) {
        final File storageFile = getStorageFile(metadata);

        // Delay until transaction has completed with success.
        TransactionSynchronizationManager.registerSynchronization(new TransactionSynchronizationAdapter() {
            @Override
            public void afterCommit() {
                if (!storageFile.delete()) {
                    LOG.warn("Could not delete file {} within rollback.", storageFile.getName());
                }
            }
        });
    }

    private static File getStorageFile(final PersistentFileMetadata metadata) {
        try {
            final URL resourceUrl = Preconditions.checkNotNull(metadata.getResourceURL());

            Preconditions.checkArgument("file".equals(resourceUrl.getProtocol()), "Should have file:// as URL scheme");

            return new File(resourceUrl.toURI());

        } catch (URISyntaxException ex) {
            throw new RuntimeException("Could not parse resourceUrl as File", ex);
        }
    }

    protected abstract File getStorageFolder();

    protected abstract String createStorageFilename(FileType fileType,
                                                    PersistentFileMetadata metadata);
}
