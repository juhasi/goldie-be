package fi.vincit.feature.storage.impl.aws;

import com.google.common.base.Throwables;
import fi.vincit.feature.storage.api.FileType;
import fi.vincit.feature.storage.spi.FileStorageSpi;
import fi.vincit.feature.storage.entity.PersistentFileMetadata;
import org.jets3t.service.Constants;
import org.jets3t.service.Jets3tProperties;
import org.jets3t.service.S3Service;
import org.jets3t.service.ServiceException;
import org.jets3t.service.impl.rest.httpclient.RestS3Service;
import org.jets3t.service.model.S3Bucket;
import org.jets3t.service.model.S3Object;
import org.jets3t.service.security.AWSCredentials;
import org.jets3t.service.utils.ServiceUtils;
import org.springframework.transaction.support.TransactionSynchronizationAdapter;
import org.springframework.transaction.support.TransactionSynchronizationManager;
import org.springframework.web.util.UriComponentsBuilder;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;

// All not so interesting boiler-plate code is inherited from base class.
abstract class BaseS3FileStorage implements FileStorageSpi {
    public static final String ENDPOINT_EU_WEST1 = "s3-eu-west-1.amazonaws.com";

    static URL createResourceURL(final S3Bucket s3Bucket,
                                 final S3Object s3Object,
                                 final String s3Endpoint) {
        try {
            final String hostname = ServiceUtils.generateS3HostnameForBucket(
                    s3Bucket.getName(), false, s3Endpoint);

            return UriComponentsBuilder.newInstance()
                    .scheme("http")
                    .host(hostname)
                    .path(s3Object.getKey())
                    .build().toUri().toURL();

        } catch (Exception e) {
            throw new RuntimeException("Could not construct S3 resource URL", e);
        }
    }

    static S3Object parseResourceURL(final URL resourceUrl,
                                     final String s3Endpoint) {
        try {
            return ServiceUtils.buildObjectFromUrl(resourceUrl.getHost(),
                    resourceUrl.getPath(), s3Endpoint);

        } catch (Exception e) {
            throw new RuntimeException("Could not parse S3 resource URL", e);
        }
    }

    private S3Service s3Service;

    @PostConstruct
    public void init() {
        Jets3tProperties props = Jets3tProperties.getInstance(Constants.JETS3T_PROPERTIES_FILENAME);
        props.setProperty("httpclient.proxy-autodetect", "false");
        this.s3Service = new RestS3Service(getCredentials());
    }

    @Override
    public InputStream getDataInputStream(final PersistentFileMetadata metadata) {
        final S3Object s3Object = getBucketObject(metadata);
        try {
            return s3Service.getObject(s3Object.getBucketName(), s3Object.getKey()).getDataInputStream();
        } catch (ServiceException ex) {
            throw Throwables.propagate(ex);
        }
    }

    @Override
    public void removeFromStorage(final PersistentFileMetadata metadata) {
        final S3Object s3Object = getBucketObject(metadata);

        // Delay S3 object delete until transaction is complete and succesful
        TransactionSynchronizationManager.registerSynchronization(new TransactionSynchronizationAdapter() {
            @Override
            public void afterCommit() {
                try {
                    s3Service.deleteObject(s3Object.getBucketName(), s3Object.getKey());

                } catch (ServiceException ex) {
                    throw Throwables.propagate(ex);
                }
            }
        });
    }

    @Override
    public void storeFile(FileType contentType,
                          PersistentFileMetadata metadata,
                          InputStream inputStream) throws IOException {
        final S3Bucket s3Bucket = getBucket(contentType, metadata);
        final S3Object s3Object = uploadObject(inputStream, s3Bucket, contentType, metadata);

        // Store S3 bucket and key embedded as resource URL
        metadata.setResourceURL(createResourceURL(s3Bucket, s3Object, ENDPOINT_EU_WEST1));
    }

    private static S3Object getBucketObject(PersistentFileMetadata metadata) {
        return parseResourceURL(metadata.getResourceURL(), ENDPOINT_EU_WEST1);
    }

    private S3Object uploadObject(InputStream inputStream, S3Bucket s3Bucket,
                                  FileType contentType,
                                  PersistentFileMetadata metadata) throws IOException {
        final S3Object s3Object = createBucketObject(s3Bucket, metadata, contentType);
        s3Object.setDataInputStream(inputStream);

        try {
            return s3Service.putObject(s3Object.getBucketName(), s3Object);

        } catch (ServiceException ex) {
            throw new RuntimeException("Could not store file content to bucket", ex);

        } finally {
            s3Object.closeDataInputStream();
        }
    }

    protected abstract S3Object createBucketObject(
            S3Bucket s3Bucket,
            PersistentFileMetadata metadata, FileType contentType);

    protected abstract S3Bucket getBucket(
            FileType contentType,
            PersistentFileMetadata metadata);

    protected abstract AWSCredentials getCredentials();
}