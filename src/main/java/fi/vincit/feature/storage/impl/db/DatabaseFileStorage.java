package fi.vincit.feature.storage.impl.db;

import fi.vincit.feature.storage.dao.PersistentFileContentRepository;
import fi.vincit.feature.storage.api.FileType;
import fi.vincit.feature.storage.spi.FileStorageSpi;
import fi.vincit.feature.storage.entity.PersistentFileContent;
import fi.vincit.feature.storage.entity.PersistentFileMetadata;
import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;

@Component
@Qualifier("db")
public class DatabaseFileStorage implements FileStorageSpi {
    @Resource
    private PersistentFileContentRepository contentRepository;

    @Override
    public InputStream getDataInputStream(PersistentFileMetadata metadata) {
        return new ByteArrayInputStream(fetchPersistentContent(metadata).getContent());
    }

    @Override
    public void storeFile(FileType fileType,
                          PersistentFileMetadata metadata,
                          InputStream inputStream) throws IOException {
        final PersistentFileContent fileContent = new PersistentFileContent();

        fileContent.setContent(IOUtils.toByteArray(inputStream));
        fileContent.setMetadata(metadata);

        contentRepository.save(fileContent);
    }

    @Override
    public void removeFromStorage(final PersistentFileMetadata metadata) {
        contentRepository.delete(metadata.getId());
    }

    private PersistentFileContent fetchPersistentContent(final PersistentFileMetadata persistentFileMetadata) {
        final PersistentFileContent fileContent = contentRepository.findOne(persistentFileMetadata.getId());

        if (fileContent == null) {
            throw new IllegalStateException("Could not find file content for uuid=" + persistentFileMetadata.getId());
        }

        return fileContent;
    }
}
