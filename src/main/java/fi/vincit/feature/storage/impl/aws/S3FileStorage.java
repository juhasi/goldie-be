package fi.vincit.feature.storage.impl.aws;

import com.google.common.base.Preconditions;

import fi.vincit.feature.storage.api.FileType;
import fi.vincit.feature.storage.entity.PersistentFileMetadata;

import org.jets3t.service.acl.AccessControlList;
import org.jets3t.service.model.S3Bucket;
import org.jets3t.service.model.S3Object;
import org.jets3t.service.security.AWSCredentials;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

@Component
@Qualifier("s3")
public class S3FileStorage extends BaseS3FileStorage {

    @Resource
    private Environment environment;

    /**
     * Environment variable names are based
     * on default Amazon Beanstalk variables.
     */
    @Override
    protected AWSCredentials getCredentials() {
        return new AWSCredentials(environment.getProperty("AWS_ACCESS_KEY_ID"), environment.getProperty("AWS_SECRET_KEY"));
    }

    /**
     * Storage bucket can be customized to for example storage
     * different content types in separate buckets.
     *
     * @param fileType
     * @param metadata
     */
    @Override
    protected S3Bucket getBucket(final FileType fileType,
                                 final PersistentFileMetadata metadata) {
        return new S3Bucket(environment.getRequiredProperty("AWS_S3_BUCKET"), S3Bucket.LOCATION_EUROPE);
    }

    @Override
    protected S3Object createBucketObject(final S3Bucket s3Bucket,
                                          final PersistentFileMetadata metadata,
                                          final FileType fileType) {
        final String key = Preconditions.checkNotNull(metadata.getId()).toString();
        final S3Object s3Object = new S3Object(s3Bucket, key);

        s3Object.setContentLength(Preconditions.checkNotNull(metadata.getContentSize()));
        s3Object.setContentType(Preconditions.checkNotNull(metadata.getContentType()));

        // Reduced Redundancy class is little bit less expensive option
        s3Object.setStorageClass(S3Object.STORAGE_CLASS_REDUCED_REDUNDANCY);

        // Allow read for authenticated users and write only to owner
        s3Object.setAcl(AccessControlList.REST_CANNED_AUTHENTICATED_READ);

        return s3Object;
    }
}
