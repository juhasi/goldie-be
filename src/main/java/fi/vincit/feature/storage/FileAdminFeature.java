package fi.vincit.feature.storage;

import com.google.common.base.Function;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import fi.vincit.feature.storage.dao.PersistentFileMetadataRepository;
import fi.vincit.feature.storage.api.FileType;
import fi.vincit.feature.storage.api.FileStorageService;
import fi.vincit.feature.storage.dto.AdminFileUploadDTO;
import fi.vincit.feature.storage.api.FileMetadata;
import fi.vincit.feature.storage.dto.PersistentFileDTO;
import fi.vincit.feature.storage.entity.PersistentFileMetadata;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import java.io.IOException;
import java.util.UUID;

@Service
@Transactional(rollbackFor = IOException.class)
@PreAuthorize("hasRole('ROLE_ADMIN')")
public class FileAdminFeature {

    @Resource
    private PersistentFileMetadataRepository metadataRepository;

    @Resource
    private FileStorageService fileStorageService;

    @Transactional(readOnly = true)
    public Page<PersistentFileDTO> listFiles(final Pageable page) {
        final Page<PersistentFileMetadata> all = metadataRepository.findAll(page);

        return transformToDTO(all, page, new Function<PersistentFileMetadata, PersistentFileDTO>() {
            @Override
            public PersistentFileDTO apply(PersistentFileMetadata input) {
                return PersistentFileDTO.createFor(input);
            }
        });
    }

    private static <E, D> Page<D> transformToDTO(final Page<E> resultPage,
                                                 final Pageable pageRequest,
                                                 final Function<E, D> transformation) {
        return new PageImpl<>(Lists.newLinkedList(
                Iterables.transform(resultPage, transformation)),
                pageRequest, resultPage.getTotalElements());
    }

    @Transactional(readOnly = true, rollbackFor = IOException.class)
    public HttpEntity<byte[]> download(final UUID uuid) throws IOException {
        final PersistentFileMetadata metadata = fileStorageService.getMetadata(uuid);
        final HttpHeaders headers = new HttpHeaders();

        headers.setContentLength(metadata.getContentSize());
        headers.setContentType(MediaType.parseMediaType(metadata.getContentType()));
        headers.setContentDispositionFormData("file",
                metadata.getOriginalFilename() != null
                        ? metadata.getOriginalFilename()
                        : metadata.getId().toString());

        return new HttpEntity<>(fileStorageService.getBytes(uuid), headers);
    }

    @Transactional(rollbackFor = IOException.class)
    public void upload(final AdminFileUploadDTO dto) throws IOException {
        final MultipartFile f = dto.getMultipartFile();
        final FileMetadata metadata = FileMetadata.createForMultipartFile(f);

        fileStorageService.storeFile(FileType.UNKNOWN, dto.getStorageType(), metadata, f.getInputStream());
    }

    @Transactional(rollbackFor = IOException.class)
    public void delete(final UUID uuid) {
        fileStorageService.remove(uuid);
    }
}
