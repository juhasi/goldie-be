package fi.vincit.feature.common.workaround;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.PropertyAccessor;
import com.fasterxml.jackson.databind.*;
import com.fasterxml.jackson.databind.module.SimpleModule;
import com.fasterxml.jackson.datatype.joda.JodaModule;
import com.fasterxml.jackson.module.afterburner.AfterburnerModule;
import fi.vincit.feature.common.util.RuntimeEnvironmentUtil;
import fi.vincit.feature.common.dto.PageResponse;
import org.springframework.data.domain.Page;

public class CustomJacksonObjectMapper extends ObjectMapper {
    public CustomJacksonObjectMapper(RuntimeEnvironmentUtil environmentUtil) {
        // Pretty print output when running in development environment
        this(environmentUtil.isDevelopmentEnvironment());
    }

    public CustomJacksonObjectMapper(boolean prettyPrint) {
        super();

        // Only map fields and ignore get/set methods
        setVisibility(PropertyAccessor.ALL, JsonAutoDetect.Visibility.NONE);
        setVisibility(PropertyAccessor.FIELD, JsonAutoDetect.Visibility.ANY);

        configure(SerializationFeature.INDENT_OUTPUT, prettyPrint);

        // By default serialize enums by name
        enable(SerializationFeature.WRITE_ENUMS_USING_TO_STRING);

        // Do not skip empty arrays
        enable(SerializationFeature.WRITE_EMPTY_JSON_ARRAYS);

        // Skipp null map properties
        disable(SerializationFeature.WRITE_NULL_MAP_VALUES);

        // Skip null properties
        setSerializationInclusion(JsonInclude.Include.NON_NULL);

        // Export dates as strings
        disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS);

        // Ignore unknown properties
        disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);
        disable(DeserializationFeature.FAIL_ON_NULL_FOR_PRIMITIVES);

        // Joda time date handling
        registerModule(new JodaModule());

        // Dynamic byte-code optimization
        registerModule(new AfterburnerModule());

        // Allow returning Spring Data JPA Page<?> from controller
        final SimpleModule module = new SimpleModule();
        module.addAbstractTypeMapping(Page.class, PageResponse.class);
        registerModule(module);
    }
}
