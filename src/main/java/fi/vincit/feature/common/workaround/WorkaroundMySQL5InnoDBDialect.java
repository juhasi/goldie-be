package fi.vincit.feature.common.workaround;

import org.hibernate.dialect.MySQL5InnoDBDialect;
import org.hibernate.dialect.function.StandardSQLFunction;
import org.hibernate.type.StandardBasicTypes;

import java.sql.Types;

/**
 * This class is created as a workaround for Hibernate bug: HHH-6935
 *
 * More information: https://hibernate.onjira.com/browse/HHH-6935
 */
public class WorkaroundMySQL5InnoDBDialect extends MySQL5InnoDBDialect {
    public WorkaroundMySQL5InnoDBDialect() {
        super();

        registerColumnType( Types.BOOLEAN, "bit" );

        registerFunction("convert_tz", new StandardSQLFunction("convert_tz", StandardBasicTypes.TIMESTAMP) );
    }
}
