package fi.vincit.feature.common.workaround;

import org.hibernate.Session;
import org.hibernate.jdbc.ReturningWork;
import org.springframework.jdbc.datasource.DataSourceUtils;
import org.springframework.orm.jpa.vendor.HibernateJpaDialect;
import org.springframework.transaction.TransactionDefinition;
import org.springframework.transaction.TransactionException;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceException;

import java.sql.Connection;
import java.sql.SQLException;

// original version: http://amitstechblog.wordpress.com/2011/05/31/supporting-custom-isolation-levels-with-jpa/
// and http://shahzad-mughal.blogspot.fi/2012/04/spring-jpa-hibernate-support-for-custom.html
public class IsolationSupportHibernateJpaDialect extends HibernateJpaDialect {

    /**
     * This method is overridden to set custom isolation levels on the connection
     */
    @Override
    public Object beginTransaction(final EntityManager entityManager, final TransactionDefinition definition)
            throws PersistenceException, TransactionException {

        final Session session = (Session) entityManager.getDelegate();

        if (definition.getTimeout() != TransactionDefinition.TIMEOUT_DEFAULT) {
            getSession(entityManager).getTransaction().setTimeout(definition.getTimeout());
        }

        entityManager.getTransaction().begin();

        return session.doReturningWork(new ReturningWork<Object>() {
            @Override
            public Object execute(final Connection connection) throws SQLException {
                final Integer previousIsolationLevel = DataSourceUtils.prepareConnectionForTransaction(connection, definition);

                DataSourceUtils.prepareConnectionForTransaction(connection, definition);

                final Object transactionDataFromHibernateJpaTemplate = prepareTransaction(entityManager, definition.isReadOnly(), definition.getName());

                return new IsolationSupportSessionTransactionData(transactionDataFromHibernateJpaTemplate, previousIsolationLevel, connection);
            }
        });
    }

    /* (non-Javadoc)
     * @see org.springframework.orm.jpa.vendor.HibernateJpaDialect#cleanupTransaction(java.lang.Object)
     */
    @Override
    public void cleanupTransaction(Object transactionData) {
        super.cleanupTransaction(((IsolationSupportSessionTransactionData) transactionData).getSessionTransactionDataFromHibernateTemplate());
        ((IsolationSupportSessionTransactionData) transactionData).resetIsolationLevel();
    }

    private static class IsolationSupportSessionTransactionData {

        private final Object sessionTransactionDataFromHibernateJpaTemplate;
        private final Integer previousIsolationLevel;
        private final Connection connection;

        public IsolationSupportSessionTransactionData(Object sessionTransactionDataFromHibernateJpaTemplate,
                                                      Integer previousIsolationLevel,
                                                      Connection connection) {
            this.sessionTransactionDataFromHibernateJpaTemplate = sessionTransactionDataFromHibernateJpaTemplate;
            this.previousIsolationLevel = previousIsolationLevel;
            this.connection = connection;
        }

        public void resetIsolationLevel() {
            if (this.previousIsolationLevel != null) {
                DataSourceUtils.resetConnectionAfterTransaction(connection, previousIsolationLevel);
            }
        }

        public Object getSessionTransactionDataFromHibernateTemplate() {
            return this.sessionTransactionDataFromHibernateJpaTemplate;
        }
    }
}
