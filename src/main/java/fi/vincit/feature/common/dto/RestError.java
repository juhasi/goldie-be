package fi.vincit.feature.common.dto;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.google.common.base.Preconditions;
import org.springframework.http.HttpStatus;

import java.util.ArrayList;
import java.util.List;

@JsonSerialize(include = JsonSerialize.Inclusion.NON_EMPTY)
public class RestError {
    private final HttpStatus status;
    private final int code;
    private final String message;
    private final List<RestValidationError> validationErrors;

    public RestError(final Builder builder) {
        status = Preconditions.checkNotNull(builder.status, "HttpStatus argument cannot be null.");
        code = builder.code;
        message = builder.message;
        validationErrors = builder.validationErrors;
    }

    public HttpStatus getStatus() {
        return status;
    }

    public int getCode() {
        return code;
    }

    public String getMessage() {
        return message;
    }

    public List<RestValidationError> getValidationErrors() {
        return validationErrors;
    }

    @Override
    public String toString() {
        return new StringBuilder()
                .append(getStatus().value())
                .append(" (")
                .append(getStatus().getReasonPhrase())
                .append(" )")
                .toString();
    }

    @JsonSerialize(include = JsonSerialize.Inclusion.ALWAYS)
    public static class RestValidationError {
        private final String field;
        private final String fieldErrorCode;
        private final String errorMessage;

        public RestValidationError(final String field,
                                   final String fieldErrorCode,
                                   final String errorMessage) {
            this.field = field;
            this.fieldErrorCode = fieldErrorCode;
            this.errorMessage = errorMessage;
        }

        public String getField() {
            return field;
        }

        public String getFieldErrorCode() {
            return fieldErrorCode;
        }

        public String getErrorMessage() {
            return errorMessage;
        }
    }

    public static class Builder {
        private HttpStatus status;
        private int code;
        private String message;
        private List<RestValidationError> validationErrors;

        public Builder() {
        }

        public Builder setStatus(int statusCode) {
            this.status = HttpStatus.valueOf(statusCode);
            return this;
        }

        public Builder setStatus(HttpStatus status) {
            this.status = status;
            return this;
        }

        public Builder setCode(int code) {
            this.code = code;
            return this;
        }

        public Builder setMessage(String message) {
            this.message = message;
            return this;
        }

        public Builder validationError(final String field,
                                       final String fieldErrorCode,
                                       final String errorMessage) {
            if (validationErrors == null) {
                this.validationErrors = new ArrayList<>();
            }
            validationErrors.add(new RestValidationError(field, fieldErrorCode, errorMessage));
            return this;
        }

        public RestError build() {
            if (this.status == null) {
                this.status = HttpStatus.INTERNAL_SERVER_ERROR;
            }
            return new RestError(this);
        }
    }
}
