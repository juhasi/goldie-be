package fi.vincit.feature.common.dto;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;

@JsonSerialize(include = JsonSerialize.Inclusion.NON_EMPTY)
public class GenericAPIResponse {
    private RequestStatus status;
    private Object result;

    private GenericAPIResponse() {
    }

    public static GenericAPIResponse createSuccessfulResponse(Object result) {
        final GenericAPIResponse built = new GenericAPIResponse();

        built.status = RequestStatus.SUCCESS;
        built.result = result;

        return built;
    }

    public RequestStatus getStatus() {
        return status;
    }

    public Object getResult() {
        return result;
    }

    public enum RequestStatus {
        ERROR,
        SUCCESS
    }
}
