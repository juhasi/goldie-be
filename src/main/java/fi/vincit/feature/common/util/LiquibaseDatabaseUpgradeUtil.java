package fi.vincit.feature.common.util;


import com.google.common.base.Throwables;
import liquibase.Liquibase;
import liquibase.database.Database;
import liquibase.database.DatabaseFactory;
import liquibase.database.jvm.JdbcConnection;
import liquibase.exception.LiquibaseException;
import liquibase.resource.ResourceAccessor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;
import org.springframework.core.io.support.ResourcePatternUtils;
import org.springframework.jdbc.support.JdbcUtils;

import javax.sql.DataSource;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.HashSet;
import java.util.Set;

public final class LiquibaseDatabaseUpgradeUtil {

    private static final Logger LOG = LoggerFactory.getLogger(LiquibaseDatabaseUpgradeUtil.class);

    public static interface Callback {
        public void run(Database database, Liquibase liquibase) throws LiquibaseException;
    }

    public static void upgradeDatabase(final ApplicationContext applicationContext,
                                       final DataSource dataSource,
                                       final String changelogLocation,
                                       final String contexts) {

        executeCallback(applicationContext, dataSource, changelogLocation, new Callback() {
            @Override
            public void run(Database database, Liquibase liquibase) throws LiquibaseException {
                LOG.info("Upgrading database (productId={}, version={}) using changelog file location: {}",
                        database.getDatabaseProductName(),
                        database.getDatabaseProductVersion(),
                        changelogLocation);

                liquibase.update(contexts != null ? contexts : "");
            }
        });
    }

    public static void executeCallback(final ResourceLoader resourceLoader, final DataSource dataSource,
                                       final String changeLogFile, final Callback callback) {
        Connection c = null;

        try {
            c = dataSource.getConnection();
            Database database = DatabaseFactory.getInstance().findCorrectDatabaseImplementation(new JdbcConnection(c));
            ResourceAccessor resourceAccessor = new SpringResourceOpener(resourceLoader, changeLogFile);
            callback.run(database, new Liquibase(changeLogFile, resourceAccessor, database));

        } catch (Throwable t) {
            throw Throwables.propagate(t);

        } finally {
            if (c != null) {
                try {
                    if (!c.getAutoCommit()) {
                        c.rollback();
                    }
                } catch (SQLException e) {
                    // nothing to do
                }
            }

            JdbcUtils.closeConnection(c);
        }
    }

    public static class SpringResourceOpener implements ResourceAccessor {
        private final String parentFile;
        private final ResourceLoader resourceLoader;

        public SpringResourceOpener(ResourceLoader ctx, String parentFile) {
            this.resourceLoader = ctx;
            this.parentFile = parentFile;
        }

        @Override
        public Set<String> list(String relativeTo, String path, boolean includeFiles, boolean includeDirectories, boolean recursive) throws IOException {
            final Resource[] resources = getResources(path);
            final Set<String> returnSet = new HashSet<>();

            for (Resource res : resources) {
                returnSet.add(res.getURL().toExternalForm());
            }

            return returnSet;
        }

        @Override
        public Set<InputStream> getResourcesAsStream(String path) throws IOException {
            final Resource[] resources = getResources(path);
            final Set<InputStream> returnSet = new HashSet<>();

            if (resources == null || resources.length == 0) {
                return null;
            }

            for (Resource resource : resources) {
                returnSet.add(resource.getURL().openStream());
            }

            return returnSet;
        }

        private Resource[] getResources(String path) throws IOException {
            return ResourcePatternUtils.getResourcePatternResolver(resourceLoader).getResources(adjustClasspath(path));
        }

        private String adjustClasspath(String file) {
            return isPrefixPresent(parentFile) && !isPrefixPresent(file) ? ResourceLoader.CLASSPATH_URL_PREFIX + file : file;
        }

        public static boolean isPrefixPresent(String file) {
            return file.startsWith("classpath") || file.startsWith("file:") || file.startsWith("url:");
        }

        @Override
        public ClassLoader toClassLoader() {
            return resourceLoader.getClassLoader();
        }
    }

    private LiquibaseDatabaseUpgradeUtil() {
        throw new AssertionError();
    }
}
