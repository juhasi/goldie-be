package fi.vincit.feature.common.util;

import com.google.common.base.Function;
import com.google.common.collect.Lists;
import fi.vincit.feature.common.entity.BaseEntity;
import fi.vincit.feature.common.entity.BaseEntityDTO;
import fi.vincit.feature.common.exception.RevisionConflictException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.io.Serializable;
import java.util.List;
import java.util.Objects;

public final class DtoUtil {

    private DtoUtil() {
        throw new AssertionError();
    }

    // Page of entities
    public static <ID extends Serializable, E extends BaseEntity<ID>, D extends BaseEntityDTO<ID>> Page<D> toDTO(
            @Nonnull final Page<E> resultPage,
            @Nonnull final Pageable pageRequest,
            @Nonnull final Function<E, D> function) {

        Objects.requireNonNull(resultPage, "resultPage must not be null");
        Objects.requireNonNull(pageRequest, "pageRequest must not be null");
        Objects.requireNonNull(function, "function must not be null");

        final List<D> list = Lists.transform(resultPage.getContent(), function);

        return new PageImpl<>(list, pageRequest, resultPage.getTotalElements());
    }

    public static <ID extends Serializable> void assertNoVersionConflict(
            @Nonnull final BaseEntity<ID> entity, @Nonnull final BaseEntityDTO<ID> dto) {

        if (checkForVersionConflict(entity, dto)) {
            final String msg =
                    String.format("Update is in conflict with current resource version. %s:%s:%s, %s:%s:%s",
                            entity.getClass().getSimpleName(),
                            entity.getId(),
                            entity.getConsistencyVersion(),
                            dto.getClass().getSimpleName(),
                            dto.getId(),
                            dto.getRev());
            throw new RevisionConflictException(msg);
        }
    }

    public static <ID extends Serializable> boolean checkForVersionConflict(
            @Nonnull final BaseEntity<ID> entity, @Nonnull final BaseEntityDTO<ID> dto) {

        Objects.requireNonNull(entity, "entity must not be null");
        Objects.requireNonNull(dto, "dto must not be null");

        return checkForVersionConflict(entity, dto.getRev());
    }

    // Protection for update race-conditions using DTO revision number
    public static <ID extends Serializable> boolean checkForVersionConflict(
            @Nonnull final BaseEntity<ID> entity, @Nullable final Integer rev) {

        Objects.requireNonNull(entity, "entity must not be null");

        if (rev == null || entity.getConsistencyVersion() == null) {
            // Cannot check without version information
            return false;
        }

        // Persisted entity should have earlier
        return entity.getConsistencyVersion() > rev;
    }

    public static <ID extends Serializable> void copyBaseFields(
            @Nonnull final BaseEntity<ID> entity, @Nonnull final BaseEntityDTO<ID> dto) {

        Objects.requireNonNull(entity, "entity must not be null");
        Objects.requireNonNull(dto, "dto must not be null");

        dto.setId(entity.getId());
        dto.setRev(entity.getConsistencyVersion());
    }
}
