package fi.vincit.feature.common.util;

import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

import javax.inject.Inject;
import java.net.URI;
import java.util.Locale;

@Component
public class RuntimeEnvironmentUtil {
    private final Environment environment;

    @Inject
    public RuntimeEnvironmentUtil(Environment environment) {
        this.environment = environment;
    }

    public Locale getDefaultLocale() {
        return new Locale("fi", "FI");
    }

    private String getEnvironmentId() {
        return environment.getRequiredProperty("environment.id");
    }

    public boolean isDevelopmentEnvironment() {
        return "dev".equalsIgnoreCase(getEnvironmentId());
    }

    public boolean isStagingEnvironment() {
        return "staging".equals(getEnvironmentId());
    }

    public boolean isProductionEnvironment() {
        return "prod".equalsIgnoreCase(getEnvironmentId());
    }

    public boolean isIntegrationTestEnvironment() {
        return "integration-test".equals(getEnvironmentId());
    }

    public boolean isEmailSendingEnabled() {
        return environment.getRequiredProperty("mail.enabled", boolean.class);
    }

    public URI getBackendBaseUri() {
        return environment.getProperty("server.url", URI.class);
    }

    public boolean isLoginAuditingEnabled() {
        return environment.getProperty("security.audit.login.enabled", boolean.class, false);
    }

    public boolean isPreviousPasswordCheckEnabled() {
        return environment.getRequiredProperty("password.policy.previousMatchConflicts.enabled", boolean.class);
    }

    /**
     * Number of user's previously used passwords to check for collisions.
     */
    public int getMaximumPreviousPasswordsHeld() {
        return environment.getRequiredProperty("password.policy.previousMatchConflicts.passwordsHeld", int.class);
    }
}
