package fi.vincit.feature.common.exception;

public class ExpiredTokenException extends IllegalStateException {
    public ExpiredTokenException(String s) {
        super(s);
    }
}
