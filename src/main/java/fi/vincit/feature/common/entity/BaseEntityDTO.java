package fi.vincit.feature.common.entity;

import java.io.Serializable;

public abstract class BaseEntityDTO<PK extends Serializable> {

    public abstract PK getId();

    public abstract void setId(final PK id);

    public abstract Integer getRev();

    public abstract void setRev(final Integer rev);

    @Override
    public String toString() {
        String idPart = this.getId() == null ? ":<transient object>" : (":<" + this.getId().toString() + ">");
        return this.getClass().getName() + idPart;
    }
}
