package fi.vincit.feature.common.support;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.base.Preconditions;
import com.google.common.collect.Maps;
import fi.vincit.feature.common.exception.ExpiredTokenException;
import org.joda.time.Instant;
import org.springframework.security.core.token.Token;
import org.springframework.security.core.token.TokenService;
import org.springframework.util.StringUtils;
import org.springframework.web.util.UriComponentsBuilder;

import java.io.IOException;
import java.net.URI;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

public abstract class AbstractSignedTokenLink {
    private static final String KEY_TYPE = "$";

    protected AbstractSignedTokenLink() {}

    /**
     * Builder is used to encode information to URL with signed token.
     */
    protected static abstract class Builder<T extends Builder<T>> {
        private final TokenService tokenService;
        private final String tokenType;
        private final Map<String, String> params = Maps.newHashMap();

        private URI uriBase = null;
        private String uriPath = null;
        private String uriFragment = null;
        private String uriQuery = null;

        protected Builder(final TokenService tokenService,
                          final String tokenType) {
            this.tokenService = Preconditions.checkNotNull(tokenService);
            this.tokenType = Preconditions.checkNotNull(tokenType);
            Preconditions.checkArgument(StringUtils.hasText(this.tokenType), "Empty tokenType");
        }

        public final URI buildLinkURI() {
            Preconditions.checkArgument(null != this.uriBase, "No URI base");
            Preconditions.checkState(isValid(), "Builder is not in valid state");

            final Token token = allocateLoginToken();

            return UriComponentsBuilder.fromUri(uriBase)
                    .replacePath(uriPath)
                    .replaceQuery(uriQuery)
                    .fragment(uriFragment)
                    .buildAndExpand(token.getKey())
                    .toUri();
        }

        protected boolean isValid() {
            return true;
        }

        public final T withUriBase(URI uri) {
            this.uriBase = uri;
            return getThis();
        }

        public final T withUriPath(String path) {
            this.uriPath = path;
            return getThis();
        }

        public final T withUriQuery(String query) {
            this.uriQuery = query;
            return getThis();
        }

        public final T withUriFragment(String fragment) {
            this.uriFragment = fragment;
            return getThis();
        }

        protected abstract T getThis();

        protected final void addParameter(String key, String value) {
            Preconditions.checkArgument(!KEY_TYPE.equals(key), "Cannot override key reserved for type parameter");
            this.params.put(key, value);
        }

        protected final boolean hasParameter(String key) {
            return StringUtils.hasText(this.params.get(key));
        }

        protected final Token allocateLoginToken() {
            return tokenService.allocateToken(createTokenData());
        }

        private final String createTokenData() {
            this.params.put(KEY_TYPE, tokenType);

            try {
                return new ObjectMapper().writeValueAsString(this.params);
            } catch (JsonProcessingException e) {
                throw new IllegalArgumentException("Invalid data for token", e);
            }
        }
    }

    /**
     * Parser is used to restore token information using token link data.
     * For example using: http://localhost:8080/login?token=abcd234123
     */
    protected static abstract class Parser<T extends Parser<T>> {
        private final TokenService tokenService;
        private final String tokenType;
        private long tokenValidityPeriod = TimeUnit.HOURS.toMillis(2);

        protected Parser(TokenService tokenService, String tokenType) {
            this.tokenService = Preconditions.checkNotNull(tokenService);
            this.tokenType = Preconditions.checkNotNull(tokenType);
            Preconditions.checkArgument(StringUtils.hasText(this.tokenType), "Empty tokenType");
        }

        public abstract AbstractSignedTokenLink parse(final String tokenKey);

        protected abstract T getThis();

        public T withTokenValidityPeriod(final long value, final TimeUnit timeUnit) {
            Preconditions.checkNotNull(timeUnit, "Time unit is empty");
            Preconditions.checkArgument(value > 0, "Validity time <= 0");

            this.tokenValidityPeriod = timeUnit.toMillis(value);

            return getThis();
        }

        protected Map<String, String> extractTokenAttributes(final String tokenKey) throws ExpiredTokenException {
            final Token token = getValidToken(tokenKey);

            // Check token data
            if (StringUtils.hasText(token.getExtendedInformation())) {
                final HashMap<String, String> params = parseExtendedInformation(token);

                // Check token type is as expected
                if (!tokenType.equals(params.get(KEY_TYPE))) {
                    throw new IllegalArgumentException("Token type is incorrect: " + params.get(KEY_TYPE));
                }

                return params;
            }
            throw new IllegalArgumentException("Token extendedInformation is missing");
        }

        private static HashMap<String, String> parseExtendedInformation(Token token) {
            try {
                return new ObjectMapper().readValue(
                    token.getExtendedInformation(), new TypeReference<HashMap<String, String>>() {});
            } catch (IOException e) {
                throw new IllegalArgumentException("Invalid token", e);
            }
        }

        private Token getValidToken(String tokenKey) {
            Preconditions.checkNotNull(tokenKey);

            final Token token = tokenService.verifyToken(tokenKey);

            final Instant creationTime = new Instant(token.getKeyCreationTime());

            if (creationTime.plus(tokenValidityPeriod).isBeforeNow()) {
                throw new ExpiredTokenException("Token has expired");
            }

            return token;
        }
    }
}
