package fi.vincit;

import fi.vincit.config.*;
import fi.vincit.feature.common.postinit.PostInitialize;
import fi.vincit.feature.mail.MailContext;
import fi.vincit.feature.mock.CreateInitialDatabaseContentFeature;
import org.apache.velocity.exception.VelocityException;
import org.springframework.context.annotation.*;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;
import org.springframework.http.client.ClientHttpRequestFactory;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.ui.velocity.VelocityEngineFactoryBean;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.multipart.MultipartResolver;
import org.springframework.web.multipart.commons.CommonsMultipartResolver;
import org.springframework.web.multipart.support.MultipartFilter;

import javax.annotation.Resource;
import java.io.IOException;
import java.util.Properties;

@Configuration
@Import({
        DataSourceConfig.class,
        JPAConfig.class,
        CacheConfig.class,
        LocalizationConfig.class,
        AsyncConfig.class,
        MailContext.class,
        SecurityConfig.class,
        AopConfig.class
})
@PropertySource({"classpath:configuration/application.properties", "classpath:git.properties"})
@ComponentScan({"fi.vincit.feature"})
public class GoldieApplicationContext {
    // Limit upload size to 50 MBytes
    public static final long MAX_UPLOAD_SIZE = 50 * 1024 * 1024;

    @Resource
    private CreateInitialDatabaseContentFeature initialDatabaseContentFeature;

    @PostInitialize
    public void afterStartup() {
        initialDatabaseContentFeature.initSampleContent();
    }

    @Bean
    public static PropertySourcesPlaceholderConfigurer propertySourcesPlaceholderConfigurer() {
        return new PropertySourcesPlaceholderConfigurer();
    }

    @Bean(name = MultipartFilter.DEFAULT_MULTIPART_RESOLVER_BEAN_NAME)
    public MultipartResolver multipartResolver() {
        CommonsMultipartResolver multipartResolver = new CommonsMultipartResolver();
        multipartResolver.setMaxUploadSize(MAX_UPLOAD_SIZE);
        multipartResolver.setDefaultEncoding("UTF-8");

        return multipartResolver;
    }

    @Bean
    public VelocityEngineFactoryBean velocityEngine() throws VelocityException, IOException {
        VelocityEngineFactoryBean engineFactory = new VelocityEngineFactoryBean();
        engineFactory.setResourceLoaderPath("classpath:/velocity/");
        Properties properties = new Properties();
        properties.put("input.encoding", "UTF-8");
        properties.put("output.encoding", "UTF-8");
        engineFactory.setVelocityProperties(properties);
        engineFactory.afterPropertiesSet();
        return engineFactory;
    }

    /**
     * Spring RestTemplate should be configured to use shared HTTP request factory.
     *
     * @return
     */
    @Bean
    public ClientHttpRequestFactory clientHttpRequestFactory() {
        HttpComponentsClientHttpRequestFactory factory = new HttpComponentsClientHttpRequestFactory();
        factory.setConnectTimeout(5 * 1000);
        factory.setReadTimeout(10 * 1000);

        return factory;
    }

    @Bean
    public RestTemplate defaultRestTemplate() {
        return new RestTemplate(clientHttpRequestFactory());
    }
}