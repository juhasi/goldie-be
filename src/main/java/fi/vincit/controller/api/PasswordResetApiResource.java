package fi.vincit.controller.api;

import fi.vincit.feature.account.PasswordResetFeature;
import fi.vincit.feature.account.dto.ForgotPasswordDTO;
import fi.vincit.feature.account.dto.PasswordResetDTO;
import org.springframework.http.HttpStatus;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

@RestController
public class PasswordResetApiResource {
    @Resource
    private PasswordResetFeature passwordResetFeature;

    @ResponseStatus(HttpStatus.NO_CONTENT)
    @RequestMapping(value = "/api/v1/password/forgot", method = RequestMethod.POST)
    public void forgotPassword(@RequestBody @Validated ForgotPasswordDTO dto) {
        passwordResetFeature.sendPasswordResetEmail(dto.getEmail());
    }

    @ResponseStatus(HttpStatus.NO_CONTENT)
    @RequestMapping(value = "/api/v1/password/reset", method = RequestMethod.POST)
    public void resetPassword(@RequestBody @Validated PasswordResetDTO dto) {
        passwordResetFeature.processPasswordReset(dto);
    }
}