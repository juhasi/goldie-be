package fi.vincit.controller.api;

import fi.vincit.controller.interceptor.cache.CacheControl;
import fi.vincit.controller.interceptor.cache.CachePolicy;
import fi.vincit.feature.account.AccountEditFeature;
import fi.vincit.feature.account.dto.AccountDTO;
import fi.vincit.feature.account.dto.ChangePasswordDTO;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import javax.validation.Valid;

@RestController
@RequestMapping(value = "/api/v1/account", produces = MediaType.APPLICATION_JSON_VALUE)
public class AccountApiResource {
    @Inject
    private AccountEditFeature accountEditFeature;

    /**
     * GET  /account -> get the current user.
     */
    @CacheControl(policy = CachePolicy.NO_CACHE)
    @RequestMapping(method = RequestMethod.GET)
    public AccountDTO getAccount() {
        return accountEditFeature.getActiveAccount();
    }

    /**
     * PUT, POST, PATCH  /account -> update the current user information.
     */
    @RequestMapping(method = {RequestMethod.POST, RequestMethod.PUT, RequestMethod.PATCH})
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void saveAccount(@RequestBody @Valid AccountDTO dto) {
        accountEditFeature.updateActiveAccount(dto);
    }

    /**
     * POST  /account/password -> changes the current user's password
     */
    @RequestMapping(value = "password", method = RequestMethod.POST)
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void changePassword(@RequestBody @Valid ChangePasswordDTO dto) {
        accountEditFeature.changeActiveUserPassword(dto);
    }
}
