package fi.vincit.controller.api;

import fi.vincit.controller.interceptor.cache.CacheControl;
import fi.vincit.controller.interceptor.cache.CachePolicy;
import fi.vincit.feature.common.dto.GenericAPIResponse;
import fi.vincit.feature.common.exception.NotFoundException;
import fi.vincit.feature.common.exception.NotImplementedException;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(value = "/api/test")
public class TestApiResource {

    @CacheControl(policy = CachePolicy.NO_CACHE)
    @RequestMapping(value = "/hello", produces = "text/plain")
    public String hello() {
        return "Hello World!";
    }

    @RequestMapping(value = "/helloPOST", method = RequestMethod.POST, produces = "text/plain")
    public String helloPOST(@RequestBody String body) {
        return "Hello " + body + "!";
    }

    @CacheControl(policy = CachePolicy.NO_CACHE)
    @RequestMapping(value = "/helloJSON", produces = "application/json")
    public GenericAPIResponse helloJSON() {
        return GenericAPIResponse.createSuccessfulResponse("Hello World!");
    }

    @CacheControl(policy = CachePolicy.NO_CACHE)
    @RequestMapping(value = "/secure/hello", produces = "text/plain")
    public String helloSecure() {
        return "Hello World!";
    }

    @RequestMapping(value = "/throw")
    public String throwTestException(@RequestParam(required = false) String type) {
        if ("state".equals(type)) {
            throw new IllegalStateException("state");
        } else if (type.equals("argument")) {
            throw new IllegalArgumentException("argument");
        } else if (type.equals("notfound")) {
            throw new NotFoundException("notfound");
        } else if (type.equals("notimplemented")) {
            throw new NotImplementedException("notimplemented");
        } else if (type.equals("accessdenied")) {
            throw new AccessDeniedException("accessdenied");
        } else {
            throw new RuntimeException("unknown");
        }
    }
}