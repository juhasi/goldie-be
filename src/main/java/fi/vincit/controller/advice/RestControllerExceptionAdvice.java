package fi.vincit.controller.advice;

import fi.vincit.feature.common.dto.RestError;
import fi.vincit.feature.common.exception.NotFoundException;
import fi.vincit.feature.common.exception.NotImplementedException;
import fi.vincit.feature.common.exception.RevisionConflictException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.ConversionNotSupportedException;
import org.springframework.beans.TypeMismatchException;
import org.springframework.dao.OptimisticLockingFailureException;
import org.springframework.http.*;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.http.converter.HttpMessageNotWritableException;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;
import org.springframework.validation.BindException;
import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;
import org.springframework.web.HttpMediaTypeNotAcceptableException;
import org.springframework.web.HttpMediaTypeNotSupportedException;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.ServletRequestBindingException;
import org.springframework.web.bind.UnsatisfiedServletRequestParameterException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.multipart.support.MissingServletRequestPartException;
import org.springframework.web.servlet.mvc.multiaction.NoSuchRequestHandlingMethodException;

import javax.validation.ValidationException;
import java.util.List;
import java.util.Set;

@ControllerAdvice(annotations = RestController.class)
public class RestControllerExceptionAdvice {
    private static final Logger LOG = LoggerFactory.getLogger(RestControllerExceptionAdvice.class);

    @ExceptionHandler(value = Exception.class)
    public ResponseEntity<RestError> handleGeneralException(Exception ex, WebRequest request) {
        return handleExceptionInternal(ex, false, new HttpHeaders(), HttpStatus.INTERNAL_SERVER_ERROR, request);
    }

    @ExceptionHandler(value = {
            IllegalArgumentException.class,
            IllegalStateException.class,
            ValidationException.class,
            NotFoundException.class,
            NotImplementedException.class,
            RevisionConflictException.class,
            OptimisticLockingFailureException.class,
            AccessDeniedException.class
    })
    public ResponseEntity<RestError> handleCustomException(Exception ex, WebRequest request) {
        final HttpHeaders headers = new HttpHeaders();

        if (ex instanceof IllegalArgumentException) {
            return handleExceptionInternal(ex, false, headers, HttpStatus.BAD_REQUEST, request);
        } else if (ex instanceof IllegalStateException) {
            return handleExceptionInternal(ex, false, headers, HttpStatus.INTERNAL_SERVER_ERROR, request);
        } else if (ex instanceof ValidationException) {
            return handleExceptionInternal(ex, false, headers, HttpStatus.BAD_REQUEST, request);
        } else if (ex instanceof NotFoundException) {
            return handleExceptionInternal(ex, true, headers, HttpStatus.NOT_FOUND, request);
        } else if (ex instanceof NotImplementedException) {
            return handleExceptionInternal(ex, true, headers, HttpStatus.NOT_IMPLEMENTED, request);
        } else if (ex instanceof RevisionConflictException) {
            return handleExceptionInternal(ex, true, headers, HttpStatus.CONFLICT, request);
        } else if (ex instanceof OptimisticLockingFailureException) {
            return handleExceptionInternal(ex, false, headers, HttpStatus.CONFLICT, request);
        } else if (ex instanceof AccessDeniedException) {
            return handleExceptionInternal(ex, false, headers, HttpStatus.FORBIDDEN, request);
        } else {
            LOG.warn("Unknown exception type: " + ex.getClass().getName());

            return handleExceptionInternal(ex, false, headers, HttpStatus.INTERNAL_SERVER_ERROR, request);
        }
    }

    // For reference: org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler
    @ExceptionHandler(value = {
            NoSuchRequestHandlingMethodException.class,
            HttpRequestMethodNotSupportedException.class,
            HttpMediaTypeNotSupportedException.class,
            HttpMediaTypeNotAcceptableException.class,
            MissingServletRequestParameterException.class,
            UnsatisfiedServletRequestParameterException.class,
            ServletRequestBindingException.class,
            ConversionNotSupportedException.class,
            TypeMismatchException.class,
            HttpMessageNotReadableException.class,
            HttpMessageNotWritableException.class,
            MethodArgumentNotValidException.class,
            MissingServletRequestPartException.class,
            BindException.class
    })
    public static final ResponseEntity<RestError> handleException(Exception ex, WebRequest request) {
        final HttpHeaders headers = new HttpHeaders();

        if (ex instanceof NoSuchRequestHandlingMethodException) {
            return handleExceptionInternal(ex, false, headers, HttpStatus.NOT_FOUND, request);
        } else if (ex instanceof HttpRequestMethodNotSupportedException) {
            Set<HttpMethod> supportedMethods = ((HttpRequestMethodNotSupportedException) ex).getSupportedHttpMethods();
            if (!supportedMethods.isEmpty()) {
                headers.setAllow(supportedMethods);
            }
            return handleExceptionInternal(ex, false, headers, HttpStatus.METHOD_NOT_ALLOWED, request);
        } else if (ex instanceof HttpMediaTypeNotSupportedException) {
            HttpStatus status = HttpStatus.UNSUPPORTED_MEDIA_TYPE;
            List<MediaType> mediaTypes = ((HttpMediaTypeNotSupportedException) ex).getSupportedMediaTypes();
            if (!CollectionUtils.isEmpty(mediaTypes)) {
                headers.setAccept(mediaTypes);
            }
            return handleExceptionInternal(ex, false, headers, status, request);
        } else if (ex instanceof HttpMediaTypeNotAcceptableException) {
            return handleExceptionInternal(ex, false, headers, HttpStatus.NOT_ACCEPTABLE, request);
        } else if (ex instanceof MissingServletRequestParameterException) {
            return handleExceptionInternal(ex, true, headers, HttpStatus.BAD_REQUEST, request);
        } else if (ex instanceof UnsatisfiedServletRequestParameterException) {
            return handleExceptionInternal(ex, true, headers, HttpStatus.BAD_REQUEST, request);
        } else if (ex instanceof ServletRequestBindingException) {
            return handleExceptionInternal(ex, false, headers, HttpStatus.BAD_REQUEST, request);
        } else if (ex instanceof ConversionNotSupportedException) {
            return handleExceptionInternal(ex, false, headers, HttpStatus.INTERNAL_SERVER_ERROR, request);
        } else if (ex instanceof TypeMismatchException) {
            return handleExceptionInternal(ex, false, headers, HttpStatus.BAD_REQUEST, request);
        } else if (ex instanceof HttpMessageNotReadableException) {
            return handleExceptionInternal(ex, false, headers, HttpStatus.BAD_REQUEST, request);
        } else if (ex instanceof HttpMessageNotWritableException) {
            return handleExceptionInternal(ex, false, headers, HttpStatus.INTERNAL_SERVER_ERROR, request);
        } else if (ex instanceof MethodArgumentNotValidException) {
            return handleMethodArgumentNotValidException((MethodArgumentNotValidException) ex, headers, HttpStatus.BAD_REQUEST);
        } else if (ex instanceof MissingServletRequestPartException) {
            return handleExceptionInternal(ex, true, headers, HttpStatus.BAD_REQUEST, request);
        } else if (ex instanceof BindException) {
            return handleExceptionInternal(ex, false, headers, HttpStatus.BAD_REQUEST, request);
        } else {
            LOG.warn("Unknown exception type: " + ex.getClass().getName());

            return handleExceptionInternal(ex, false, headers, HttpStatus.INTERNAL_SERVER_ERROR, request);
        }
    }

    private static ResponseEntity<RestError> handleMethodArgumentNotValidException(
            final MethodArgumentNotValidException ex, final HttpHeaders headers, final HttpStatus status) {

        final RestError.Builder builder = new RestError.Builder()
                .setStatus(status)
                .setMessage(ex.getMessage())
                .setCode(status.value());

        for (final ObjectError objectError : ex.getBindingResult().getGlobalErrors()) {
            builder.validationError(objectError.getObjectName(), objectError.getCode(), objectError.getDefaultMessage());
        }

        for (final FieldError fieldError : ex.getBindingResult().getFieldErrors()) {
            builder.validationError(fieldError.getField(), fieldError.getCode(), fieldError.getDefaultMessage());
        }

        return new ResponseEntity<>(builder.build(), headers, status);
    }

    private static ResponseEntity<RestError> handleExceptionInternal(final Exception ex,
                                                                     final boolean exposeExceptionMessage,
                                                                     final HttpHeaders headers,
                                                                     final HttpStatus status,
                                                                     final WebRequest request) {

        final RestError.Builder builder = new RestError.Builder()
                .setStatus(status)
                .setCode(status.value());

        if (exposeExceptionMessage && StringUtils.hasText(ex.getMessage())) {
            builder.setMessage(ex.getMessage());
        } else {
            builder.setMessage(status.getReasonPhrase());
        }

        if (HttpStatus.INTERNAL_SERVER_ERROR.equals(status)) {
            request.setAttribute("javax.servlet.error.exception", ex, WebRequest.SCOPE_REQUEST);
        }

        return new ResponseEntity<>(builder.build(), headers, status);
    }

}
