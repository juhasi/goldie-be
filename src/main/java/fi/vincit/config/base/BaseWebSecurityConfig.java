package fi.vincit.config.base;

import fi.vincit.config.properties.SecurityConfigProperties;
import fi.vincit.feature.common.util.RuntimeEnvironmentUtil;
import fi.vincit.security.authentication.CustomAuthenticationFailureHandler;
import fi.vincit.security.authentication.CustomAuthenticationSuccessHandler;
import fi.vincit.security.authentication.Http401UnauthorizedEntryPoint;
import fi.vincit.security.csrf.CookieCsrfTokenRepository;
import org.springframework.context.annotation.Bean;
import org.springframework.security.access.expression.SecurityExpressionHandler;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.web.FilterInvocation;
import org.springframework.security.web.authentication.LoginUrlAuthenticationEntryPoint;
import org.springframework.security.web.authentication.RememberMeServices;
import org.springframework.security.web.authentication.logout.LogoutHandler;
import org.springframework.security.web.authentication.logout.LogoutSuccessHandler;
import org.springframework.security.web.util.matcher.RequestMatcher;

import javax.annotation.Resource;

public class BaseWebSecurityConfig extends WebSecurityConfigurerAdapter {
    @Resource
    protected RuntimeEnvironmentUtil runtimeEnvironmentUtil;

    @Resource
    protected SecurityConfigProperties properties;

    @Resource
    protected SecurityExpressionHandler<FilterInvocation> webSecurityExpressionHandler;

    @Resource
    protected RememberMeServices rememberMeServices;

    @Resource
    protected LogoutSuccessHandler logoutSuccessHandler;

    @Resource
    protected LogoutHandler logoutAuditEventListener;

    @Resource
    protected CustomAuthenticationSuccessHandler authenticationSuccessHandler;

    @Resource
    protected CustomAuthenticationFailureHandler authenticationFailureHandler;

    @Resource
    protected CookieCsrfTokenRepository csrfTokenRepository;

    @Resource(name = "csrfRequestMatcher")
    protected RequestMatcher csrfRequestMatcher;

    @Bean
    public LoginUrlAuthenticationEntryPoint mainEntryPoint() {
        return new LoginUrlAuthenticationEntryPoint(properties.getLoginPageUrl());
    }

    @Bean
    public Http401UnauthorizedEntryPoint apiEntryPoint() {
        return new Http401UnauthorizedEntryPoint();
    }
}