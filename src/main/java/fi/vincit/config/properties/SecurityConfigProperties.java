package fi.vincit.config.properties;

import org.springframework.beans.factory.annotation.Value;

public class SecurityConfigProperties {
    @Value("${security.remember.key}")
    private String anonymousAuthenticationKey;

    @Value("${security.remember.key}")
    private String rememberMeSecretKey;

    @Value("${security.remember.token.validity.seconds}")
    private int rememberMeTokenValiditySeconds;

    @Value("${security.logout.action}")
    private String logoutAction;

    @Value("${security.login.page}")
    private String loginPageUrl;

    @Value("${security.login.action}")
    private String loginActionUrl;

    @Value("${security.basic.realm}")
    private String basicAuthenticationRealm;

    @Value("${security.token.secret}")
    private String tokenGenerationSecret;

    @Value("${security.token.salt}")
    private String tokenGenerationSalt;

    @Value("${security.token.integer}")
    private int tokenGenerationInteger;

    public String getRememberMeSecretKey() {
        return rememberMeSecretKey;
    }

    public int getRememberMeTokenValiditySeconds() {
        return rememberMeTokenValiditySeconds;
    }

    public String getAnonymousAuthenticationKey() {
        return anonymousAuthenticationKey;
    }

    public String getLogoutAction() {
        return logoutAction;
    }

    public String getLoginPageUrl() {
        return loginPageUrl;
    }

    public String getLoginActionUrl() {
        return loginActionUrl;
    }

    public String getBasicAuthenticationRealm() {
        return basicAuthenticationRealm;
    }

    public String getTokenGenerationSecret() {
        return tokenGenerationSecret;
    }

    public String getTokenGenerationSalt() {
        return tokenGenerationSalt;
    }

    public int getTokenGenerationInteger() {
        return tokenGenerationInteger;
    }
}
