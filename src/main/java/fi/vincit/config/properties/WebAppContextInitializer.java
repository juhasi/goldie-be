package fi.vincit.config.properties;

import com.google.common.base.Joiner;
import com.google.common.base.Splitter;
import com.google.common.base.Throwables;

import fi.vincit.config.Constants;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContextInitializer;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.core.env.ConfigurableEnvironment;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.FileSystemResource;
import org.springframework.core.io.Resource;
import org.springframework.core.io.support.ResourcePropertySource;
import org.springframework.util.StringUtils;
import org.springframework.web.WebApplicationInitializer;
import org.springframework.web.context.ContextLoader;

import javax.servlet.ServletContext;

import java.io.IOException;
import java.util.Properties;

/**
 * This ContextInitializer loads properties in environment override file,
 * if such file is specified in build profile. File should be used to store sensitive
 * credentials such as the database password on production server.
 */
@Order(Ordered.LOWEST_PRECEDENCE)
public class WebAppContextInitializer
        implements WebApplicationInitializer, ApplicationContextInitializer<ConfigurableApplicationContext> {

    public static final String PROPERTY_ENVIRONMENT_CONFIG = "environment.override";
    public static final String PATH_APPLICATION_PROPERTIES = "configuration/application.properties";

    private static final Logger LOG = LoggerFactory.getLogger(WebAppContextInitializer.class);

    private static final String ACTIVE_PROFILES_PROPERTY = "spring.profiles.active";

    private static final String[] DEFAULT_SPRING_PROFILES = new String[] {
            Constants.STANDARD_DATABASE
    };

    @Override
    public void onStartup(ServletContext servletContext) {
        // Process Spring applicationContext using this class after ContextLoader has finished
        servletContext.setInitParameter(
                ContextLoader.CONTEXT_INITIALIZER_CLASSES_PARAM, WebAppContextInitializer.class.getCanonicalName());
    }

    @Override
    public void initialize(ConfigurableApplicationContext applicationContext) {
        final ConfigurableEnvironment configurableEnvironment = applicationContext.getEnvironment();

        try {
            final String environmentConfigPath = getEnvironmentOverrideFileName();

            if (StringUtils.hasText(environmentConfigPath)) {
                addPropertySourceIfExists(configurableEnvironment, new FileSystemResource(environmentConfigPath));
            } else {
                LOG.info("No environment override config file loaded");
            }

        } catch (IOException e) {
            Throwables.propagate(e);
        }

        setActiveProfiles(configurableEnvironment);
    }

    private static String getEnvironmentOverrideFileName() throws IOException {
        final Properties properties = new Properties();
        properties.load(new ClassPathResource(PATH_APPLICATION_PROPERTIES).getInputStream());
        return properties.getProperty(PROPERTY_ENVIRONMENT_CONFIG);
    }

    private static void addPropertySourceIfExists(
            ConfigurableEnvironment configurableEnvironment, Resource overrideResource) {

        final String path = overrideResource.getFilename();

        if (overrideResource.exists() && overrideResource.isReadable()) {
            LOG.info("Trying to load properties from: {}", path);

            addPropertySource(configurableEnvironment, overrideResource);

        } else {
            LOG.warn("Could not open properties file: {}", path);
        }
    }

    private static void addPropertySource(ConfigurableEnvironment configurableEnvironment, Resource resource) {
        try {
            configurableEnvironment.getPropertySources().addFirst(new ResourcePropertySource(resource));

        } catch (IOException e) {
            throw Throwables.propagate(e);
        }
    }

    private static void setActiveProfiles(final ConfigurableEnvironment env) {
        final String activeProfilesPropertyValue = System.getProperty(ACTIVE_PROFILES_PROPERTY);

        LOG.debug("{}: {}", ACTIVE_PROFILES_PROPERTY, StringUtils.quoteIfString(activeProfilesPropertyValue));

        boolean activeProfilesFound = false;

        if (StringUtils.hasText(activeProfilesPropertyValue)) {
            final Splitter splitter = Splitter.on(",").trimResults().omitEmptyStrings();

            if (splitter.split(activeProfilesPropertyValue).iterator().hasNext()) {
                activeProfilesFound = true;
            }
        }

        if (!activeProfilesFound) {
            env.setActiveProfiles(DEFAULT_SPRING_PROFILES);
            LOG.info("Activated default Spring profiles: {}", Joiner.on(',').skipNulls().join(DEFAULT_SPRING_PROFILES));
        }
    }

}
