package fi.vincit.config;

import fi.vincit.config.base.BaseSecurityConfig;
import fi.vincit.feature.account.entity.SystemUser;
import fi.vincit.security.authentication.CustomUserDetailsService;
import fi.vincit.security.authorization.EntityPermissionEvaluator;
import fi.vincit.security.crypto.SafeBase64TextEncryptor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.access.PermissionEvaluator;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.encrypt.Encryptors;
import org.springframework.security.crypto.encrypt.TextEncryptor;
import org.springframework.security.crypto.password.PasswordEncoder;

@Configuration
public class SecurityConfig extends BaseSecurityConfig {

    @Override
    protected void configure(AuthenticationManagerBuilder builder) {
        builder.authenticationProvider(daoAuthenticationProvider())
                .authenticationProvider(rememberMeAuthenticationProvider())
                .authenticationProvider(preAuthenticatedAuthenticationProvider())
                .authenticationProvider(anonymousAuthenticationProvider())
                .eraseCredentials(true);
    }

    @Bean
    @Override
    public PasswordEncoder passwordEncoder() {
        // OpenBSD Blowish encoder is most advanced and
        // most CPU intensive (~100 ms) PasswordEncoder implementation.
        return new BCryptPasswordEncoder(10);
    }

    @Bean
    public TextEncryptor tokenEncryptor() {
        return new SafeBase64TextEncryptor(Encryptors.standard(
                securityConfigurationProperties().getTokenGenerationSecret(),
                securityConfigurationProperties().getTokenGenerationSalt()));
    }

    @Bean
    @Override
    public UserDetailsService userDetailsService() {
        return new CustomUserDetailsService();
    }

    @Override
    protected String[] getRoleMappings() {
        // Implicit role mappings can be used to reduce configuration
        // for multiple allowed roles.
        return new String[]{
                SystemUser.Role.ROLE_ADMIN.includes(SystemUser.Role.ROLE_USER),
                SystemUser.Role.ROLE_USER.includes(SystemUser.Role.ROLE_REST)
        };
    }

    @Bean
    @Override
    public PermissionEvaluator permissionEvaluator() {
        // PermissionEvaluator is required to use domain entity checks
        // using @PreAuthorize() or JSP security tags. Default is disabled.
        // Example: hasPermission(#entityDTO, 'write')
        return new EntityPermissionEvaluator();
    }
}
