package fi.vincit.config;

public final class Constants {

    // Spring 3.1 profiles
    public static final String STANDARD_DATABASE = "standardDatabase";
    public static final String EMBEDDED_DATABASE = "embeddedDatabase";

    private Constants() {
        throw new AssertionError();
    }

}
