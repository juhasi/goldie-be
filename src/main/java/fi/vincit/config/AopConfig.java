package fi.vincit.config;

import fi.vincit.feature.common.postinit.PostInitializerRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.EnableAspectJAutoProxy;

@Configuration
@EnableAspectJAutoProxy//(proxyTargetClass = true)
@ComponentScan("fi.vincit.aspect")
public class AopConfig {
    // Advice order, lower value = higher priority
    public static final int ORDER_RETRY = 10; // HIGHEST priority
    public static final int ORDER_PERFORMANCE_MONITORING = 20;
    public static final int ORDER_TRANSACTION = 30;
    public static final int ORDER_METHOD_SECURITY = 40; // LOWEST priority

    @Bean
    public PostInitializerRunner postInitializerRunner() {
        return new PostInitializerRunner();
    }
}
