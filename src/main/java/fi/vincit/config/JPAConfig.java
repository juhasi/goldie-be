package fi.vincit.config;

import fi.vincit.feature.common.util.LiquibaseDatabaseUpgradeUtil;
import fi.vincit.feature.common.workaround.CustomHibernateNamingStrategy;
import fi.vincit.feature.common.workaround.IsolationSupportHibernateJpaDialect;

import org.hibernate.cfg.AvailableSettings;
import org.hibernate.loader.BatchFetchStyle;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AdviceMode;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.dao.annotation.PersistenceExceptionTranslationPostProcessor;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.transaction.support.TransactionTemplate;

import javax.annotation.Resource;
import javax.sql.DataSource;

import java.util.HashMap;
import java.util.Map;

@Configuration
@EnableTransactionManagement(mode = AdviceMode.PROXY, order = AopConfig.ORDER_TRANSACTION)
@EnableJpaRepositories({
        "fi.vincit.feature.account.dao",
        "fi.vincit.feature.storage.dao",
        "fi.vincit.feature.mail.dao",
        "fi.vincit.feature.account.dao"
})
@PropertySource(value = {
        "classpath:configuration/db.properties",
        "classpath:configuration/jpa.properties" })
public class JPAConfig {

    public static final String MODEL_CLASS_PACKAGES[] = {
            "fi.vincit.feature.common.entity",
            "fi.vincit.feature.account.entity",
            "fi.vincit.feature.mail.entity",
            "fi.vincit.feature.storage.entity"
    };

    public static final String LIQUIBASE_CHANGELOG_LOCATION = "migrations/db.changelog.xml";

    @Resource
    private ApplicationContext applicationContext;

    @Resource
    protected Environment environment;

    @Resource
    private DataSourceContext dataSourceContext;

    @Bean
    public JpaTransactionManager transactionManager() {
        return new JpaTransactionManager(entityManagerFactory().getObject());
    }

    @Bean
    public TransactionTemplate transactionTemplate() {
        return new TransactionTemplate(transactionManager());
    }

    @Bean
    public PersistenceExceptionTranslationPostProcessor exceptionTranslationPostProcessor() {
        // Translate native persistence exceptions into Spring DataAccessException hierarchy
        return new PersistenceExceptionTranslationPostProcessor();
    }

    @Bean
    public LocalContainerEntityManagerFactoryBean entityManagerFactory() {
        // Run Liquibase migrations before ORM setup
        if (isLiquibaseUpgradeEnabled()) {
            upgradeDatabase(dataSourceContext.getDataSource());
        }

        final HibernateJpaVendorAdapter jpaVendorAdapter = new HibernateJpaVendorAdapter();
        jpaVendorAdapter.setDatabasePlatform(dataSourceContext.getHibernateDatabaseDialect());

        final LocalContainerEntityManagerFactoryBean emf = new LocalContainerEntityManagerFactoryBean();

        // Extended dialect supports changing dataSource default isolation level per transaction
        emf.setJpaDialect(new IsolationSupportHibernateJpaDialect());
        emf.setJpaVendorAdapter(jpaVendorAdapter);
        emf.setJpaPropertyMap(getEntityManagerProperties(dataSourceContext.getHbm2ddlAutoMode()));
        emf.setDataSource(dataSourceContext.getDataSource());
        emf.setPackagesToScan(MODEL_CLASS_PACKAGES);

        return emf;
    }

    protected boolean isLiquibaseUpgradeEnabled() {
        return environment.getRequiredProperty("liquibase.enabled", boolean.class);
    }

    protected void upgradeDatabase(final DataSource dataSource) {
        LiquibaseDatabaseUpgradeUtil.upgradeDatabase(
            applicationContext,
            dataSource,
            LIQUIBASE_CHANGELOG_LOCATION,
            environment.getProperty("liquibase.contexts", ""));
    }

    // @see org.hibernate.cfg.AvailableSettings
    protected Map<String, ?> getEntityManagerProperties(final String hbm2ddlAuto) {
        final Map<String, Object> props = new HashMap<>();

        configureJodaTimeIntegration(props);
        configureJDBCBatchUpdates(props);
        configureJSR303Validation(props);

        props.put("hibernate.connection.charSet", "UTF-8");
        props.put(AvailableSettings.HBM2DDL_AUTO, hbm2ddlAuto);
        props.put(AvailableSettings.SHOW_SQL, environment.getRequiredProperty("hibernate.show_sql", boolean.class));
        props.put(AvailableSettings.FORMAT_SQL, environment.getRequiredProperty("hibernate.format_sql", boolean.class));
        props.put(
            AvailableSettings.GENERATE_STATISTICS,
            environment.getRequiredProperty("hibernate.generate_statistics", boolean.class));
        props.put(
            AvailableSettings.USE_STREAMS_FOR_BINARY,
            environment.getRequiredProperty("hibernate.jdbc.use_streams_for_binary", boolean.class));

        // Automatically map column names using underscores
        props.put(org.hibernate.jpa.AvailableSettings.NAMING_STRATEGY, CustomHibernateNamingStrategy.class.getName());

        // Pessimistic locking for at most 15 sec (default = wait forever)
        props.put(org.hibernate.jpa.AvailableSettings.LOCK_TIMEOUT, "15000");

        // Hibernate defaults to EXTENDED locking
        props.put(org.hibernate.jpa.AvailableSettings.LOCK_SCOPE, null);

        // Sets a maximum "depth" for the outer join fetch tree for single-ended associations (one-to-one, many-to-one).
        // A 0 disables default outer join fetching. e.g. recommended values between 0 and 3
        props.put(AvailableSettings.MAX_FETCH_DEPTH, 1);

        // Should be disabled. See https://hibernate.atlassian.net/browse/HHH-227
        props.put(AvailableSettings.USE_REFLECTION_OPTIMIZER, false);

        return props;
    }

    private static void configureJSR303Validation(Map<String, Object> props) {
        // JSR303 Validator (Hibernate Validator) config
        props.put(org.hibernate.jpa.AvailableSettings.VALIDATION_MODE, "callback, ddl");
        props.put(org.hibernate.jpa.AvailableSettings.PERSIST_VALIDATION_GROUP, "javax.validation.groups.Default");
        props.put(org.hibernate.jpa.AvailableSettings.UPDATE_VALIDATION_GROUP, "javax.validation.groups.Default");
    }

    private static void configureJDBCBatchUpdates(Map<String, Object> props) {
        props.put(AvailableSettings.BATCH_FETCH_STYLE, BatchFetchStyle.PADDED);

        // A non-zero value enables use of JDBC2 batch updates by Hibernate. e.g. recommended values between 5 and 30
        props.put(AvailableSettings.STATEMENT_BATCH_SIZE, 32);

        // Set this property to true if your JDBC driver returns correct row counts from executeBatch().
        // It is usually safe to turn this option on. Hibernate will then
        // use batched DML for automatically versioned data. Defaults to false.
        props.put(AvailableSettings.BATCH_VERSIONED_DATA, true);

        // Sets a default size for Hibernate batch fetching of associations. e.g. recommended values 4, 8, 16
        props.put(AvailableSettings.DEFAULT_BATCH_FETCH_SIZE, 16);

        // Forces Hibernate to order SQL updates by the primary key value of the items being updated.
        // This will result in fewer transaction deadlocks in highly concurrent systems.
        props.put(AvailableSettings.ORDER_UPDATES, true);

        // Enable ordering of insert statements for the purpose of more efficient JDBC batching.
        props.put(AvailableSettings.ORDER_INSERTS, true);
    }

    /**
     * UserType configuration for Joda-time. Automatically registered types require no @Type mapping are:
     * PersistentBigMoneyAmount, PersistentMoneyAmount, PersistentCurrency, PersistentCurrencyUnit
     * PersistentDateTime, PersistentDurationAsString, PersistentInstantAsTimestamp
     * PersistentLocalDate, PersistentLocalTime,
     * PersistentMonthDayAsString, PersistentPeriodAsString,
     * PersistentTimeOfDay, PersistentYearMonthDay, PersistentYears
     * ref: http://blog.jadira.co.uk/blog/2012/1/19/release-jadira-usertype-300cr1-with-support-for-joda-money-0.html
     */
    private static void configureJodaTimeIntegration(final Map<String, Object> props) {
        props.put("jadira.usertype.autoRegisterUserTypes", "true");
        props.put("jadira.usertype.currencyCode", "EUR");
        props.put("jadira.usertype.seed", "org.jadira.usertype.spi.shared.JvmTimestampSeed");
    }

}
