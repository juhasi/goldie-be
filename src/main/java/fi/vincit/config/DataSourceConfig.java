package fi.vincit.config;

import com.jolbox.bonecp.BoneCPDataSource;

import fi.vincit.config.profile.EmbeddedDatabase;
import fi.vincit.config.profile.StandardDatabase;
import fi.vincit.feature.common.util.RuntimeEnvironmentUtil;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseBuilder;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseType;
import org.springframework.util.StringUtils;

import javax.annotation.Resource;
import javax.sql.DataSource;

@Configuration
@PropertySource(value = {
        "classpath:configuration/db.properties",
        "classpath:configuration/jpa.properties" })
public class DataSourceConfig {

    private static final Logger LOG = LoggerFactory.getLogger(DataSourceConfig.class);

    @Configuration
    @EmbeddedDatabase
    static class TestDataSourceContext implements DataSourceContext {

        @Bean(name = "dataSource", destroyMethod = "shutdown")
        @Override
        public DataSource getDataSource() {
            return new EmbeddedDatabaseBuilder()
                    .setType(EmbeddedDatabaseType.H2)
                    .build();
        }

        @Override
        public String getHibernateDatabaseDialect() {
            return "org.hibernate.dialect.H2Dialect";
        }

        @Override
        public String getHbm2ddlAutoMode() {
            return "update";
        }
    }

    @Configuration
    @StandardDatabase
    static class DefaultDataSourceContext implements DataSourceContext {

        @Resource
        private Environment environment;

        @Resource
        private RuntimeEnvironmentUtil runtimeEnvironmentUtil;

        @Bean(destroyMethod = "close", name = "dataSource")
        @Override
        public DataSource getDataSource() {
            if (checkForProductionSafetySwitch()) {
                throw new IllegalStateException("Sorry bro, running PROD package is not allowed in this context! " +
                        "Add '-D allow-prod=true' to application servers boneCPDataSource.");
            }

            return createDataSource();
        }

        @Override
        public String getHibernateDatabaseDialect() {
            return environment.getRequiredProperty("jpa.platform");
        }

        @Override
        public String getHbm2ddlAutoMode() {
            return environment.getRequiredProperty("hibernate.hbm2ddl.auto");
        }

        protected boolean checkForProductionSafetySwitch() {
            // Check that production is allowed if production profile is used, otherwise stop startup.
            return runtimeEnvironmentUtil.isProductionEnvironment() &&
                    !environment.getProperty("allow-prod", boolean.class, false);
        }

        protected BoneCPDataSource createDataSource() {
            final BoneCPDataSource dataSource = new BoneCPDataSource();

            LOG.info("Using JDBC url={}", environment.getRequiredProperty("db.url"));

            dataSource.setDriverClass(environment.getRequiredProperty("db.driver"));
            dataSource.setJdbcUrl(environment.getRequiredProperty("db.url"));
            dataSource.setUsername(environment.getRequiredProperty("db.username"));
            dataSource.setPassword(environment.getRequiredProperty("db.password"));

            dataSource.setIdleConnectionTestPeriodInMinutes(240); // Default: 240
            dataSource.setIdleMaxAgeInMinutes(60); // Default: 60
            dataSource.setConnectionTestStatement(environment.getRequiredProperty("db.connection.test.statement"));

            // Create new connections when available count is below 20 %
            dataSource.setPoolAvailabilityThreshold(20); // Default: 20
            dataSource.setPartitionCount(environment.getRequiredProperty("db.partition.count", int.class)); // Default: 1
            dataSource.setMinConnectionsPerPartition(
                    environment.getRequiredProperty("db.partition.min.connections", int.class));
            dataSource.setMaxConnectionsPerPartition(
                    environment.getRequiredProperty("db.partition.max.connections", int.class));
            dataSource.setAcquireIncrement(2); // Default: 2
            dataSource.setStatementsCacheSize(50); // Default: 0
            dataSource.setDisableConnectionTracking(true); // Not required with Spring
            dataSource.setDefaultAutoCommit(false); // Hibernate default
            dataSource.setDeregisterDriverOnClose(true);

            // Read Committed is the default in PostgreSQL. ref: http://www.postgresql.org/docs/9.1/static/transaction-iso.html
            // Repeatable Read is the default in MySQL InnoDB. ref: http://dev.mysql.com/doc/refman/5.1/en/set-transaction.html#isolevel_repeatable-read
            final String defaultTransactionIsolation = environment.getRequiredProperty("db.isolation.level");
            if (StringUtils.hasText(defaultTransactionIsolation)) {
                // Should be set to one of: NONE, READ_COMMITTED, READ_UNCOMMITTED, REPEATABLE_READ or SERIALIZABLE
                dataSource.setDefaultTransactionIsolation(defaultTransactionIsolation);
            }

            return dataSource;
        }
    }

}
