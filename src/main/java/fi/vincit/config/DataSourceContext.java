package fi.vincit.config;

import javax.sql.DataSource;

public interface DataSourceContext {

    DataSource getDataSource();

    String getHibernateDatabaseDialect();

    String getHbm2ddlAutoMode();

}
