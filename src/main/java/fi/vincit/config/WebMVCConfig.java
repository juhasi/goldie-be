package fi.vincit.config;

import com.google.common.base.Throwables;

import fi.vincit.controller.interceptor.cache.CacheControlHandlerInterceptor;
import fi.vincit.feature.common.util.RuntimeEnvironmentUtil;
import fi.vincit.feature.common.workaround.CustomJacksonObjectMapper;

import org.joda.time.Days;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.data.web.config.EnableSpringDataWebSupport;
import org.springframework.http.converter.ByteArrayHttpMessageConverter;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.ResourceHttpMessageConverter;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.http.converter.support.AllEncompassingFormHttpMessageConverter;
import org.springframework.http.converter.xml.Jaxb2RootElementHttpMessageConverter;
import org.springframework.http.converter.xml.SourceHttpMessageConverter;
import org.springframework.security.config.annotation.web.servlet.configuration.EnableWebMvcSecurity;
import org.springframework.security.config.annotation.web.servlet.configuration.WebMvcSecurityConfiguration;
import org.springframework.web.servlet.HandlerExceptionResolver;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.ViewResolver;
import org.springframework.web.servlet.config.annotation.DefaultServletHandlerConfigurer;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import org.springframework.web.servlet.mvc.annotation.ResponseStatusExceptionResolver;
import org.springframework.web.servlet.mvc.method.annotation.ExceptionHandlerExceptionResolver;
import org.springframework.web.servlet.mvc.support.DefaultHandlerExceptionResolver;
import org.springframework.web.servlet.view.InternalResourceViewResolver;
import org.springframework.web.servlet.view.JstlView;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.nio.charset.Charset;
import java.util.List;

@EnableWebMvc
@EnableWebMvcSecurity
@EnableSpringDataWebSupport
@ComponentScan("fi.vincit.controller")
@Import({
        WebSecurityConfig.class,
        WebMvcSecurityConfiguration.class
})
@Configuration
public class WebMVCConfig extends WebMvcConfigurerAdapter {
    private static final Logger LOG = LoggerFactory.getLogger(WebMVCConfig.class);

    @Resource
    private RuntimeEnvironmentUtil runtimeEnvironmentUtil;

    @Bean
    public ViewResolver viewResolver() {
        final InternalResourceViewResolver resolver = new InternalResourceViewResolver();

        resolver.setViewClass(JstlView.class);
        resolver.setPrefix("/WEB-INF/jsp/");
        resolver.setSuffix(".jsp");

        return resolver;
    }

    @Override
    public void addViewControllers(ViewControllerRegistry registry) {
        registry.addViewController("/admin/hibernate");
    }

    @Override
    public void configureDefaultServletHandling(DefaultServletHandlerConfigurer configurer) {
        /*
         * Ensures that dispatcher servlet can be mapped to '/' and that static resources
         * are still served by the containers default servlet.
         */
        configurer.enable();
    }

    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
//        registry.addResourceHandler("/static/**").addResourceLocations("/static/");
//        registry.addResourceHandler("/frontend/**").addResourceLocations("/frontend/");
        registry.addResourceHandler("/favicon.ico")
                .addResourceLocations("/static/img/favicon.ico")
                .setCachePeriod(Days.days(365).toStandardSeconds().getSeconds());
    }

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        // Cache control handler interceptor to assign cache-control headers to HTTP responses.
        final CacheControlHandlerInterceptor cacheControlHandlerInterceptor = new CacheControlHandlerInterceptor();
        cacheControlHandlerInterceptor.setUseExpiresHeader(true);

        registry.addInterceptor(cacheControlHandlerInterceptor);
    }

    // @see org.springframework.web.servlet.config.annotation.WebMvcConfigurationSupport
    @Override
    public void configureMessageConverters(List<HttpMessageConverter<?>> messageConverters) {
        final CustomJacksonObjectMapper objectMapper = new CustomJacksonObjectMapper(runtimeEnvironmentUtil);
        final MappingJackson2HttpMessageConverter jsonMessageConverter = new MappingJackson2HttpMessageConverter();
        jsonMessageConverter.setObjectMapper(objectMapper);

        final StringHttpMessageConverter stringHttpMessageConverter = new StringHttpMessageConverter(Charset.forName("UTF-8"));
        stringHttpMessageConverter.setWriteAcceptCharset(false);

        messageConverters.add(new ByteArrayHttpMessageConverter());
        messageConverters.add(stringHttpMessageConverter);
        messageConverters.add(new ResourceHttpMessageConverter());
        messageConverters.add(new SourceHttpMessageConverter<>());
        messageConverters.add(new AllEncompassingFormHttpMessageConverter());
        messageConverters.add(jsonMessageConverter);
        messageConverters.add(new Jaxb2RootElementHttpMessageConverter());
    }

    // @see org.springframework.web.servlet.config.annotation.WebMvcConfigurationSupport
    @Override
    public void configureHandlerExceptionResolvers(List<HandlerExceptionResolver> exceptionResolvers) {
        // NOTE: Do not change the order! Only first resolution is used.

        // By default Spring MVC does not log exceptions from @Controller methods.
        if (!runtimeEnvironmentUtil.isIntegrationTestEnvironment()) {
            exceptionResolvers.add(new LoggingHandlerExceptionResolver());
        }
        // Process exception using @ExceptionHandler method inside @Controller
        exceptionResolvers.add(exceptionHandlerExceptionResolver());
        // Return status code specified using @ResponseStatus in exception class
        exceptionResolvers.add(new ResponseStatusExceptionResolver());
        // Send HTTP status code depending on the exception which is mapped to error page in web.xml
        exceptionResolvers.add(new DefaultHandlerExceptionResolver());
    }

    @Bean
    public HandlerExceptionResolver exceptionHandlerExceptionResolver() {
        final ExceptionHandlerExceptionResolver exceptionHandlerExceptionResolver = new ExceptionHandlerExceptionResolver();
        exceptionHandlerExceptionResolver.setPreventResponseCaching(true);
        exceptionHandlerExceptionResolver.getMessageConverters().add(new MappingJackson2HttpMessageConverter());

        return exceptionHandlerExceptionResolver;
    }

    private static class LoggingHandlerExceptionResolver implements HandlerExceptionResolver {
        @Override
        public ModelAndView resolveException(HttpServletRequest request,
                                             HttpServletResponse response,
                                             Object handler,
                                             Exception ex) {
            LOG.error("Handler execution resulted in exception", Throwables.getRootCause(ex));

            return null;
        }
    }
}
