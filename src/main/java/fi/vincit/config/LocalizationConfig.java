package fi.vincit.config;

import fi.vincit.config.locale.AngularCookieLocaleResolver;
import fi.vincit.feature.common.util.RuntimeEnvironmentUtil;
import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.support.ReloadableResourceBundleMessageSource;
import org.springframework.web.servlet.LocaleResolver;
import org.springframework.web.servlet.i18n.CookieLocaleResolver;

import javax.annotation.Resource;

@Configuration
public class LocalizationConfig {
    @Resource
    private RuntimeEnvironmentUtil runtimeEnvironmentUtil;

    @Bean(name="messageSource")
    public MessageSource messageSource() {
        // MessageSource configuration for localized messages.
        final ReloadableResourceBundleMessageSource source = new ReloadableResourceBundleMessageSource();

        source.setBasenames("/WEB-INF/i18n/messages", "ValidationMessages");
        source.setUseCodeAsDefaultMessage(true);
        source.setFallbackToSystemLocale(false);
        source.setDefaultEncoding("ISO-8859-1");

        if (runtimeEnvironmentUtil.isDevelopmentEnvironment()) {
            // Check for updates on every refresh, otherwise cache forever
            source.setCacheSeconds(1);
        }

        return source;
    }

    @Bean
    public LocaleResolver localeResolver() {
        final CookieLocaleResolver localeResolver = new AngularCookieLocaleResolver();
        localeResolver.setCookieName("NG_TRANSLATE_LANG_KEY");

        // This should force to use browser language when not available
        localeResolver.setDefaultLocale(runtimeEnvironmentUtil.getDefaultLocale());

        return localeResolver;
    }
}
