package fi.vincit.config;

import fi.vincit.config.base.BaseWebSecurityConfig;
import fi.vincit.security.csrf.CookieCsrfTokenRepository;
import fi.vincit.security.csrf.DefaultCsrfRequestMatcher;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpMethod;
import org.springframework.security.access.PermissionEvaluator;
import org.springframework.security.access.expression.SecurityExpressionHandler;
import org.springframework.security.access.hierarchicalroles.RoleHierarchy;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.web.FilterInvocation;
import org.springframework.security.web.access.expression.DefaultWebSecurityExpressionHandler;
import org.springframework.security.web.csrf.CsrfTokenRepository;
import org.springframework.security.web.savedrequest.NullRequestCache;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;
import org.springframework.security.web.util.matcher.RequestMatcher;

@Configuration
@EnableWebSecurity
public class WebSecurityConfig {

    public static final String PATTERN_API = "/api/**";
    public static final String PATTERN_ADMIN_API = "/api/v1/admin/**";
    public static final String PATTERN_ADMIN_UI = "/admin/**";
    public static final String PATTERN_REVISION_API = "/api/revision";

    public static final String[] PUBLIC_PATTERNS = new String[] {
            // Registration
            "/api/v1/register/**",

            // Password reset
            "/api/v1/password/forgot",
            "/api/v1/password/reset"
    };

    // Security filter chain can be skipped completely for ignored patterns
    public static final String[] IGNORED_PATTERS = new String[] {
            "/static/**",
            "/frontend/**",
            "/favicon.ico"
    };

    @Bean
    public SecurityExpressionHandler<FilterInvocation> webSecurityExpressionHandler(
            PermissionEvaluator permissionEvaluator, RoleHierarchy roleHierarchy) {
        final DefaultWebSecurityExpressionHandler handler = new DefaultWebSecurityExpressionHandler();

        handler.setRoleHierarchy(roleHierarchy);
        handler.setPermissionEvaluator(permissionEvaluator);

        return handler;
    }

    @Bean
    public CsrfTokenRepository csrfTokenRepository() {
        return new CookieCsrfTokenRepository();
    }

    @Bean
    public RequestMatcher csrfRequestMatcher() {
        // Default matches all requests but GET, HEAD, TRACE or OPTIONS
        return new DefaultCsrfRequestMatcher();
    }

    @Configuration
    @Order(1)
    public static class ApiWebSecurityConfig extends BaseWebSecurityConfig {

        @Override
        protected void configure(HttpSecurity http) throws Exception {
            // Define security for URI starting with api-prefix
            final HttpSecurity httpSecurity =
                    http.antMatcher("/api/**");

            httpSecurity
                    .httpBasic().authenticationEntryPoint(apiEntryPoint()).and()
                    .rememberMe().rememberMeServices(rememberMeServices).and()
                    .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.NEVER).and()
                    .formLogin().disable()
                    .logout().disable()
                    .requestCache().requestCache(new NullRequestCache()).and()

                    .headers().frameOptions().contentTypeOptions().xssProtection().httpStrictTransportSecurity().and()
                    .csrf().csrfTokenRepository(csrfTokenRepository).requireCsrfProtectionMatcher(csrfRequestMatcher).and()
                    .exceptionHandling().authenticationEntryPoint(apiEntryPoint()).and()
                    .authorizeRequests().expressionHandler(webSecurityExpressionHandler);

            // API testing with CSRF protection is too complicated.
            if (runtimeEnvironmentUtil.isIntegrationTestEnvironment()) {
                httpSecurity.csrf().disable();
            }

            for (String publicPattern : PUBLIC_PATTERNS) {
                httpSecurity.authorizeRequests().antMatchers(publicPattern).permitAll();
            }

            httpSecurity.authorizeRequests()
                    // Admin API
                    .antMatchers(PATTERN_ADMIN_API).hasRole("ADMIN")

                    // Restricted API for revision
                    .antMatchers(PATTERN_REVISION_API).hasRole("ADMIN");

            setupIntegrationTestApi(httpSecurity);

            httpSecurity.authorizeRequests()
                    // Default permissions
                    .antMatchers(PATTERN_API).hasAnyRole("REST", "USER", "ADMIN")

                    // Deny anything else
                    .anyRequest().denyAll();
        }

        private void setupIntegrationTestApi(HttpSecurity httpSecurity) throws Exception {
            // Configure requests related to integration testing
            if (runtimeEnvironmentUtil.isDevelopmentEnvironment() ||
                    runtimeEnvironmentUtil.isIntegrationTestEnvironment()) {
                httpSecurity.authorizeRequests()
                        .antMatchers("/api/test/secure/**").hasRole("REST")
                        .antMatchers("/api/test/**").permitAll();
            } else {
                httpSecurity.authorizeRequests()
                        .antMatchers("/api/test/**").denyAll();
            }
        }
    }

    @Order(2)
    @Configuration
    public static class SessionBasedSecurityConfigurationAdapter extends BaseWebSecurityConfig {
        @Override
        public void configure(WebSecurity web) {
            // Public resources do not pass security filter chain
            web.ignoring().antMatchers(IGNORED_PATTERS);
            web.expressionHandler(webSecurityExpressionHandler);
        }

        @Override
        protected void configure(HttpSecurity http) throws Exception {
            HttpSecurity httpSecurity =
                    http.antMatcher("/**");

            httpSecurity
                    .rememberMe().rememberMeServices(rememberMeServices).and()
                    .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.IF_REQUIRED).and()

                    // Replace normal form-login with one-time-token filter
                    .formLogin()
                    .loginPage(properties.getLoginPageUrl())
                    .loginProcessingUrl(properties.getLoginActionUrl())
                    .successHandler(authenticationSuccessHandler)
                    .failureHandler(authenticationFailureHandler)
                    .and()
                    .logout()
                    .logoutRequestMatcher(new AntPathRequestMatcher(properties.getLogoutAction(), "POST"))
                    .logoutSuccessHandler(logoutSuccessHandler)
                    .deleteCookies("JSESSIONID").and()
                    .requestCache().requestCache(new NullRequestCache()).and()

                    .headers().frameOptions().contentTypeOptions().xssProtection().httpStrictTransportSecurity().and()
                    .csrf().csrfTokenRepository(csrfTokenRepository).requireCsrfProtectionMatcher(csrfRequestMatcher).and()
                    .exceptionHandling().authenticationEntryPoint(mainEntryPoint())
                    .and()
                    .authorizeRequests().expressionHandler(webSecurityExpressionHandler);

            if (runtimeEnvironmentUtil.isIntegrationTestEnvironment()) {
                http.csrf().disable();
            }

            httpSecurity.authorizeRequests()
                    .antMatchers(PATTERN_ADMIN_UI).hasRole("ADMIN")
                    .antMatchers(HttpMethod.GET, properties.getLoginPageUrl()).permitAll()

                    // Deny anything else
                    .anyRequest().denyAll();

            if (runtimeEnvironmentUtil.isLoginAuditingEnabled()) {
                httpSecurity.logout().addLogoutHandler(logoutAuditEventListener);
            }
        }

    }
}
