<%@ page contentType="text/html;charset=UTF-8" language="java" session="false" %>
<%@ include file="/WEB-INF/jsp/common/taglib.jsp" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="Vincit Oy"/>
    <meta name="_csrf" content="${_csrf.token}"/>
    <meta name="_csrf_header" content="${_csrf.headerName}"/>

    <%-- Apply cache busting by revisioning JS/CSS assets using GIT revision (prod) or current timestamp (dev) --%>
    <spring:eval expression="@environment['git.commit.id.abbrev']"  javaScriptEscape="true" var="gitCommitId" />
    <jsp:useBean id="currentDate" class="java.util.Date" />
    <c:set var="rev" value="${fn:length(gitCommitId) > 0 ? gitCommitId : currentDate.time}"  />

    <title ng-bind-template="{{pageTitle}}"></title>
    <link rel="stylesheet" href="${contextPath}/v/${rev}/css/app.css">

    <!--[if lte IE 7]>
    <script src="${contextPath}/v/${rev}/js/lib/json3.min.js"></script>
    <![endif]-->

    <!--[if lt IE 9]>
    <script src="${contextPath}/v/${rev}/js/lib/es5-shim.min.js"></script>
    <![endif]-->

    <script>
        // include angular loader, which allows the files to load in any order
        <%@ include file="/frontend/js/lib/angular-loader.min.js" %>
        <%@ include file="/frontend/js/lib/script.min.js" %>

        // load all of the dependencies asynchronously.
        $script([
            '${contextPath}/v/${rev}/js/vendor.min.js',
            '${contextPath}/v/${rev}/js/app.min.js',
            '${contextPath}/v/${rev}/js/partials.js'
        ], function() {
            angular.module('app.metadata', []).constant('appRevision', '${rev}');

            // when all is done, execute bootstrap angular application
            angular.bootstrap(document, ['app']);
        });
    </script>
</head>
<body ng-cloak>
<noscript>
    <div class="no-js">
        <div class="well">
            <p>Selaimesi ei tue JavaScriptia tai olet poistanut JavaScript tuen käytöstä.</p>
            <p>Tämän sivuston käyttäminen vaatii JavaScript tuen. Jos nykyinen selaimesi ei tue JavaScriptia, lataa jokin alla olevista selaimista.</p>
        </div>

        <div class="well">
            <p>Your browser does not support JavaScript or you have disabled JavaScript support.</p>
            <p>This web-site requires JavaScript support. If your current browser doesn't support JavaScript please download some other browser from the links below.</p>
        </div>

        <ul class="list-unstyled well">
            <li><a href="http://www.google.com/chrome">Google Chrome</a></li>
            <li><a href="http://www.apple.com/safari/">Safari</a></li>
            <li><a href="http://www.mozilla.org/en-US/firefox/new/">Mozilla Firefox</a></li>
        </ul>
    </div>
</noscript>
<div class="wrapper">
    <nav class="navbar navbar-default navbar-inverse navbar-static-top" role="navigation">
        <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" ng-init="navCollapsed = true" ng-click="navCollapsed = !navCollapsed">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>

                <a class="navbar-brand" ui-sref="home">
                    <span class="glyphicon glyphicon-home">&nbsp;</span>
                    Template
                </a>
            </div>

            <div class="navbar-collapse collapse" ng-class="!navCollapsed && 'in'">
                <div ng-include="'frontend/partials/nav.html'"></div>
            </div><!-- /.navbar-collapse -->
        </div>
    </nav>

    <div class="container main-content">
        <div ui-view autoscroll="false"></div>
    </div>

    <div class="push"></div>
</div>
<footer class="footer">
    <div class="container">
        <p class="text-muted text-center">
            &copy; 2013 <a href="http://www.vincit.fi" target="_blank">Vincit | Version <span app-version></span></a>
        </p>
    </div>
</footer>
</body>
</html>