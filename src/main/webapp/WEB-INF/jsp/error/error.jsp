<%@ page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8" language="java" session="false" %>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="vincit" tagdir="/WEB-INF/tags" %>

<spring:eval expression="@environment['environment.id'] == 'dev'" var="showExceptionDetails"/>

<vincit:errorLayout>
    <jsp:attribute name="pageTitle"><spring:message code="error.page.title"/></jsp:attribute>
    <jsp:body>
        <h1><spring:message code="error.page.title"/></h1>
        <p><strong><c:out value="${requestScope['javax.servlet.error.status_code']}"/></strong></p>
        <p>Repository version: <spring:eval expression="@environment['git.commit.id.describe']" /></p>
        <p>Build time: <spring:eval expression="@environment['git.build.time']" /></p>

        <div class="error-details">
            <dl>
                <c:if test="${showExceptionDetails}">
                    <dt>Exception</dt>
                    <dd><c:out value="${requestScope['javax.servlet.error.exception']}"/></dd>
                </c:if>

                <c:if test="${not showExceptionDetails}">
                    <dt>Error message</dt>
                    <dd><c:out value="${requestScope['javax.servlet.error.message']}"/></dd>
                </c:if>

                <dt>Action URL</dt>
                <dd><c:out value="${requestScope['javax.servlet.forward.request_uri']}"/></dd>

                <dt>User agent</dt>
                <dd><c:out value="${header['user-agent']}"/></dd>
            </dl>

            <c:if test="${showExceptionDetails}">
                <div class="error-stacktrace"><pre><vincit:stackTrace/></pre></div>
            </c:if>
        </div>
    </jsp:body>
</vincit:errorLayout>
