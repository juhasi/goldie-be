<%@ page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8" language="java" session="false" %>

<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="vincit" tagdir="/WEB-INF/tags" %>

<vincit:errorLayout>
    <jsp:attribute name="pageTitle"><spring:message code="access.denied.page.title"/></jsp:attribute>
    <jsp:body>
        <h1><spring:message code="access.denied.page.title"/></h1>
        <p><spring:message code="access.denied.page.message"/></p>
    </jsp:body>
</vincit:errorLayout>