<%@tag description="Prints stack trace of the specified Throwable" pageEncoding="UTF-8"%><%
    java.io.PrintWriter pOut = new java.io.PrintWriter(out);
    try {
        // The Servlet spec guarantees this attribute will be available
        Throwable err = (Throwable)
                request.getAttribute("javax.servlet.error.exception");

        if(err != null) {
            if(err instanceof ServletException) {
                // It's a ServletException: we should extract the root cause
                ServletException se = (ServletException) err;
                Throwable rootCause = se.getRootCause();
                if(rootCause == null) {
                    rootCause = se;
                }
                out.println("** Root cause is: " + rootCause.getMessage());
                rootCause.printStackTrace(pOut);
            }else {
                // It's not a ServletException, so we'll just show it
                err.printStackTrace(pOut);
            }
        }else {
            out.println("No error information available");
        }

        // Display cookies
        out.println("\nCookies:\n");
        Cookie[] cookies = request.getCookies();
        if(cookies != null) {
            for(int i = 0; i < cookies.length; i++) {
                out.println(cookies[i].getName() + "=[" +
                        cookies[i].getValue() + "]");
            }
        }

    } catch(Exception ex) {
        ex.printStackTrace(pOut);
    }
%>