<%@tag pageEncoding="UTF-8" trimDirectiveWhitespaces="true" %>
<%@attribute name="pageTitle" required="false" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %><!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>${pageTitle}</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <style>

        * {
            margin: 0;
            line-height: 1.5;
        }

        html {
            color: #888;
            font-family: sans-serif;
            text-align: center;
        }

        body {
            left: 50%;
            margin: -43px 0 0 -150px;
            position: absolute;
            top: 50%;
            width: 300px;
        }

        h1 {
            color: #555;
            font-size: 2em;
            font-weight: 400;
        }

        p {
            line-height: 1.2;
        }

        .error-details {
            position: fixed;
            right: 1em;
            top: 0;
            width: 400px;
            text-align: left;
        }

        dt {
            font-weight: bold;
            margin-top: 1em;
        }

        .error-stacktrace {
            position: fixed;
            left: 1em;
            top: 0;
            width: 1200px;
            text-align: left;
        }

        @media only screen and (max-width: 270px) {

            body {
                margin: 10px auto;
                position: static;
                width: 95%;
            }

            h1 {
                font-size: 1.5em;
            }

        }

    </style>
</head>
<body><jsp:doBody/></body>
</html>
<!-- IE requires 512+ bytes: http://www.404-error-page.com/404-error-page-too-short-problem-microsoft-ie.shtml -->
